DROP DATABASE if exists formations;
CREATE DATABASE formations;
USE formations;
-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Ven 22 Février 2019 à 14:08
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `formations`
--

-- --------------------------------------------------------

--
-- Structure de la table `cours`
--

CREATE TABLE `cours` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `pre_requis` text NOT NULL,
  `documents_utiles` text NOT NULL,
  `exemption` text NOT NULL,
  `observation` text NOT NULL,
  `section` int(10) UNSIGNED NOT NULL,
  `lien` varchar(255) DEFAULT NULL,
  `texte` text,
  `header_path` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `cours`
--

INSERT INTO `cours` (`id`, `nom`, `description`, `pre_requis`, `documents_utiles`, `exemption`, `observation`, `section`, `lien`, `texte`, `header_path`) VALUES
(1, 'Technicien en comptabilité', '<p>Le technicien en comptabilité est capable :</p><ul><li>de transcrire et enregistrer des données comptables à partir de pièces justificatives;</li><li>d\'organiser leur classement et leur archivage, en utilisant les techniques informatiques et bureautiques, dans le respect des prescriptions légales;</li><li>de rédiger les déclarations à la Sécurité sociale, à la TVA, à l’impôt des personnes physiques et à l’impôt des sociétés, en ce qui concerne les PME;</li><li>d\'élaborer ou participer à l’élaboration des documents de fin d’exercice.</li></ul><p>La présentation d\'une épreuve intégrée permet l\'obtention du certificat authentifié par la Fédération Wallonie Bruxelles. Une année complémentaire permet d\'obtenir l\'équivalence complète avec le CESS des Humanités générales.</p>', 'Aucun pré-requis n\'est demandé.', 'Munissez-vous de votre carte d’identité, des éventuelles attestations d’exonération, du permis de séjour d’une validité de plus de trois mois (étrangers hors CEE) et du montant du droit d’inscription (espèces ou bancontact).', '<ul><li>Jeunes de moins de 18 ans au 30 septembre de l’année en cours, sur attestation d’inscription dans une école de plein exercice.</li><li>Chômeurs complets indemnisés et demandeurs d’emplois, les droits d’inscription sont exigés, mais remboursés sur avis favorable du FOREM ou d’ACTIRIS.</li><li>Sur attestation du VDAB.</li><li>Personnes bénéficiant du revenu d’intégration sociale sur base d’une attestation originale du C.P.A.S.</li><li>Autres ayants droit légaux.</li></ul>', 'Certains étrangers sont soumis à un droit d’inscription complémentaire.', 1, NULL, NULL, 'uploaded/headers/24307TechCompta.jpg'),
(2, 'Technicien électricien/automaticien', '<p>L\'électricien-automaticien est un technicien chargé de monter, modifier, régler ou entretenir des équipements automatisés à partir de cahiers des charges, de plans mécaniques, de schémas électriques ou de documents « constructeur ».</p><p>Il est capable d’intervenir à divers degrés dans les domaines de l’électrotechnique, l’automatisation, la régulation, la pneumatique et l’hydraulique.</p><p>Pour accomplir ces différents travaux, il doit respecter le R.G.I.E, les règles et dispositions en matière de sécurité, d’hygiène et de protection des biens, des personnes et de l’environnement.</p><p>Il est capable de s’adapter à l’évolution technologique.</p><p>C’est également une personne de communication apte à échanger des informations à caractère technique.</p><p>L’électricien automaticien, dont la formation est renforcée par de l’électronique, est très apprécié et recherché dans le monde du travail actuellement.</p><p>Une étude du CEFORA et de l’UPEDI signale le technicien en électricité comme fonction critique en Brabant wallon.</p><p>A noter que les cours de français, mathématiques et méthodes de travail peuvent faire l’objet de dispenses pour les titulaires d’un CESS.</p>', 'Aucun pré-requis n\'est demandé.', 'Munissez-vous de votre carte d’identité, des éventuelles attestations d’exonération, du permis de séjour d’une validité de plus de trois mois (étrangers hors CEE) et du montant du droit d’inscription (espèces ou bancontact).', '<ul><li>Jeunes de moins de 18 ans au 30 septembre de l’année en cours, sur attestation d’inscription dans une école de plein exercice.</li><li>Chômeurs complets indemnisés et demandeurs d’emplois, les droits d’inscription sont exigés, mais remboursés sur avis favorable du FOREM ou d’ACTIRIS.</li><li>Sur attestation du VDAB.</li><li>Personnes bénéficiant du revenu d’intégration sociale sur base d’une attestation originale du C.P.A.S.</li><li>Autres ayants droit légaux.</li></ul>', 'Certains étrangers sont soumis à un droit d’inscription complémentaire.', 1, NULL, NULL, 'uploaded/headers/4641TechElec.jpg'),
(3, 'Connaissance de gestion', '<p>Sur le plan légal, la section a pour but de répondre aux exigences de l\'article 7 de l\'arrêté royal du 21/10/98 fixant les mesures d\'exécution de la loi du 10/02/98 relative à la Promotion de l\'entreprise indépendante.</p><p>Sur le plan socio-professionnel, la section vise à permettre à l\'étudiant de maîtriser les compétences de base dans le domaine de la création d\'entreprise, de la comptabilité, de la fiscalité, de la gestion financière et commerciale et de la législation nécessaires à l\'exercice de toute activité professionnelle à titre d\'indépendant.</p><p>L\'étudiant est amené à appréhender ses qualités d\'entrepreneur, à maîtriser les compétences de base dans le domaine de la création d\'entreprise, de la comptabilité, de la fiscalité, de la gestion financière et commerciale et de la législation.</p>', 'Aucun pré-requis n\'est demandé.', 'Munissez-vous de votre carte d’identité, des éventuelles attestations d’exonération, du permis de séjour d’une validité de plus de trois mois (étrangers hors CEE) et du montant du droit d’inscription (espèces ou bancontact).', '<ul><li>Jeunes de moins de 18 ans au 30 septembre de l’année en cours, sur attestation d’inscription dans une école de plein exercice.</li><li>Chômeurs complets indemnisés et demandeurs d’emplois, les droits d’inscription sont exigés, mais remboursés sur avis favorable du FOREM ou d’ACTIRIS.</li><li>Sur attestation du VDAB.</li><li>Personnes bénéficiant du revenu d’intégration sociale sur base d’une attestation originale du C.P.A.S.</li><li>Autres ayants droit légaux.</li></ul>', 'Certains étrangers sont soumis à un droit d’inscription complémentaire.', 1, NULL, NULL, 'uploaded/headers/6280gestion.jpg'),
(4, 'Technicien en informatique', '<p>Le technicien en informatique intervient sur des ensembles liés à la micro-informatique et aux réseaux d\'ordinateurs, tant au niveau logiciel que matériel : installation/désinstallation, modification et connexion d\'équipements informatiques.</p><p>Il assure les différentes fonctions suivantes :</p><ul><li>démontage et mise en service d’un système informatique sur le site de l\'entreprise ou chez le client,</li><li>maintenance, diagnostic de pannes et interventions techniques,</li><li>conseil, formation et assistance technique,</li><li>support aux utilisateurs (helpdesk).</li></ul>', 'Aucun pré-requis n\'est demandé.', 'Munissez-vous de votre carte d’identité, des éventuelles attestations d’exonération, du permis de séjour d’une validité de plus de trois mois (étrangers hors CEE) et du montant du droit d’inscription (espèces ou bancontact).', '<ul><li>Jeunes de moins de 18 ans au 30 septembre de l’année en cours, sur attestation d’inscription dans une école de plein exercice.</li><li>Chômeurs complets indemnisés et demandeurs d’emplois, les droits d’inscription sont exigés, mais remboursés sur avis favorable du FOREM ou d’ACTIRIS.</li><li>Sur attestation du VDAB.</li><li>Personnes bénéficiant du revenu d’intégration sociale sur base d’une attestation originale du C.P.A.S.</li><li>Autres ayants droit légaux.</li></ul>', 'Certains étrangers sont soumis à un droit d’inscription complémentaire.', 1, NULL, NULL, 'uploaded/headers/30278TechInfo.jpg'),
(6, 'Bachelier en comptabilité', '<p>Agréé depuis septembre 1992 dans le cadre de la formation des candidats experts comptables, notre diplôme forme à la gestion d’une entreprise. Conscient du poids de la fiscalité directe et indirecte, le comptable IFOSUP reçoit, par une approche pratique, la formation nécessaire permettant de remplir les déclarations IPP, I.Soc. et TVA. Il est à même d’orienter les gestionnaires dans la prise de décision. (Diplôme reconnu dans toute la CEE, totalisant 180 ECTS).</p>', '<p>Pour être admis comme étudiant(e) régulier(ère) dans le supérieur de type court, l’étudiant(e) doit fournir le diplôme du secondaire supérieur (CESS). Est également admissible l’étudiant(e) qui réussit une épreuve d’admission (pour bénéficier de cette possibilité, l’étudiant(e) doit déposer son dossier personnel auprès de la Direction).</p><p>Des dispenses peuvent être accordées par le Conseil des études.</p><p>Attention: notre école ne permet pas aux étudiants hors CEE d’obtenir un permis de séjour, sauf pour le français langue étrangère (intensif).</p>', 'Munissez-vous de votre carte d’identité, des éventuelles attestations d’exonération, du permis de séjour d’une validité de plus de trois mois (étrangers hors CEE) et du montant du droit d’inscription (espèces ou bancontact).', '<ul><li>Jeunes de moins de 18 ans au 30 septembre de l’année en cours, sur attestation d’inscription dans une école de plein exercice.</li><li>Chômeurs complets indemnisés et demandeurs d’emplois, les droits d’inscription sont exigés, mais remboursés sur avis favorable du FOREM ou d’ACTIRIS.</li><li>Sur attestation du VDAB.</li><li>Personnes bénéficiant du revenu d’intégration sociale sur base d’une attestation originale du C.P.A.S.</li><li>Autres ayants droit légaux.</li></ul>', 'Certains étrangers sont soumis à un droit d’inscription complémentaire.', 2, NULL, NULL, 'uploaded/headers/23053BacCompta.jpg'),
(7, 'Bachelier en informatique de gestion', '<p>Le bachelier en informatique élabore, seul ou intégré dans une équipe de spécialistes, le dossier d’analyse d’un projet. Il programme des applications, en assure la maintenance et participe à la formation des utilisateurs finaux. Il résout, seul ou avec l’assistance d’un spécialiste, les problèmes liés aux environnements systèmes d’exploitation, réseaux locaux ou systèmes de télécommunication. Polyvalent, il assure le support utilisateurs et se trouve au cœur des problèmes de convivialité et de productivité des ressources des systèmes informatiques. Il assume la gestion du parc machines.</p><p>Dans la situation de pénurie actuelle, le bachelier en Informatique trouve de l’emploi dans le courant de sa formation en tant que spécialiste de l’informatique dans tous les domaines.</p><p>(Diplôme reconnu dans toute la CEE, totalisant 180 ECTS).</p>', '<p>Pour être admis comme étudiant(e) régulier(ère) dans le supérieur de type court, l’étudiant(e) doit fournir le diplôme du secondaire supérieur (CESS). Est également admissible l’étudiant(e) qui réussit une épreuve d’admission (pour bénéficier de cette possibilité, l’étudiant(e) doit déposer son dossier personnel auprès de la Direction).</p><p>Des dispenses peuvent être accordées par le Conseil des études.</p><p>Attention: notre école ne permet pas aux étudiants hors CEE d’obtenir un permis de séjour, sauf pour le français langue étrangère (intensif).</p>', 'Munissez-vous de votre carte d’identité, des éventuelles attestations d’exonération, du permis de séjour d’une validité de plus de trois mois (étrangers hors CEE) et du montant du droit d’inscription (espèces ou bancontact).', '<ul><li>Jeunes de moins de 18 ans au 30 septembre de l’année en cours, sur attestation d’inscription dans une école de plein exercice.</li><li>Chômeurs complets indemnisés et demandeurs d’emplois, les droits d’inscription sont exigés, mais remboursés sur avis favorable du FOREM ou d’ACTIRIS.</li><li>Sur attestation du VDAB.</li><li>Personnes bénéficiant du revenu d’intégration sociale sur base d’une attestation originale du C.P.A.S.</li><li>Autres ayants droit légaux.</li></ul>', 'Certains étrangers sont soumis à un droit d’inscription complémentaire.', 2, NULL, NULL, 'uploaded/headers/3194BacInfo.jpg'),
(8, 'Bachelier en marketing', '<p>Le Bachelier en Marketing identifie les courants d\'affaires, les besoins réels et latents, actuels et futurs des consommateurs. Il participe à l\'audit marketing, au marketing mix opérationnel et stratégique. Il met en œuvre les plans marketing, promotionnels et publicitaires. Il met en œuvre les stratégies et procédures de management en place. Il gère les ventes d\'affaires et suit les dossiers commerciaux. Il participe aux activités de marketing industriel et de commerce international.</p><p>(Diplôme reconnu dans toute la CEE, totalisant 180 ECTS)</p>', '<p>Pour être admis comme étudiant(e) régulier(ère) dans le supérieur de type court, l’étudiant(e) doit fournir le diplôme du secondaire supérieur (CESS). Est également admissible l’étudiant(e) qui réussit une épreuve d’admission (pour bénéficier de cette possibilité, l’étudiant(e) doit déposer son dossier personnel auprès de la Direction).</p><p>Des dispenses peuvent être accordées par le Conseil des études.</p><p>Attention: notre école ne permet pas aux étudiants hors CEE d’obtenir un permis de séjour, sauf pour le français langue étrangère (intensif).</p>', 'Munissez-vous de votre carte d’identité, des éventuelles attestations d’exonération, du permis de séjour d’une validité de plus de trois mois (étrangers hors CEE) et du montant du droit d’inscription (espèces ou bancontact).', '<ul><li>Jeunes de moins de 18 ans au 30 septembre de l’année en cours, sur attestation d’inscription dans une école de plein exercice.</li><li>Chômeurs complets indemnisés et demandeurs d’emplois, les droits d’inscription sont exigés, mais remboursés sur avis favorable du FOREM ou d’ACTIRIS.</li><li>Sur attestation du VDAB.</li><li>Personnes bénéficiant du revenu d’intégration sociale sur base d’une attestation originale du C.P.A.S.</li><li>Autres ayants droit légaux.</li></ul>', 'Certains étrangers sont soumis à un droit d’inscription complémentaire.', 2, NULL, NULL, 'uploaded/headers/11307BacMarketing.jpg'),
(9, 'BES Webdesign', '<p>Naviguer et exploiter les ressources sur internet, créer des pages WEB, créer un site WEB intégrant les images fixes et animées, les sons et les séquences vidéo, créer des fichiers images et sons, modifier des images animées et films, gérer un site WEB.</p><p>Cette formation s\'adresse à toute personne désireuse de participer en tant qu\'acteur à la révolution économique et culturelle qu\'induit le développement des applications internet. Elle concerne tous ceux qui veulent travailler dans ce domaine, à croissance exponentielle, qui tend à bouleverser notre mode de vie.</p>', '<p>Pour être admis comme étudiant(e) régulier(ère) dans le supérieur de type court, l’étudiant(e) doit fournir le diplôme du secondaire supérieur (CESS). Est également admissible l’étudiant(e) qui réussit une épreuve d’admission (pour bénéficier de cette possibilité, l’étudiant(e) doit déposer son dossier personnel auprès de la Direction).</p><p>Des dispenses peuvent être accordées par le Conseil des études.</p><p>Attention: notre école ne permet pas aux étudiants hors CEE d’obtenir un permis de séjour, sauf pour le français langue étrangère (intensif).</p>', 'Munissez-vous de votre carte d’identité, des éventuelles attestations d’exonération, du permis de séjour d’une validité de plus de trois mois (étrangers hors CEE) et du montant du droit d’inscription (espèces ou bancontact).', '<ul><li>Jeunes de moins de 18 ans au 30 septembre de l’année en cours, sur attestation d’inscription dans une école de plein exercice.</li><li>Chômeurs complets indemnisés et demandeurs d’emplois, les droits d’inscription sont exigés, mais remboursés sur avis favorable du FOREM ou d’ACTIRIS.</li><li>Sur attestation du VDAB.</li><li>Personnes bénéficiant du revenu d’intégration sociale sur base d’une attestation originale du C.P.A.S.</li><li>Autres ayants droit légaux.</li></ul>', 'Certains étrangers sont soumis à un droit d’inscription complémentaire.', 2, NULL, NULL, 'uploaded/headers/1069webdesign.jpg'),
(10, 'BES Webdeveloper', '<p>Naviguer et exploiter les ressources sur internet, créer des pages WEB, créer un site WEB intégrant des images fixes et animées, des sons et des séquences vidéo, créer des fichiers images et sons, modifier des images animées et films, gérer un site WEB.</p><p>Cette formation s\'adresse à toute personne désireuse de participer en tant qu\'acteur à la révolution économique et culturelle qu\'induit le développement des applications internet. Elle concerne tous ceux qui veulent travailler dans ce domaine, à croissance exponentielle, qui tend à bouleverser notre mode de vie.</p>', '<p>Pour être admis comme étudiant(e) régulier(ère) dans le supérieur de type court, l’étudiant(e) doit fournir le diplôme du secondaire supérieur (CESS). Est également admissible l’étudiant(e) qui réussit une épreuve d’admission (pour bénéficier de cette possibilité, l’étudiant(e) doit déposer son dossier personnel auprès de la Direction).</p><p>Des dispenses peuvent être accordées par le Conseil des études.</p><p>Attention: notre école ne permet pas aux étudiants hors CEE d’obtenir un permis de séjour, sauf pour le français langue étrangère (intensif).</p>', 'Munissez-vous de votre carte d’identité, des éventuelles attestations d’exonération, du permis de séjour d’une validité de plus de trois mois (étrangers hors CEE) et du montant du droit d’inscription (espèces ou bancontact).', '<ul><li>Jeunes de moins de 18 ans au 30 septembre de l’année en cours, sur attestation d’inscription dans une école de plein exercice.</li><li>Chômeurs complets indemnisés et demandeurs d’emplois, les droits d’inscription sont exigés, mais remboursés sur avis favorable du FOREM ou d’ACTIRIS.</li><li>Sur attestation du VDAB.</li><li>Personnes bénéficiant du revenu d’intégration sociale sur base d’une attestation originale du C.P.A.S.</li><li>Autres ayants droit légaux.</li></ul>', 'Certains étrangers sont soumis à un droit d’inscription complémentaire.', 2, NULL, NULL, 'uploaded/headers/11618webdeveloper.jpg'),
(11, 'Français langue étrangère', '', '', '', '', '', 3, NULL, NULL, ''),
(12, 'Anglais', '', '', '', '', '', 3, NULL, NULL, ''),
(13, 'Néerlandais', '', '', '', '', '', 3, NULL, NULL, ''),
(14, 'Allemand', '', '', '', '', '', 3, NULL, NULL, ''),
(15, 'Espagnol', '', '', '', '', '', 3, NULL, NULL, ''),
(16, 'Italien', '', '', '', '', '', 3, NULL, NULL, ''),
(17, 'Langue des signes', '', '', '', '', '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Langue des signes</span></p>', 3, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Structure de la table `cours_aside`
--

CREATE TABLE `cours_aside` (
  `id_cours` int(10) UNSIGNED NOT NULL,
  `ordre_texte` smallint(6) NOT NULL,
  `texte` text NOT NULL,
  `lien` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `cours_aside`
--

INSERT INTO `cours_aside` (`id_cours`, `ordre_texte`, `texte`, `lien`) VALUES
(1, 1, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Liste des cours</span></p>', 'uploaded/documents/27117TechComptaCours.jpg'),
(1, 2, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 1 | 183,20€</span></p>', 'uploaded/documents/34CTSS_Compta_1.pdf'),
(1, 3, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 2 | 160,20€</span></p>', 'uploaded/documents/6835CTSS_Compta_2.pdf'),
(2, 1, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Liste des cours</span></p>', 'uploaded/documents/5403SchemaUFElec.png'),
(2, 2, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 1 | 197,00€</span></p>', 'uploaded/documents/26296CTSS_Elec_1.pdf'),
(2, 3, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 2 | 146,40€</span></p>', 'uploaded/documents/31889CTSS_Elec_2.pdf'),
(2, 4, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 3 | 103,16€</span></p>', 'uploaded/documents/26614CTSS_Elec_3.pdf'),
(3, 1, '<p style="color: rgb(116, 115, 115); font-size: 14.4px;">Liste des cours</p><ul style="margin-bottom: 0px; color: rgb(116, 115, 115); font-size: 14.4px;"><li>Compétences entrepreneuriales</li><li>Création d\'entreprise</li><li>Aspects comptables, financiers et fiscaux</li><li>Gestion commerciale</li><li>Législation</li><li>Plan d\'entreprise</li></ul>', NULL),
(3, 2, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 1 | 72,80€</span></p>', 'uploaded/documents/13924Gestion.pdf'),
(4, 1, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Liste des cours</span></p>', 'uploaded/documents/14313TechInfoCours.jpg'),
(4, 2, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 1 | 198,84€</span></p>', 'uploaded/documents/31854CTSS_Info.pdf'),
(6, 1, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Liste des cours</span></p>', 'uploaded/documents/26132SchemaUFComp.png'),
(6, 2, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 1 | 252,60€</span></p>', 'uploaded/documents/16061BAC_Compta_1.pdf'),
(6, 3, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 2 | 275,40€</span></p>', 'uploaded/documents/31922BAC_Compta_2.pdf'),
(6, 4, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 3 | 233,60€</span></p>', 'uploaded/documents/1027BAC_Compta_3.pdf'),
(7, 1, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Liste des cours</span></p>', 'uploaded/documents/20685SchemaUFInfo.png'),
(7, 2, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 1 | 271,60€</span></p>', 'uploaded/documents/3458BAC_Info_1.pdf'),
(7, 3, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 2 | 279,20€</span></p>', 'uploaded/documents/3219BAC_Info_2.pdf'),
(7, 4, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 3 | 226,00€</span></p>', 'uploaded/documents/21712BAC_Info_3.pdf'),
(8, 1, '<p>Liste des cours</p>', 'uploaded/documents/28551BacMarketing.jpg'),
(8, 2, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 1 | 298,20€</span><br></p>', 'uploaded/documents/20148BAC_Market_1.pdf'),
(8, 3, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 2 | 283,00€</span></p>', 'uploaded/documents/2781BAC_Market_2.pdf'),
(8, 4, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 3 | 241,20€</span></p>', 'uploaded/documents/19403BAC_Market_3.pdf'),
(9, 1, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Liste des cours</span></p>', 'uploaded/documents/19356SchemaUFWebDes.png'),
(9, 2, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 1 | 340,00€</span></p>', 'uploaded/documents/27045BES_Web_Des_1.pdf'),
(9, 3, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 2 | 142,40€</span></p>', 'uploaded/documents/29050BES_Web_Des_2.pdf'),
(10, 1, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Liste des cours</span></p>', 'uploaded/documents/11563SchemaUFWebDev.png'),
(10, 2, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 1 | 264,00€</span></p>', 'uploaded/documents/1416BES_Web_Dev_1.pdf'),
(10, 3, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Horaire : Niveau 2 | 218,14€</span></p>', 'uploaded/documents/31521BES_Web_Dev_2.pdf'),
(11, 1, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Français langue étrangère</span></p>', 'uploaded/documents/23903FR_Signes.pdf'),
(12, 1, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Anglais</span></p>', 'uploaded/documents/25516Anglais.pdf'),
(13, 1, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Néerlandais</span></p>', 'uploaded/documents/2165Ndls_Allemand.pdf'),
(14, 1, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Allemand</span></p>', 'uploaded/documents/21918Ndls_Allemand.pdf'),
(15, 1, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Espagnol</span></p>', 'uploaded/documents/24303Esp_Ital.pdf'),
(16, 1, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Italien</span></p>', 'uploaded/documents/28795Esp_Ital.pdf'),
(17, 1, '<p><span style="transition-duration: 0.5s; font-size: 14.4px;">Langue des signes</span></p>', 'uploaded/documents/3736FR_Signes.pdf');

--
-- Déclencheurs `cours_aside`
--
DELIMITER $$
CREATE TRIGGER `insert_aside` BEFORE INSERT ON `cours_aside` FOR EACH ROW BEGIN
  SET @ot := (SELECT MAX(ordre_texte) FROM cours_aside WHERE id_cours = NEW.id_cours);
  SET NEW.ordre_texte = COALESCE(@ot+1, 1);
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `cours_niveau`
--

CREATE TABLE `cours_niveau` (
  `id_cours` int(10) UNSIGNED NOT NULL,
  `niveau` varchar(50) NOT NULL,
  `lien` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `cours_niveau`
--

INSERT INTO `cours_niveau` (`id_cours`, `niveau`, `lien`) VALUES
(1, '1', 'inconnu'),
(1, '2', 'inconnu'),
(2, '1', 'inconnu'),
(2, '2', 'inconnu'),
(3, '1', 'inconnu'),
(3, '2', 'inconnu'),
(4, '1', 'inconnu'),
(6, '1', 'inconnu'),
(6, '2', 'inconnu'),
(6, '3', 'inconnu'),
(7, '1', 'inconnu'),
(7, '2', 'inconnu'),
(7, '3', 'inconnu'),
(8, '1', 'superieur/BAC_Compta_1.pdf'),
(8, '2', 'superieur/BAC_Compta_2.pdf'),
(8, '3', 'superieur/BAC_Compta_3.pdf'),
(9, '1', 'inconnu'),
(9, '2', 'inconnu'),
(10, '1', 'inconnu'),
(10, '2', 'inconnu');

-- --------------------------------------------------------

--
-- Structure de la table `module`
--

CREATE TABLE `module` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `description` text,
  `jour` varchar(25) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `heure_debut` time NOT NULL,
  `heure_fin` time NOT NULL,
  `section` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `module`
--

INSERT INTO `module` (`id`, `nom`, `description`, `jour`, `date_debut`, `date_fin`, `heure_debut`, `heure_fin`, `section`) VALUES
(1, 'Bases de l’informatique et messagerie électronique (gmail)', 'Ce cours regroupe un bref récapitulatif des bases à connaître pour utiliser pleinement son ordinateur et son accès à Internet. Nous nous attarderons plus en détails sur la création et l’utilisation de la messagerie électronique et de ses fonctionnalités.', 'Vendredi', '2018-09-28', '2018-12-14', '09:00:00', '11:55:00', 4),
(2, 'Découverte de la tablette Androïd', 'Dans ce module, nous verrons comment utiliser une tablette ou un smartphone Androïd. Ce genre de bijou n’étant pas livré avec un mode d’emploi, en voici un ! Attention, le cours est basé sur Androïd 4+. Venez avec votre tablette ! Les Ipads ne seront pas vu ici.', 'Mardi', '2018-10-09', '2019-01-08', '09:00:00', '11:55:00', 4),
(3, 'Maintenance PC', 'Ou comment garder en bon état de marche mon ordinateur ? Une bonne maintenance de l’outil informatique permet de prévenir les pannes. Nous verrons aussi comment se protéger et sauver nos données numériques.', 'Jeudi', '2019-01-24', '2019-05-09', '09:00:00', '11:30:00', 4),
(4, 'Initiation à l\'impression 3D ', NULL, 'Lundi', '2019-02-11', '2019-05-06', '18:00:00', '21:45:00', 4),
(5, 'Découverte du MacBook', 'Découvrir les manipulations de base d\'une technologie informatique. Utilisation du clavier, de la souris; créer, manipuler et ranger des fichiers; clôture une session de travail.', 'Vendredi', '2019-01-11', '2019-02-15', '09:00:00', '11:55:00', 4),
(6, 'Maintenance Mac', 'Apprendre à créer les sauvegardes de votre machine ainsi qu\'installer correctement les mises à jour du logiciel d\'exploitation et de ses applications en toute sécurité.', 'Vendredi', '2019-02-22', '2019-04-05', '09:00:00', '11:55:00', 4),
(7, 'Utilisation iPad', 'Découvrir les fonctionnaliés de base de sa tablette Apple et l\'utiliser pour communiquer, chercher et collecter des informations en vue de les conserver et les partager.', 'Jeudi', '2019-01-10', '2019-03-28', '13:35:00', '16:30:00', 4),
(8, 'Internet avec Google Chrome', '<p>Comment s’y retrouver dans la masse d’information qu’est Internet ?&nbsp;</p><p>Comment naviguer efficacement sur l’océan du Web ? Dans ce module, nous partirons à la conquête du Monde. À travers l’utilisation de Chrome, le navigateur le plus rapide, nous verrons l’utilisation de cet outil de communication. Nous verrons également les bases des réseaux sociaux à travers Facebook et Twitter.</p>', 'Jeudi', '2019-01-10', '2019-02-21', '09:00:00', '11:55:00', 4),
(9, 'Windows 10', NULL, 'Mardi', '2019-01-29', '2019-04-30', '13:35:00', '16:30:00', 4),
(10, 'Introduction à Linux', NULL, 'Lundi', '2018-09-10', '2018-10-22', '09:00:00', '11:30:00', 4),
(11, 'Adobe Indesign : Affiches, flyers et revues à l’aspect professionnel', 'Adobe Indesign est le logiciel de P.A.O. (Publication Assistée par Ordinateur) de la célèbre suite d’Adobe. Il permet de réaliser des mises en page professionnelles, créatives et complexes. Création de cartes de visite, de magazines, de livres, de folders, de plaquettes, de journaux, etc … Voici les domaines de prédilection de ce logiciel puissant. Là où les programmes tels que Microsoft Word s’occupent de la gestion du contenu de documents, Indesign se concentre sur la forme, la mise en page des données.', 'Mardi', '2018-09-18', '2018-12-18', '18:00:00', '21:45:00', 4),
(12, 'Apprendre à programmer en donnant vie à des objets', '<p>Dans ce cours, vous allez apprendre à programmer de manière ludique en donnant vie à différents objets que vous ne pensiez peut-être pas pouvoir contrôler un jour. En effet, nous allons vous initier grâce à la réalisation de différents projets concrets et pratiques tels que l’allumage de leds, la construction et le téléguidage d’une voiture, la gestion d’une caméra orientable à distance, la détection des obstacles et des mouvements, etc.&nbsp;<span style="font-size: 1rem;">&nbsp;</span></p><p><span style="font-size: 1rem;">A la fin de ce module de formation, vous aurez eu un petit aperçu des possibilités infinies que vous offre la maîtrise d’un langage de programmation.</span></p>', 'Jeudi', '2018-09-06', '2019-01-31', '18:00:00', '21:45:00', 4),
(13, 'Office 2016 - Initiation', 'Dans ce cours, nous ferons un tour d’horizon des différentes applications d’Office 2016. Nous verrons comment gérer, utiliser et sauvegarder les différents fichiers produits sur l’ordinateur. Nous découvrirons ensemble Word et Excel à travers la création de projets complets et concrets.', 'Vendredi', '2018-09-28', '2019-02-08', '13:35:00', '16:30:00', 4),
(14, 'Word 2016 ou Exploiter un outil de traitement de texte - Base', '<p><ol><li>\r\n    Découvrir l’environnement Word, brève alphabétisation&nbsp;</li><li>Les opérations de base d’un traitement de texte&nbsp;</li><li>Rédiger correctement un courrier&nbsp;</li><li>Les listes à puces et numéros&nbsp;</li><li>La mise en page d’un document (mettre en colonnes, insérer une image)&nbsp;</li><li><span style="font-size: 1rem;">Les tableaux&nbsp;</span></li><li><span style="font-size: 1rem;">Utiliser un modèle prédéfini</span></li><li><span style="font-size: 1rem;">Utiliser les styles de mise en page, les tables des matières et notes de bas de pages</span></li></ol></p><p><span style="font-size: 1rem;">Finalité : concevoir un document imprimable (ex : courrier, rapport)</span></p>', 'Lundi', '2018-09-10', '2018-11-12', '18:00:00', '21:45:00', 4),
(15, 'Excel 2016 ou Exploiter un tableur - Base', '<p><ol><li>\r\n    Découvrir l’environnement Excel, appréhender une feuille de calcul&nbsp;</li><li>Faciliter la saisie de données et imprimer ses tableaux&nbsp;</li><li>Les formules dans une et plusieurs feuilles de calcul&nbsp;</li><li>Utiliser les fonctions&nbsp;</li><li>Utiliser les mises en forme automatiques et conditionnelles&nbsp;</li><li>Gérer les filtres, les tris, les sous-totaux&nbsp;</li></ol></p><p>Finalité : concevoir une facture calculée automatiquement</p>', 'Lundi', '2018-11-19', '2019-01-28', '18:00:00', '21:45:00', 4),
(16, 'Powerpoint 2016 - Base', 'Powerpoint permet de projeter des diaporamas composés de multiples diapositives avec texte et/ou photos, dessins, graphiques (utile lors de présentation d’un projet).', 'Lundi', '2019-02-04', '2019-04-29', '18:00:00', '21:45:00', 4),
(17, 'Excel 2016', '<p><ol><li>\r\n    Découvrir l’environnement Excel, appréhender une feuille de calcul&nbsp;<span style="font-size: 1rem;">&nbsp;</span></li><li><span style="font-size: 1rem;">Faciliter la saisie de données et imprimer ses tableaux&nbsp;</span></li><li><span style="font-size: 1rem;">Les formules dans une et plusieurs feuilles de calcul&nbsp;</span></li><li><span style="font-size: 1rem;">Utiliser les fonctions&nbsp;</span></li><li><span style="font-size: 1rem;">Utiliser les mises en forme automatiques et conditionnelles&nbsp;</span></li><li><span style="font-size: 1rem;">Gérer les filtres, les tris, les sous-totaux&nbsp;</span></li></ol></p><p><span style="font-size: 1rem;">Finalité : concevoir une facture calculée automatiquement</span></p>', 'Jeudi', '2019-01-10', '2019-03-14', '18:00:00', '21:45:00', 5),
(18, 'Office 2016', 'Dans ce cours, nous ferons interagir les programmes d’Office 2016. Nous découvrirons en détails l’annonce d’OFFICE qui suit: «Qu’allez-vous faire pendant les 365 prochains jours ? Avec Office, des possibilités infinies vous attendent cette année, et l’aventure commence dès maintenant !» Tout un programme qui nous attend en Word, Excel et PowerPoint.', 'Vendredi', '2019-02-15', '2019-05-17', '13:35:00', '16:30:00', 5);

-- --------------------------------------------------------

--
-- Structure de la table `page`
--

CREATE TABLE `page` (
  `nom` varchar(150) NOT NULL,
  `contenu` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `page`
--

INSERT INTO `page` (`nom`, `contenu`) VALUES
('IFOSUP - Promotion sociale - Wavre', '<h2>Cours du soir et de jour&nbsp;</h2><p>L’IFOSUP c’est :&nbsp;</p><p><ul><li>3 bacheliers : informatique de gestion, comptabilité et marketing;&nbsp;</li><li>2 brevets d’enseignement supérieur : Webdeveloper et Webdesigner;&nbsp;</li><li>4 sections secondaires : Technicien en comptabilité, Technicien électricien-automaticien, Technicien en informatique et Connaissances de gestion de base;&nbsp;</li><li>Des langues : Allemand, Anglais, Néerlandais, Italien, Espagnol, Français pour non-francophones et langue des signes;&nbsp;</li><li>Des modules informatiques dans des domaines variés;</li></ul></p>'),
('Inscription à l\'IFOSUP', '<h2>Année académique 2018-2019</h2><ul><li>Les inscriptions sont ouvertes du 1er au 22 juin 2018, de 10h00 à 12h00 et de 17h00 à 19h00 (du lundi au vendredi).&nbsp;</li><li>Les inscriptions reprendront ensuite à partir du lundi 27 août 2018, de 16h30 à 19 heures.&nbsp;</li><li>La reprise des cours est fixée au lundi 3 septembre 2018.&nbsp;</li><li>Les cours se donnent du lundi au vendredi, entre 18h00 et 21h45 et le samedi matin, entre 8h30 et 13h05; certains cours se donnent en journée.&nbsp;</li></ul><h3>Documents utiles&nbsp;</h3><p>Munissez-vous de votre carte d’identité, des éventuelles attestations de réussite et/ou d’exonération, du certificat d’enseignement secondaire supérieur (CESS) pour les Bacheliers et BES, du permis de séjour d’une validité de plus de trois mois (étrangers hors CEE) et du montant du droit d’inscription (espèces ou paiement par Bancontact).&nbsp;</p><h3>Droits d\'inscriptions&nbsp;</h3><h4>Enseignement supérieur :&nbsp;</h4><p>Un forfait de 26,00 €/étudiants + 0,38 € par période de cours jusqu’à la 800e période (maximum de 322,00 €/an).&nbsp;</p><h4>Enseignement secondaire :&nbsp;</h4><p>Un forfait de 26,00 €/étudiants + 0,23 € par période de cours jusqu’à la 800e période (maximum de 210,00 €/an). Exemples: un cours de langue de 120 périodes: 53,60 € deux cours de langues: 81,20 € (hors frais d\'inscription).</p><h4>&nbsp;Exemptions :&nbsp;</h4><ul><li>Jeunes de moins de 18 ans au 30 septembre de l’année en cours, sur attestation d’inscription dans une école de plein exercice.&nbsp;</li><li>Chômeurs complets indemnisés et demandeurs d’emplois, les droits d’inscription sont exigés, mais remboursés sur avis favorable du FOREM ou d’ACTIRIS.&nbsp;</li><li>Sur attestation du VDAB.&nbsp;</li><li>Personnes bénéficiant du revenu d’intégration sociale sur base d’une attestation originale du C.P.A.S. datée du premier jour de la formation.&nbsp;</li><li>Autres ayants droit légaux.&nbsp;</li></ul><h4>Observation :&nbsp;</h4><p>Conformément à la circulaire 4652 du 5/12/2013, certains étudiants étrangers sont soumis à un droit d’inscription spécifique (DIS).</p><h3>Admission des étudiants&nbsp;</h3><h4>Enseignement Supérieur&nbsp;</h4><p><span style="font-size: 1rem;">Pour être admis comme étudiant(e) régulier(ère) dans l’enseignement supérieur de type court, l’étudiant(e) doit fournir le certificat d’enseignement secondaire supérieur (CESS). Est également admissible l’étudiant(e) qui réussit une épreuve d’admission (pour bénéficier de cette possibilité, l’étudiant(e) doit déposer son dossier personnel auprès de la Direction). Des dispenses peuvent être accordées par le Conseil des études.</span></p><p><span style="font-size: 1rem;">Attention: notre école ne permet pas aux étudiants hors CEE d’obtenir un permis de séjour, sauf pour le français langue étrangère (intensif/480 périodes étalées sur les 40 semaines de l’année scolaire).&nbsp;</span><br></p><h4>Formations en langues&nbsp;<br></h4><p>Un test de niveau est organisé et permet l’accès direct au cours correspondant; sauf si l’étudiant présente une attestation de réussite émanant d’un établissement de promotion sociale.&nbsp;</p><h4>Unités d’Enseignement (U.E.)&nbsp;</h4><p>Une vérification des éventuels prérequis est organisée en fonction de la spécificité de l’unité d’enseignement en début de formation.</p><h4>Conditions d’admission des mineurs d’âge&nbsp;</h4><p>Être âgé(e) de 15 ans et avoir suivi deux années dans l’enseignement secondaire de plein exercice (sur présentation d’une attestation d’inscription dans cet établissement pour l’année scolaire considérée).&nbsp;</p><h4>Valeur d’une période</h4><p>1 période = 50 minutes de cours&nbsp;</p><h3>Certifications&nbsp;</h3><h4>Enseignement supérieur&nbsp;</h4><p>Obtient le diplôme, l’étudiant(e) qui a obtenu au moins 50% dans chaque U.E. et 50% à l’épreuve intégrée.&nbsp;</p><p>Des travaux de fin d’études sont réalisés dans le but d’apprécier, en collaboration avec un jury de praticiens extérieurs, la qualification professionnelle de l’étudiant(e).&nbsp;</p><h4>Enseignement secondaire supérieur&nbsp;</h4><p>Obtient le certificat, l’étudiant(e) qui a obtenu au moins 50% dans chaque U.E. et 50% à l’épreuve intégrée.&nbsp;</p><p>Des épreuves pratiques et travaux de fin d’études sont réalisés dans le but d’apprécier, en collaboration avec un jury de praticiens extérieurs, la qualification professionnelle de l’étudiant(e).&nbsp;\r\n</p><h4>Unités d’enseignement&nbsp;</h4><p>Obtient l’attestation de réussite, l’étudiant(e) qui atteint au moins 50%.&nbsp;</p><p>Toutes nos formations sont modularisées. Il est donc possible de ne s’inscrire que dans les cours (UE) souhaités sans devoir suivre l’ensemble de la section pour autant que l’on dispose des prérequis.</p>'),
('Les principaux objectifs de l\'IFOSUP sont de :', '<p></p><ul><li>concourir à l\'épanouissement individuel des personnes qui entrent en formation en promouvant leur insertion professionnelle, sociale, scolaire et culturelle;&nbsp;</li><li>de répondre aux besoins et demandes en formation émanant des entreprises, des administrations, de l\'enseignement et d\'une manière générale des milieux socio-économiques et culturels.&nbsp;</li></ul><p></p><p>Ses activités s\'articulent donc autour de deux pôles: celui du développement et celui des besoins de la société.&nbsp;</p><p>Son offre de formation est <a href="http://localhost/projet_ifosup/pages/horaires">organisée en journée ou en soirée</a>, elle est de niveau secondaire ou supérieur.&nbsp;</p><p>Par ses activités, l\'IFOSUP s\'inscrit dans une réelle dynamique de formation tout au long de la vie permettant de cumuler études et emploi ou recherche d\'emploi.&nbsp;</p><p>Les structures d\'organisation <a href="http://localhost/projet_ifosup/pages/formations">des formations proposées</a>&nbsp;aujourd\'hui se sont assouplies et adaptées au fil du temps.&nbsp;</p><p>De nos jours, l\'adulte en formation continuée construit lui-même son cursus, l\'étalant selon ses possibilités sur une soirée ou davantage et le poursuit quelques semaines ou quelques années au gré de ses disponibilités.</p>'),
('Nos formations', NULL),
('Notre métier ? Accompagner votre réussite', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `section`
--

CREATE TABLE `section` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `section_mere` int(10) UNSIGNED DEFAULT NULL,
  `description` text NOT NULL,
  `header_path` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `section`
--

INSERT INTO `section` (`id`, `nom`, `section_mere`, `description`, `header_path`) VALUES
(1, 'Sections du secondaire', NULL, '<p>Chaque section de l’enseignement secondaire de promotion sociale s’accompagne d’un profil professionnel élaboré en collaboration avec des experts du monde socio-économique.&nbsp;</p><p>Les formations de “Technicien en comptabilité” et “Technicien électricien-automaticien” sont des sections du secondaire supérieur menant à la délivrance d’un certificat de qualification spécifique à la Promotion Sociale.&nbsp;</p><p>Ce certificat n’est en aucun cas l’équivalent du CESS (Certificat d’Enseignement Secondaire Supérieur); cependant, il ouvre l’accès à une année complémentaire permettant l’obtention de celui-ci.</p>', 'uploaded/headers/28607etudes.jpg'),
(2, 'Sections du supérieur', NULL, '<h4>Bachelier&nbsp;</h4><p>Le grade de bachelier est décerné aux étudiants qui :&nbsp;</p><p></p><ul><li>ont acquis des connaissances approfondies et des compétences dans un domaine de travail ou d’études qui fait suite à et se fonde sur une formation de niveau d’enseignement secondaire supérieur. Ce domaine se situe à un haut niveau de formation basé, entre autres, sur des publications scientifiques ou des productions artistiques ainsi que sur des savoirs issus de la recherche et de l’expérience;&nbsp;</li><li>sont capables d’appliquer, de mobiliser, d’articuler et de valoriser ces connaissances et ces compétences dans le cadre d’une activité socio-professionnelle ou de la poursuite d’études et ont prouvé leur aptitude à élaborer et à développer dans leur domaine d’études des raisonnements, des argumentations et des solutions à des problématiques;&nbsp;</li><li>sont capables de collecter, d’analyser et d’interpréter, de façon pertinente, des données généralement, dans leur domaine d’études en vue de formuler des opinions, des jugements critiques ou des propositions artistiques qui intègrent une réflexion sur des questions sociétales, scientifiques, techniques, artistiques ou éthiques;&nbsp;</li><li>sont capables de communiquer, de façon claire et structurée, à des publics avertis ou non, des informations, des idées, des problèmes et des solutions, selon les standards de communication spécifiques au contexte;&nbsp;</li><li>ont développé les stratégies d’apprentissage qui sont nécessaires pour poursuivre leur formation avec un fort degré d’autonomie.&nbsp;</li></ul><p></p><h4>&nbsp;Le brevet de l’enseignement supérieur (BES) est décerné aux étudiants qui :&nbsp;</h4><p>Le brevet de l’enseignement supérieur (BES) est décerné aux étudiants qui :&nbsp;</p><p></p><ul><li>ont acquis des connaissances théoriques et des compétences pratiques diversifiées dans un champ professionnel donné qui fait suite à et se fonde sur une formation de niveau d’enseignement secondaire supérieur. Ce champ professionnel est basé, entre autres, sur des publications scientifiques ou des productions artistiques ainsi que sur des savoirs issus de l’expérience;&nbsp;</li><li>sont capables d’indépendance dans la gestion de projets qui demandent la résolution de problèmes incluant de nombreux facteurs dont certains interagissent et sont sources de changements imprévisibles et de développer un savoir-faire tel qu’ils peuvent produire des réponses stratégiques et créatives dans la recherche de solutions à des problèmes concrets et abstraits bien définis;&nbsp;</li><li>sont capables de collecter, d’analyser et d’interpréter, de façon pertinente, des données exclusivement dans leur domaine d’études en vue de formuler des opinions, des jugements critiques ou des propositions artistiques qui intègrent une réflexion sur des questions techniques, artistiques ou éthiques;&nbsp;</li><li>sont capables de transmettre des idées de façon structurée et cohérente en utilisant des informations qualitatives et quantitatives;&nbsp;</li><li>sont capables d’identifier leurs besoins d’apprentissage nécessaire à la poursuite de leur parcours de formation.\r\n</li></ul><p></p>', 'uploaded/headers/4574EtudesSup.jpg'),
(3, 'Langues', NULL, '<p>Placée au coeur de la mosaïque des états européens, la Belgique occupe une place privilégiée. La maîtrise des langues est un moyen incomparable d’accéder pleinement à la richesse de la culture européenne.&nbsp;</p><p>L’IFOSUP vous offre la possibilité d’étudier ces langues. La première admission en langue se fait sur test (sauf présentation d’une attestation de réussite émanant d’un établissement de promotion sociale).</p>', 'uploaded/headers/29792languages.jpg'),
(4, 'Découverte - Initiation', NULL, '<p></p><hr><p></p>', 'uploaded/headers/28210decouverte.jpg'),
(5, 'Perfectionnement', NULL, '', 'uploaded/headers/12387perfection.jpg'),
(6, 'Nos services aux entreprises', NULL, '<h3>Quelle formule?&nbsp;</h3><ol><li><span style="font-size: 1rem;">Optez pour un module dédié au personnel de votre entreprise (8 à 12 participants).&nbsp;</span></li><li><span style="font-size: 1rem;">Optez pour un module inter-entreprises (8 à 12 participants) et partagez le coût de la formation.&nbsp;</span></li></ol><h3>Quel domaine ?&nbsp;</h3><ul><li><span style="font-size: 1rem;">Gestion des ressources humaines et management.</span></li><li><span style="font-size: 1rem;">Bureautique - Office 2016.&nbsp;</span></li><li><span style="font-size: 1rem;">Vente et communication.</span></li><li><span style="font-size: 1rem;">Langues.&nbsp;</span></li></ul><h3>Quel prix ?&nbsp;</h3><p><span style="font-size: 1rem;">En tant qu’établissement de promotion sociale, nous avons la possibilité d’établir des « conventions » avec les entreprises partenaires.&nbsp;</span></p><p><span style="font-size: 1rem;">Le prix de la formation est fixé directement par la Fédération Wallonie Bruxelles et dépend de la durée du module (les tarifs sont indexés chaque année en juin).&nbsp;</span></p><p><span style="font-size: 1rem;">Le tarif est fixe, quel que soit le nombre de participants.</span><br></p>', 'uploaded/headers/8174ServiceEntreprise.jpg');

--
-- Déclencheurs `section`
--
DELIMITER $$
CREATE TRIGGER `prevent_delete` BEFORE DELETE ON `section` FOR EACH ROW BEGIN
  SIGNAL SQLSTATE '45000' 
  SET MESSAGE_TEXT = 'les entrées de cette table ne peuvent pas être supprimées';
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `prevent_update_langues` BEFORE UPDATE ON `section` FOR EACH ROW BEGIN
  IF new.id = 3 THEN
    SET new.nom = 'Langues';
  END IF;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `mdp` char(128) NOT NULL,
  `email` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `mdp`, `email`) VALUES
(1, 'df6b9fb15cfdbb7527be5a8a6e39f39e572c8ddb943fbc79a943438e9d3d85ebfc2ccf9e0eccd9346026c0b6876e0e01556fe56f135582c05fbdbb505d46755a', 'azerty@mail.com');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `cours`
--
ALTER TABLE `cours`
  ADD PRIMARY KEY (`id`),
  ADD KEY `section` (`section`);

--
-- Index pour la table `cours_aside`
--
ALTER TABLE `cours_aside`
  ADD PRIMARY KEY (`id_cours`,`ordre_texte`);

--
-- Index pour la table `cours_niveau`
--
ALTER TABLE `cours_niveau`
  ADD PRIMARY KEY (`id_cours`,`niveau`);

--
-- Index pour la table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `section` (`section`);

--
-- Index pour la table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`nom`);

--
-- Index pour la table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `section_mere` (`section_mere`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `cours`
--
ALTER TABLE `cours`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `module`
--
ALTER TABLE `module`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `cours`
--
ALTER TABLE `cours`
  ADD CONSTRAINT `cours_ibfk_1` FOREIGN KEY (`section`) REFERENCES `section` (`id`);

--
-- Contraintes pour la table `cours_aside`
--
ALTER TABLE `cours_aside`
  ADD CONSTRAINT `cours_aside_ibfk_1` FOREIGN KEY (`id_cours`) REFERENCES `cours` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `cours_niveau`
--
ALTER TABLE `cours_niveau`
  ADD CONSTRAINT `cours_niveau_ibfk_1` FOREIGN KEY (`id_cours`) REFERENCES `cours` (`id`);

--
-- Contraintes pour la table `module`
--
ALTER TABLE `module`
  ADD CONSTRAINT `module_ibfk_1` FOREIGN KEY (`section`) REFERENCES `section` (`id`);

--
-- Contraintes pour la table `section`
--
ALTER TABLE `section`
  ADD CONSTRAINT `section_ibfk_1` FOREIGN KEY (`section_mere`) REFERENCES `section` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
