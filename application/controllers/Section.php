<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Section extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('SectionModel');
    }

    public function listeSections(){
        //vérification si user connecté
        session_start();
        if(isset($_SESSION['connected'])) {
            $data['sections'] = $this->SectionModel->getSections();
            $data['pages'] = 'pages/sectionAdmin';
            $this->load->view('templates/admin', $data);
        }
        else{
            //il n'était pas connecté
            redirect('/');
        }
    }

    public function updateSection(){
        //vérification si user connecté
        session_start();
        if(isset($_SESSION['connected'])) {
            //vérification si GET ou POST
            if(isset($_POST['id'])){
                $id = $_POST['id'];
                $tmp = new stdClass();
                $tmp->nom = $_POST['nom'];
                if(isset($_FILES['header']) && $_FILES['header']['size'] > 0){
                    $upload_dir = "uploaded/headers";
                    if (!file_exists($upload_dir)) {
                        mkdir($upload_dir, 0777, true);
                    }
                    $filepath = $upload_dir . '/' . rand() . $_FILES['header']['name'];
                    if (move_uploaded_file($_FILES['header']["tmp_name"], $filepath)) {
                        $tmp->header_path = $filepath;
                    }
                    else{
                        $tmp->header_path = '...';
                    }
                }
                else{
                    $tmp->header_path = '...';
                }
                $tmp->description = $_POST['description'];
                $header_to_delete = $this->SectionModel->updateSection($id, $tmp);
                http_response_code(200);
                if(count($header_to_delete) === 1){
                    $path = $header_to_delete[0]->header_path;
                    if($path !== '...' && $path != ''){
                        unlink($path);
                    }
                }
            }
            else{
                //demande d'accès au formulaire de mise à jour
                $id = $this->uri->segment(3);
                if($id){
                    $tmp = $this->SectionModel->getSection($id);
                    $data['section'] = $tmp[0];
                    $data['pages']= 'pages/updateSection';
                    $this->load->view("templates/admin", $data);
                }
                else{
                    http_response_code(400);
                }
            }
        }
        else{
            //il n'était pas connecté
            redirect('/');
        }
    }

}
