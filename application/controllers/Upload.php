<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {
    function image(){
        $image = $_FILES['fileToUpload'];
        $upload_dir = 'uploaded/image';
        if (!file_exists($upload_dir)) {
            mkdir($upload_dir, 0777, true);
        }
        if (move_uploaded_file($image["tmp_name"], $upload_dir.'/'.$image['name'])){
            $tmp = json_encode(array(
                    'success' => true,
                    'url' => base_url().$upload_dir.'/'.$image['name']
                )
            );
            echo ($tmp);
        }
        else{
             http_response_code(500);
        }
    }
}