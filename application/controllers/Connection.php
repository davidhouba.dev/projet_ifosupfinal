<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Connection extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('ConnectionModel');
    }

    public function login()
    {

        session_start();
        $errors = array();

        //vérification si l'utilisateur était connecté précedemment
        if(isset($_SESSION['connected'])){
            $this->load->view('panelAdmin');
        }
        //l'utilisateur n'était pas connecté précedemment
        else{
            //Réception des identifiants
            if (isset($_POST['login_user'])) {

                $password = $_POST['password'];
                $email = $_POST['email'];

                // vérification si email ou password est vide
                if (empty($email)) {
                    array_push($errors, "email is required");
                }
                if (empty($password)) {
                    array_push($errors, "Password is required");
                }

                if (count($errors) == 0) {
                    if ($this->ConnectionModel->isUser($email, $password)) {
                        //permettra de savoir si l'utilisateur aura déjà effectué une connexion
                        $_SESSION['connected'] = true;
                        $this->load->view('panelAdmin');
                    } else {
                        array_push($errors, "Wrong email/password combination");
                        $data = array('errors' => $errors);
                        $this->load->view('login', $data);
                    }
                }
            } else {
                $data = array('errors' => $errors);
                $this->load->view('login', $data);
            }
        }
    }

	public function logout(){
        session_start();
		session_destroy();
        if(isset($_COOKIE[session_name()])):
            setcookie(session_name(), '', time()-7000000, '/');
        endif;
        if(isset($_COOKIE['login_user'])):
            setcookie('login_user', '', time()-7000000, '/');
        endif;
        redirect('/');
    }
}