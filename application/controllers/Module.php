<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Module extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('ModuleModel');
        $this->load->model('SectionModel');
    }

    public function listeModules(){
        //vérification si user connecté
        session_start();
        if($_SESSION['connected']){
            $modules = $this->ModuleModel->getModules();
            $sections = $this->SectionModel->getSections();
            $data['modules'] = $modules;
            $data['sections'] = $sections;
            $data['pages'] = 'pages/modules';
            $this->load->view('templates/admin', $data);
        }
        else{
            redirect('/');
        }
    }

    public function ajouteModule(){
        session_start();
        if($_SESSION['connected']){
            if(isset($_POST['nom']) &&
                isset($_POST['jour']) &&
                isset($_POST['date_debut']) &&
                isset($_POST['date_fin']) &&
                isset($_POST['heure_debut']) &&
                isset($_POST['heure_fin']) &&
                isset($_POST['section'])){
                $module = new stdClass();
                $module->nom = $_POST['nom'];
                $module->jour = $_POST['jour'];
                switch ($module->jour){
                    case 0:
                        $module->jour = 'Lundi';
                        break;
                    case 1:
                        $module->jour = 'Mardi';
                        break;
                    case 2:
                        $module->jour = 'Mercredi';
                        break;
                    case 3:
                        $module->jour = 'Jeudi';
                        break;
                    case 4:
                        $module->jour = 'Vendredi';
                        break;
                    case 5:
                        $module->jour = 'Samedi';
                        break;
                    case 6:
                        $module->jour = 'Dimanche';
                        break;
                    default:
                        $module->jour = 'Lundi';
                        break;
                }
                $module->date_debut = $_POST['date_debut'];
                $module->date_fin = $_POST['date_fin'];
                $module->heure_debut = $_POST['heure_debut'];
                $module->heure_fin = $_POST['heure_fin'];
                $module->section = $_POST['section'];
                $module->description = isset($_POST['description']) ? $_POST['description'] : null;
                $result = $this->ModuleModel->ajouteModule($module);
                if($result){
                    http_response_code(200);
                }
                else{
                    http_response_code(500);
                }
            }
            else{
                http_response_code(400);
            }
        }
        else{
            redirect('/');
        }
    }

    public function deleteModule(){
        session_start();
        if($_SESSION['connected']){
            if(isset($_POST['id'])){
                $module = new stdClass();
                $module->id = $_POST['id'];
                $this->ModuleModel->deleteModule($module);
                http_response_code(200);
            }
            else{
                http_response_code(400);
            }
        }
        else{
            redirect('/');
        }
    }

    public function updateModule(){
        session_start();
        if($_SESSION['connected']){
            if(isset($_POST['id'])){
                $id = $_POST['id'];
                $module = new stdClass();
                $module->nom = $_POST['nom'];
                $module->description = $_POST['description'];
                $module->jour = $_POST['jour'];
                switch ($module->jour){
                    case 0:
                        $module->jour = 'Lundi';
                        break;
                    case 1:
                        $module->jour = 'Mardi';
                        break;
                    case 2:
                        $module->jour = 'Mercredi';
                        break;
                    case 3:
                        $module->jour = 'Jeudi';
                        break;
                    case 4:
                        $module->jour = 'Vendredi';
                        break;
                    case 5:
                        $module->jour = 'Samedi';
                        break;
                    case 6:
                        $module->jour = 'Dimanche';
                        break;
                    default:
                        $module->jour = 'Lundi';
                        break;
                }
                $module->date_debut = $_POST['date_debut'];
                $module->date_fin = $_POST['date_fin'];
                $module->heure_debut = $_POST['heure_debut'];
                $module->heure_fin = $_POST['heure_fin'];
                $module->section = $_POST['section'];
                $module->description = isset($_POST['description']) ? $_POST['description'] : null;
                $this->ModuleModel->updateModule($id, $module);
                http_response_code(200);
            }
            else{
                $id = $this->uri->segment(3);
                if($id){
                    $module = $this->ModuleModel->getModule($id);
                    $module = $module[0];
                    switch ($module->jour){
                        case 'Lundi':
                            $module->jour = 0;
                            break;
                        case 'Mardi':
                            $module->jour = 1;
                            break;
                        case 'Mercredi':
                            $module->jour = 2;
                            break;
                        case 'Jeudi':
                            $module->jour = 3;
                            break;
                        case 'Vendredi':
                            $module->jour = 4;
                            break;
                        case 'Samedi':
                            $module->jour = 5;
                            break;
                        case 'Dimanche':
                            $module->jour = 6;
                            break;
                        default:
                            $module->jour = 0;
                            break;
                    }
                    $sections = $this->SectionModel->getSections();
                    $data['module'] = $module;
                    $data['sections'] = $sections;
                    $data['pages'] = 'pages/updateModule';
                    $this->load->view('templates/admin', $data);
                }
                else{
                    http_response_code(400);
                }
            }
        }
        else{
            redirect('/');
        }
    }

}