<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PagesAdmin extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('PagesModel');
    }

    public function listePages(){
        //vérification section
        session_start();
        if($_SESSION['connected']){
            $pages = $this->PagesModel->getListePages();
            $data['pagesListe'] = $pages;
            $data['pages'] = 'pages/pagesAdmin';
            $this->load->view('templates/admin', $data);
        }
        else{
            redirect('/');
        }
    }

    public function updatePage(){
        //vérification section
        session_start();
        if(isset($_SESSION['connected'])){
            //vérification si GET ou POST
            if(isset($_POST['nom'])){
                $nom = $_POST['nom'];
                $old_nom = $_POST['oldnom'];
                $contenu = $_POST['contenu'];
                $this->PagesModel->updatePage($old_nom,$nom,$contenu);
                http_response_code(200);

            }
            else{
                //demande d'accès au formulaire de mise à jour
                $nom = $this->uri->segment(3);
                if($nom){
                    $nom = rawurldecode($nom);
                    $tmp = $this->PagesModel->getPage($nom);
                    $data['page'] = $tmp[0];
                    $data['pages']= 'pages/updatePage';
                    $this->load->view("templates/admin", $data);
                }
                else{
                    http_response_code(400);
                }
            }
        }
        else{
            http_response_code(403);
        }
    }


}