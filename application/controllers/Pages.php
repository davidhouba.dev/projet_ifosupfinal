<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('GetSections','modelB');
		$this->load->model('GetModule','modelC');
		$this->load->model('GetCours','modelE');
		$this->load->model('GetPage','modelD');
		$this->load->model('Ariane','modelZ');
		$this->load->model('GetCoursAside','modelY');

	}



	public function index(){
		$data['pages']='pages/index';
		$data['data']='';
		$data['couleur'] = 'navy';
		$data['query'] = $this->modelD->afficheIndex();
		$this->load->view('templates/master',$data);
	}
	public function inscriptions(){
		$data['pages']='pages/inscriptions';
		$data['data']='';
		$data['couleur'] = 'green';
		$data['afficheInscription'] = $this->modelD->afficheInscription();
		$this->load->view('templates/master',$data);
	}
	public function roi(){
		$data['pages']='pages/roi';
		$data['data']='';
		$this->load->view('templates/master',$data);
	}
	public function valorisation(){
		$data['pages']='pages/valorisation';
		$data['data']='';
		$this->load->view('templates/master',$data);
	}
	public function ensinclusif(){
		$data['pages']='pages/ensinclusif';
		$data['data']='';
		$this->load->view('templates/master',$data);
	}
	public function qualite(){
		$data['pages']='pages/qualite';
		$data['data']='';
		$this->load->view('templates/master',$data);
	}
	public function horaires(){
		$data['pages']='pages/horaires';
		$data['data']='';
		$data['couleur'] = 'yellow';
		$this->load->view('templates/master',$data);
	}
	public function congesScolaires(){
		$data['pages']='pages/congesScolaires';
		$data['data']='';
		$this->load->view('templates/master',$data);
	}
	public function acces(){
		$data['pages']='pages/acces';
		$data['data']='';
		$data['couleur'] = 'blue';
		$this->load->view('templates/master',$data);
	}
	public function liens(){
		$data['pages']='pages/liens';
		$data['data']='';
		$this->load->view('templates/master',$data);
	}
	public function section(){

		$data['pages']= 'pages/section';
		$data['data']='';
		$data['couleur'] = 'yellow';
		if(isset($_GET['idSec'])){
			$idSec = $_GET['idSec'];
			if($idSec < 3){
				$data['affiche_section'] = $this->modelB->affiche_section($idSec);
				$data['affiche_cat'] = $this->modelZ->affiche_cat($idSec);
				$data['afficheCours'] = $this->modelE->afficheCours();
				$this->load->view('templates/master',$data);
			}else{
				$data['pages'] = 'errors/error404';
				$this->load->view('templates/master', $data);
			}
		}else{
			$this->load->view('errors/error404');
		}


	}
		public function formations(){
		$data['pages']='pages/formations';
		$data['data']='';
		$data['couleur'] = 'blue';
		$data['afficheNosFormations'] = $this->modelD->afficheNosFormations();
		$data['afficheTitreSecondaire'] = $this->modelB->afficheTitreSecondaire();
		$data['afficheTitreSuperieur'] = $this->modelB->afficheTitreSuperieur();
		$data['afficheTitreLangue'] = $this->modelB->afficheTitreLangue();
		$data['afficheLiensSecondaires'] = $this->modelE->afficheLiensSecondaires();
		$data['afficheLiensSuperieurBac'] = $this->modelE->afficheLiensSuperieurBac();
		$data['afficheLiensSuperieurBes'] = $this->modelE->afficheLiensSuperieurBes();
		$data['afficheDecouverte'] = $this->modelB->afficheDecouverte();
		$data['affichePerfection'] = $this->modelB->affichePerfection();
		$data['afficherLiensLangues'] = $this->modelY->afficherLiensLangues();
		$this->load->view('templates/master',$data);
	}

	public function decouverte(){
		$data['pages'] ='pages/ModulesInformatiques/decouverte';
		$data['data']='';
		$data['couleur'] = 'green';
		$data['afficheDecouverte'] = $this->modelB->afficheDecouverte();
		$data['afficheModuleDecouverte'] = $this->modelC->afficheModuleDecouverte();
		$data['afficheModuleDecouverteDescription'] = $this->modelC->afficheModuleDecouverteDescription();
		$nbr_col1 = ceil(count($data['afficheModuleDecouverte'])/2);
		$nbr_col2 = floor(count($data['afficheModuleDecouverte'])/2);
		$data['nbr_col1'] = $nbr_col1;
		$data['nbr_col2'] = $nbr_col2;
		$idSec = $_GET['idSec'];
		$data['affiche_cat'] = $this->modelZ->affiche_Mod($idSec);
		$this->load->view('templates/master',$data);
	}
	public function perfection(){
		$data['pages'] ='pages/ModulesInformatiques/perfection';
		$data['data']='';
		$data['couleur'] = 'green';
		$data['affichePerfection'] = $this->modelB->affichePerfection();
		$data['afficheModule'] = $this->modelC->afficheModulePerfection();
		$idSec = $_GET['idSec'];
		$data['affiche_cat'] = $this->modelZ->affiche_Mod($idSec);
		$this->load->view('templates/master',$data);
	}
	public function cours_front(){
		$data['pages']='pages/cours_front';
		$data['data']='';
		$data['couleur'] = 'yellow';
		$affiche_cours = $this->modelE->max();
		if(isset($_GET['idCours'])){
			$idCours = $_GET['idCours'];
			if($idCours < $affiche_cours[0]['id']){
				$data['affiche_cours_front'] = $this->modelE->affiche_cours_front($idCours);
				$data['affiche_cours'] = $this->modelZ->affiche_cours($idCours);
				$data['affiche_aside'] = $this->modelY->afficherAside($idCours);
				$this->load->view('templates/master',$data);
			}else{
				$data['pages'] = 'errors/error404';
				$this->load->view('templates/master', $data);
			}
		}else{
			$data['pages'] = 'errors/error404';
			$this->load->view('templates/master', $data);
		}

	}

	public function serviceentreprise(){
		$data['pages']='pages/serviceentreprise';
		$data['data']='';
		$data['couleur'] = 'yellow';
		$data['afficheServiceEntreprise'] = $this->modelB->afficheServicesEntreprise();
		$idSec = $_GET['idSec'];
		$data['affiche_cat'] = $this->modelZ->affiche_Ser($idSec);
		$this->load->view('templates/master',$data);
	}
	public function langues() {
		$data['pages']='pages/langues';
		$data['data']='';
		$data['couleur'] = 'blue';
		$data['afficheLangue'] = $this->modelB->afficheLangue();
		$idSec = $_GET['idSec'];
		$data['affiche_cat'] = $this->modelZ->affiche_cat($idSec);
		$data['afficherLiensLangues'] = $this->modelY->afficherLiensLangues();
		$this->load->view('templates/master',$data);
	}
}
