<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cours extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('CoursModel');
        $this->load->model('SectionModel');
        $this->load->model('CoursAsideModel');
        $this->load->model('UtilsDBModel');
    }

    public function listeCours(){
        session_start();
        //vérification si user connecté
        if(isset($_SESSION['connected'])){
            $result = $this->CoursModel->getCours();
            $result2 = $this->SectionModel->getSections();
            //vérification si l'appel à la DB s'est bien passé
            if($result && $result2) {
                $tmp['data']['cours'] = $result;
                $tmp['data']['sections'] = $result2;
                $tmp['pages'] = 'pages/cours';
            } else {
                http_response_code(500);
            }
            $this->load->view('templates/admin', $tmp);
        //sinon on renvoie à l'accueil
        } else {
            redirect('/');
        }

    }

    public function ajouteCours(){

        session_start();
        //vérification si user connecté
        if(isset($_SESSION['connected'])){
            //vérification si les champs obligatoires sont présents
            if (isset($_POST['nom']) && isset($_POST['section']) && isset($_FILES['header'])){
                $upload_dir = "uploaded/headers";
                if (!file_exists($upload_dir)) {
                    mkdir($upload_dir, 0777, true);
                }
                $filepath = $upload_dir . '/' . rand() . $_FILES['header']['name'];
                if (move_uploaded_file($_FILES['header']["tmp_name"], $filepath)) {
                    $nom = $_POST['nom'];
                    $section = $_POST['section'];
                    //attribution de valeurs pas défauts pour les champs non obligatoires
                    $descritpion = isset($_POST['description']) ? $_POST['description'] : null;
                    $pre_requis = isset($_POST['pre_requis']) ? $_POST['pre_requis'] : null;
                    $documents_utiles = isset($_POST['documents_utiles']) ? $_POST['documents_utiles'] : null;
                    $exemption = isset($_POST['exemption']) ? $_POST['exemption'] : null;
                    $observation = isset($_POST['documents_utiles']) ? $_POST['documents_utiles'] : null;
                    $result = $this->CoursModel->ajouteCours(
                        $nom,
                        $section,
                        $descritpion,
                        $pre_requis,
                        $documents_utiles,
                        $exemption,
                        $observation,
                        $filepath
                    );
                    //vérification si l'ajoute à la DB s'est bien passé
                    if($result){
                        http_response_code(200);
                    }
                    else{
                        //l'ajoute a échoué
                        http_response_code(500);
                    }
                }
                else{
                    http_response_code(500);
                }
            }
            else{
                // les champs obligatoires n'étaient pas présents
                http_response_code(400);
            }
        } else {
            // l'utilisateur n'était pas connecté
            redirect('/');
        }
    }

    public function deleteCours(){
        //vérification si l'utilisateur est connecté
        session_start();
        if(isset($_SESSION['connected'])){
            //vérification qu'un ID a été donné
            if(isset($_POST['id'])){
                $id = $_POST['id'];
                $header_path = $this->CoursModel->getHeaderCours($id);
                $liens_to_delete = $this->CoursModel->deleteCours($id);
                if(count($header_path) > 0 && $header_path[0]->header_path !== '...' && $header_path[0]->header_path !== ''){
                    unlink($header_path[0]->header_path);
                }
                $this->deleteFichier($liens_to_delete);

            }
            //la suppression a réussi
            http_response_code(200);
        }
        else{
            //l'utilisateur n'était pas connecté
            redirect('/');
        }
    }

    public function deleteFichierCours(){
        //vérification si l'utilisateur est connecté
        session_start();
        if(isset($_SESSION['connected'])){
            //vérification valeur chemin
            if(isset($_POST['file']) && isset($_POST['id_cours'])){
                $file = $_POST['file'];
                $id_cours = $_POST['id_cours'];
                //vérification si le fichier existe
                if(file_exists($file)){
                    $this->CoursAsideModel->deleteFichierAside($id_cours, $file);
                    unlink($file);
                }
                http_response_code(200);
            }
            else{
                http_response_code(200);
            }
        }
        else{
            //l'utilisateur n'était pas connecté
            redirect('/');
        }
    }

    public function updateCours(){
        //vérification si l'utilisateur est connecté
        session_start();
        if(isset($_SESSION['connected'])){
            //vérification si GET ou POST
            if(isset($_POST['id'])){
                $cours_id = $_POST['id'];
                //formulaire posté
                $aside_ligne = array_keys($_POST);
                $aside = array();
                foreach($aside_ligne as $key => $value){
                    //traitement des lignes une à une
                    $test = strpos($value, "ligneaside");
                    if($test !== false){
                        $array = explode("_", $value);
                        $age = $array[1];
                        $id = array_pop($array);
                        $object = new stdClass();
                        $object->texte= $_POST[$value];
                        $object->age = $age;
                        $object->id = $id;
                        //test si un fichier est lié à la ligne
                        $string = 'asidefile_'.$age.'_'.$id;
                        $file = null;
                        if(isset($_FILES[$string]) && $_FILES[$string]['size'] > 0) {
                            $file = $_FILES[$string];
                            $upload_dir = "uploaded/documents";
                            if (!file_exists($upload_dir)) {
                                mkdir($upload_dir, 0777, true);
                            }
                            $filepath = $upload_dir . '/' . rand() . $file['name'];
                            if (move_uploaded_file($file["tmp_name"], $filepath)) {
                                $object->lien = $filepath;
                            }
                            else{
                                $object->lien = null;
                            }
                        }
                        else{
                            if($object->age === "new"){
                                $object->lien = null;
                            }
                            else{
                                $object->lien = "...";
                            }
                        }
                        array_push($aside,$object);
                    }
                }

                $cours = new stdClass();
                $cours->nom = $_POST['nom'];
                $cours->section = $_POST['section'];
                $cours->description = $_POST['description'];
                $cours->pre_requis = $_POST['pre_requis'];
                $cours->documents_utiles = $_POST['documents_utiles'];
                $cours->exemption = $_POST['exemption'];
                $cours->observation = $_POST['observation'];

                if(isset($_FILES['header']) && $_FILES['header']['size'] > 0){
                    $upload_dir = "uploaded/headers";
                    if (!file_exists($upload_dir)) {
                        mkdir($upload_dir, 0777, true);
                    }
                    $filepath = $upload_dir . '/' . rand() . $_FILES['header']['name'];
                    if (move_uploaded_file($_FILES['header']["tmp_name"], $filepath)) {
                        $cours->header_path = $filepath;
                    }
                    else{
                        $cours->header_path = '...';
                    }
                }
                else{
                    $cours->header_path = '...';
                }

                //début transaction
                $this->UtilsDBModel->beginTransaction();

                $aside_to_remove = $this->updatesOldAside($cours_id,$aside);
                $new_aside = $this->addNewAside($cours_id, $aside);
                $header_to_delete = $this->CoursModel->updateCours($cours_id,$cours);

                //fin transaction
                $result = $this->UtilsDBModel->endTransaction();
                //vérification si la transaction a réussi
                if($result){
                    http_response_code(200);
                    //suppression des vieux fichiers plus utiles
                    if(count($header_to_delete) === 1){
                        $path = $header_to_delete[0]->header_path;
                        if($path !== '...' && $path != ''){
                            unlink($path);
                        }
                    }
                    $this->deleteFichier($aside_to_remove);
                }
                else{
                    http_response_code(500);
                    //suppression des nouveaux fichiers car transaction a échoué
                    $this->deleteFichier($new_aside);
                }

            }
            else{
                //Demande d'accès au formulaire pour mettre à jour
                $id = $this->uri->segment(3);
                //vérification qu'un id est bien dans l'url
                if($id){
                    $aside_ligne['pages'] = 'pages/updateCours';
                    $aside_ligne['data']['id'] = $id;
                    $cours = $this->CoursModel->getOneCours($id);
                    $cours->aside = $this->CoursAsideModel->getCoursAside($id);
                    if(isset($cours->aside[0])){
                        $cours->aside = $cours->aside[0];
                        if(isset($cours->aside->aside_texte)){
                            $cours->aside->aside_texte = explode("-~-~-~-", $cours->aside->aside_texte);
                        }
                        if(isset($cours->aside->aside_lien)){
                            $cours->aside->aside_lien = explode("-~-~-~-", $cours->aside->aside_lien);
                        }
                        if(isset($cours->aside->aside_ordre_texte)){
                            $cours->aside->aside_ordre_texte = explode("-~-~-~-", $cours->aside->aside_ordre_texte);
                        }
                    }
                    $sections = $this->SectionModel->getSections();
                    $aside_ligne['data']['cours'] = $cours;
                    $aside_ligne['data']['sections'] = $sections;
                    $this->load->view('templates/admin', $aside_ligne);
                }
                else{
                    http_response_code(400);
                }
            }
        }
        else{
            //l'utilisateur n'était pas connecté
            redirect('/');
        }
    }

    private function updatesOldAside($id_cours, $asides){
        $oldAsides = array_filter($asides, function($elem){
            if(isset($elem->age) && $elem->age === "old"){
                return true;
            }
            else{
                return false;
            }
        });
        $old_id = array();
        foreach ($oldAsides as $index=>$obj){
            array_push($old_id, $obj->id);
        }
        if(count($old_id) > 0){
            $old_liens_to_delete1 = $this->CoursAsideModel->deleteAsideNotPresents($id_cours, $old_id);
            $old_liens_to_delete2 = $this->CoursAsideModel->updateAsideLignes($id_cours, $oldAsides);
            $old_liens_to_delete = array_merge($old_liens_to_delete1, $old_liens_to_delete2);
            return $old_liens_to_delete;
        }
        else{
            return array();
        }

    }

    private function addNewAside($id_cours, $asides){
        $newAsides = array_filter($asides, function($elem){
            if(isset($elem->age) && $elem->age === "new"){
                return true;
            }
            else{
                return false;
            }
        });
        $this->CoursAsideModel->addAsideLignes($id_cours, $newAsides);
        return $newAsides;
    }

    private function deleteFichier($array_obj_lien){
        foreach ($array_obj_lien as $key=>$obj){
            if($obj->lien != null){
                unlink($obj->lien);
            }
        }
    }
}