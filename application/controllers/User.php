<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('UserModel');
    }

    public function updateUser(){
        //vérification session
        session_start();
        if(isset($_SESSION['connected'])) {
            if(isset($_POST['id'])){
                $user = new stdClass();
                $user->old_mdp = $_POST['oldmdp'];
                $user->new_mdp = $_POST['newmdp'];
                $user->id = $_POST['id'];
                $user->email = $_POST['email'];

                $result = $this->UserModel->updateUser($user);

                if($result){
                    http_response_code(200);
                }
                else{
                    http_response_code(403);
                }

            }
            else{
                $user = $this->UserModel->getUser();
                $user = $user[0];
                $data['user'] = $user;
                $data['pages'] = 'pages/updateUser';
                $this->load->view('templates/admin', $data);
            }
        }
        else{
            redirect('/');
        }
    }
}