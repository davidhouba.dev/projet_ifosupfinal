<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class My_Model extends CI_Model {
	
	// attributs
	public $nom_table = '';
	public $cle_primaire = '';
	public $filtreCle = 'intval';
	public $trie_par = '';

	public function __construct(){
		parent::__construct();
	}
	
	public function recherche( $ids = FALSE) {
		// vérifie si on réceptionne un tableau ou un seul id
		$simple =  $ids == FALSE | is_array($ids)  ? FALSE : TRUE;

		if ( $ids !== FALSE) {
			// convertir ids en tableau
			is_array($ids) || $ids = array($ids);

			// éxécute la méthode de filtrage
			$filtre = $this->filtreCle;
			$ids = array_map($filtre, $ids);

			// prepare la requête
			$this->db->where_in($this->cle_primaire, $ids);
		}

		// ajoute le tris
		$this->db->order_by($this->trie_par);

		$simple == FALSE || $this->db->limit(1);
		$method = $simple ? 'row_array' : 'result_array';

		return $this->db->get($this->nom_table)->$method();
	}

	/*
  $key = le champs ou les champs de recherche (tableau) ou pas
  $value = la valeur ou les valeurs a rechercher dans les champs
  $ou = une autre valeur à chercher
  $simple = retour une ligne ou plusieurs 
  */
  public function recherche_par($key, $val = FALSE, $ou = FALSE,
 $simple = FALSE ) {

    // On vérifie si on reçois un tableau de $key
    if( ! is_array($key)) {
      $this->db->like(htmlentities($key), htmlentities($val));
    } else {
      $key = array_map('htmlentities', $key);
      $where_method = $ou == TRUE ? 'or_where' : 'where';
      $this->db->$where_method($key);
    }
    // return le resultat
    $simple == FALSE || $this->db->limit(1);
    $method = $simple ? 'row_array' : 'result_array';
    return $this->db->get($this->nom_table)->$method();
	}
	
	public function get_assoc( $ids = FALSE ) {
    $result = $this->recherche($ids);
    if($ids != FALSE && !is_array($ids)) {
      $result = array($result);
    }
    $data = $this->to_assoc($result);

    return $data;
  }
  public function to_assoc($result = array()) {
    $data = array();
    if( count($result) > 0) {
      foreach ($result as $row) {
        $tmp = array_values(array_slice($row, 0, 1));
        $data[$tmp[0]] = $row;
      }
    }
    return $data;
	}
	
	public function sauve($data, $id = FALSE){
    echo $id;
    if ($id == FALSE) {
      // on ajoute un enregistrement
      $this->db->set($data)->insert($this->nom_table);
    } else {
      // mise à jour d'un enregistrement
      $filtre = $this->filtreCle;
      $this->db->set($data)->where($this->cle_primaire, $filtre($id))
       ->update($this->nom_table);
    }
    // retour de l'ID
    return $id == FALSE ? $this->db->insert_id() : $id;
  }

	/*
    Supprime un ou plusieurs enregistrement sur l'id
  */
  public function supprime($ids) {
    $filtre = $this->filtreCle;
    // transforme l'id en tableau si unique
    $ids = !is_array($ids) ? array($ids) : $ids;
    // boucle sur les clés
    foreach( $ids as $id) {
      $id = $filtre($id);
      if ($id) {
        $this->db->where($this->cle_primaire, $id)
                 ->limit(1)
                 ->delete($this->nom_table);
      }
    }
	}
	
	/*
   Supprime suivant une clé / valeur
  */
  public function supprime_par($key, $value) {
    if( empty($key)) {
      return FALSE;
    }
    $this->db->where(htmlentities($key, htmlentities($value)))
     ->delete($this->nom_table);
  }

}

/* End of file My_Model.php */
