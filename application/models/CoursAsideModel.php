<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class CoursAsideModel extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getCoursAside($id){
        $result = $this->db
            ->select("GROUP_CONCAT(sub.texte SEPARATOR '-~-~-~-') AS aside_texte, GROUP_CONCAT(sub.lien SEPARATOR '-~-~-~-') AS aside_lien, GROUP_CONCAT(sub.ordre_texte SEPARATOR '-~-~-~-') AS aside_ordre_texte")
            ->from("(SELECT id_cours, ordre_texte, texte, COALESCE(lien, '...') as lien FROM cours_aside ORDER BY ordre_texte) as sub")
            ->where('id_cours', $id)
            ->group_by('id_cours')
            ->get()
            ->result();
        return $result;
    }

    public function deleteAsideNotPresents($id, $aside_id){
        $this->db
            ->select('lien')
            ->from('cours_aside')
            ->where('id_cours', $id);
        if(count($aside_id) > 0){
            $this->db->where_not_in('ordre_texte',$aside_id);
        }
        $old_rows= $this->db->get()->result();

        $this->db->where('id_cours', $id);
        if(count($aside_id) > 0){
            $this->db->where_not_in('ordre_texte',$aside_id);
        }
        $this->db->delete('cours_aside');
        return $old_rows;
    }

    public function updateAsideLignes($id, $asides){
        echo '------------------';
        var_dump($asides);
        echo '------------------';
        $array_lien_to_delete = array();
        for ($i = 0 ; $i < count($asides) ; $i++){
            var_dump($i);
            echo "<br>";
            var_dump(count($asides));
            echo 'ICI';
            if($asides[$i]->lien != "..."){
                $old_lien = $this->db
                    ->select('lien')
                    ->from('cours_aside')
                    ->where('id_cours', $id)
                    ->where('ordre_texte', $asides[$i]->id)
                    ->get()
                    ->result();
                array_push($array_lien_to_delete, $old_lien[0]);
                $this->db->set('lien', $asides[$i]->lien);
            }
            $this->db
                ->where('id_cours', $id)
                ->where('ordre_texte', $asides[$i]->id)
                ->set('texte', $asides[$i]->texte);
            $this->db->update('cours_aside');
            var_dump($this->db->last_query());
        }
        return $array_lien_to_delete;
    }

    public function addAsideLignes($id, $asides){
        foreach ($asides as $key=>$obj){
            $this->db->insert('cours_aside', array(
                'id_cours' => $id,
                'ordre_texte' => 1,
                'lien' => $obj->lien,
                'texte' => $obj->texte
            ));
        }
    }

    public function deleteFichierAside($id_cours, $lien){
        $this->db
            ->where('id_cours', $id_cours)
            ->where('lien', $lien)
            ->set('lien', null)
            ->update('cours_aside');
    }

}