<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class ConnectionModel extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function isUser($email, $mdp){
        try{
            $mdp = hash('sha512',$mdp);
            $results = $this->db
                ->where(array(
                    "mdp" => $mdp,
                    "email" => $email
                ))
                ->get('user')
                ->result();
            if (count($results) == 1) {
                return true;
            }
            else{
                return false;
            }
        }
        catch(Exception $e){
            return $e->getMessage();
        }
    }

}