<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GetPage extends CI_Model{


	public $nom_table = 'cours';
	public $cle_primaire = 'id';
	public $trie_par = '';

	public function __construct(){
		parent::__construct();
	}

	public function afficheIndex(){
		$query = $this->db->select('nom,contenu')
			->from('page')
			->where ('nom NOT LIKE \'Inscription à l\'\'IFOSUP\'')
			->get();
		return $query;
	}

	public function afficheInscription(){
		$query = $this->db->select('nom,contenu')
			->from('page')
			->where ('nom LIKE \'Inscription à l\'\'IFOSUP\'')
			->get();
		$afficheInscription = $query->result_array();
		return $afficheInscription;
	}
	public function afficheNosFormations(){
		$query = $this->db->select('nom')
			->from('page')
			->where ('nom LIKE \'Nos formations\'')
			->get();
		$afficheNosFormations = $query->result_array();
		return $afficheNosFormations;
	}

}

?>
