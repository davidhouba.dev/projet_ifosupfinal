<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GetCours extends CI_Model{

    public $nom_table = 'cours';
    public $cle_primaire = 'id';
    public $trie_par = '';

    public function __construct(){
        parent::__construct();
    }

    public function affiche_cours_front($idCours){
		$query = $this->db->select('cours.id,cours.nom,cours.description,cours.pre_requis,cours.documents_utiles,cours.exemption,cours.observation,cours.section,cours.header_path')
			->from('cours')
			->where('cours.id='.$idCours)
			->get();
		$retour = $query->result_array();
		return $retour;
	}

	public function max(){
		$query = $this->db->select_max('cours.id ')
		->from('cours')
		->get();
		$retour = $query->result_array();
		return $retour;
	}

	
    public function afficheCours(){
        $query = $this->db->select('nom,description')
        ->from('cours')
        ->get();
        $retour = $query->result_array();
        return $retour;
    }

	public function afficheLiensSecondaires(){
		$query = $this->db->select('id as idSecondaire,nom as nomSecondaire')
			->from('cours')
			->where('section=1')
			->get();
			$retour = $query->result_array();
			return $retour;
	}

	public function afficheLiensSuperieurBac(){
		$query = $this->db->select('id as idSupérieur,nom as nomSuperieur')
			->from('cours')
			->where('section=2')
			->where('nom LIKE \'%Bachelier%\'')
			->get();
			$retour = $query->result_array();
			return $retour;
	}

	public function afficheLiensSuperieurBes(){
		$query = $this->db->select('id as idSupérieurBes,nom as nomSuperieurBes')
			->from('cours')
			->where('section=2')
			->where('nom LIKE \'%BES%\'')
			->get();
			$retour = $query->result_array();
			return $retour;
	}


}

?>
