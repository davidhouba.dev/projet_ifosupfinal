<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getUser(){
        return $this->db->select('*')->from('user')->get()->result();
    }

    public function updateUser($user){
        $mdp = hash('sha512',$user->old_mdp);
        $this->db->where('id', $user->id)->where('mdp', $mdp);
        if(isset($user->email) && $user->email != ''){
            $this->db->set('email', $user->email);
        }
        if(isset($user->new_mdp) && $user->new_mdp != ''){
            $this->db->set('mdp', hash('sha512',$user->new_mdp));
        }
        $this->db->update('user');
        $tmp = $this->db->affected_rows();
        if($tmp > 0){
            return true;
        }
        else{
            return false;
        }
    }

}