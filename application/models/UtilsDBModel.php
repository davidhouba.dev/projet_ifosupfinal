<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UtilsDBModel extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function beginTransaction(){
        $this->db->trans_begin();
    }

    public function endTransaction(){
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

}