<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SectionModel extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getSections(){
        $result = $this->db->order_by('nom', 'ASC')->get('section')->result();
        return $result;
    }

    public function getHeaderSection($id){
        return $this->db->select('header_path')->where('id', $id)->get('section')->result();
    }

    public function getSection($id){
        return $this->db->select('*')->from('section')->where('id', $id)->get()->result();
    }

    public function updateSection($id, $section){
        if(isset($section->header_path) && $section->header_path !== '...' && $section->header_path !== ''){
            $header_to_delete = $this->db->select('header_path')->where('id', $id)->get('section')->result();
            $this->db->where('id', $id)->update('section', $section);
            return $header_to_delete;
        }
        else if (isset($section->header_path)){
            unset($section->header_path);
        }
        $this->db->where('id', $id)->update('section', $section);
        return array();
    }

}