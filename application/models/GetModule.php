<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GetModule extends CI_Model{


	public $nom_table = 'cours';
	public $cle_primaire = 'id';
	public $trie_par = '';

	public function __construct(){
		parent::__construct();
	}

	public function afficheModuleDecouverte(){
		$query = $this->db->select('id,nom')
			->from('module')
			->where('section=4')
			->get();
		$afficheModuleDecouverte = $query->result_array();
		return $afficheModuleDecouverte;
	}

	public function afficheModuleDecouverteDescription(){
		$query = $this->db->select('id,nom,description,jour,date_debut,date_fin,heure_debut,heure_fin')
			->from('module')
			->where('section=4')
			->get();
		$afficheModuleDecouverteDescription = $query->result_array();
		return $afficheModuleDecouverteDescription;
	}

	public function afficheModulePerfection(){
		$query = $this->db->select('id,nom,description,jour,date_debut,date_fin,heure_debut,heure_fin')
			->from('module')
			->where('section=5')
			->get();
		$afficheModulePerfection = $query->result_array();
		return $afficheModulePerfection;
	}



}

?>
