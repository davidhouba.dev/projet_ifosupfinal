<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GetSections extends CI_Model{


    public $nom_table = 'cours';
    public $cle_primaire = 'id';
    public $trie_par = '';

    public function __construct(){
        parent::__construct();
    }


	public function affiche_section($idSec){
        $query = $this->db->select('section.id,section.nom as nomSection,section.description as descSection,cours.nom as nomCours,cours.id as idCours,section.header_path')
        ->from('section')
        ->join('cours','cours.section = section.id')
        ->where('section.id = '.$idSec)
        ->get();
        $affiche = $query->result_array();
        //var_dump($this->db->last_query());
        return $affiche;
    }


	public function afficheTitreSecondaire(){
		$query = $this->db->select('nom')
			->from('section')
			->where('id=1')
			->get();
		$afficheTitreSecondaire = $query->result_array();
		return $afficheTitreSecondaire;
	}

	public function afficheTitreSuperieur(){
		$query = $this->db->select('nom')
			->from('section')
			->where('id=2')
			->get();
		$afficheTitreSuperieur = $query->result_array();
		return $afficheTitreSuperieur;
	}

	public function afficheLangue(){
		$query = $this->db->select('nom,description,section.header_path')
			->from('section')
			->where('id=3')
			->get();
		$afficheLangue = $query->result_array();
		return $afficheLangue;
	}

	public function afficheTitreLangue(){
		$query = $this->db->select('nom')
			->from('section')
			->where('id=3')
			->get();
		$afficheTitreLangue = $query->result_array();
		return $afficheTitreLangue;
	}

	public function afficheDecouverte(){
		$query = $this->db->select('nom,section.header_path')
			->from('section')
			->where('id=4')
			->get();
		$afficheDecouverte = $query->result_array();
		return $afficheDecouverte;
	}
	public function affichePerfection(){
		$query = $this->db->select('nom,section.header_path')
			->from('section')
			->where('id=5')
			->get();
		$affichePerfection = $query->result_array();
		return $affichePerfection;
	}
	public function afficheServicesEntreprise(){
		$query = $this->db->select('nom,description,section.header_path')
			->from('section')
			->where('id=6')
			->get();
		$afficheServiceEntreprise = $query->result_array();
		return $afficheServiceEntreprise;
	}
}

?>
