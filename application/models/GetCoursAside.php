<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GetCoursAside extends CI_Model{

    public $nom_table = 'cours';
    public $cle_primaire = 'id';
    public $trie_par = '';

    public function __construct(){
        parent::__construct();
    }

    public function afficherAside($idCours){
        $query = $this->db->select('texte,lien')
            ->from('cours_aside')
            ->where('id_cours = '.$idCours)
            ->order_by('ordre_texte')
            ->get();
            $retour = $query->result_array();
		    return $retour;
    }

    public function afficherLiensLangues(){
        $query = $this->db->select('cours_aside.texte,cours_aside.lien,cours.nom')
            ->from('cours_aside')
            ->join('cours','cours.id = cours_aside.id_cours')
            ->where('cours.section = 3')
            ->order_by('ordre_texte')
            ->get();
            $retour = $query->result_array();
		    return $retour;
    }
}