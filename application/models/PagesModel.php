<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PagesModel extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getListePages(){
        return $this->db->select('*')->from('page')->get()->result();
    }

    public function getPage($nom){
        return $this->db->select('*')->from('page')->where('nom', $nom)->get()->result();
    }

    public function updatePage($oldnom, $nom, $contenu){
        $this->db->set('nom', $nom)->set('contenu', $contenu)->where('nom', $oldnom)->update('page');
    }

}