<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CoursModel extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getCours(){
        $result = $this->db->order_by('nom', 'ASC')->get('cours')->result();
        return $result;
    }

    public function getHeaderCours($id){
        return $this->db->select('header_path')->where('id', $id)->get('cours')->result();
    }

    public function getOneCours($id){
        $result = $this->db
            ->select('*')
            ->from('cours')
            ->where('id', $id)
            ->get()
            ->result();
        if(isset($result[0])){
            return $result[0];
        } else{
            return false;
        }
    }

    public function ajouteCours($nom, $section, $description, $pre_requis, $documents_utiles, $exemption, $observation, $header_path){
        $this->db->insert('cours', array(
            "nom" => $nom,
            "section" => $section,
            "description" => $description,
            "pre_requis" => $pre_requis,
            "documents_utiles" => $documents_utiles,
            "exemption" => $exemption,
            "observation" => $observation,
            "header_path" => $header_path
        ));
        if ($this->db->affected_rows() === 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function deleteCours($id){
        $lien_to_delete = $this->db->select('lien')->from('cours_aside')->where('id_cours', $id)->get()->result();
        $this->db->where('id', $id)->delete('cours');
        return $lien_to_delete;
    }

    public function updateCours($cours_id, $cours){
        if(isset($cours->header_path) && $cours->header_path !== '...' && $cours->header_path !== ''){
            $header_to_delete = $this->db->select('header_path')->where('id', $cours_id)->get('cours')->result();
            $this->db->where('id', $cours_id)->update('cours', $cours);
            return $header_to_delete;
        }
        else if (isset($cours->header_path)){
            unset($cours->header_path);
        }
        $this->db->where('id', $cours_id)->update('cours', $cours);
        return array();
    }
}