<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class Ariane extends CI_Model{
    public $nom_table = 'cours';
    public $cle_primaire = 'id';
    public $trie_par = '';

    public function __construct(){
        parent::__construct();
    }

    public function affiche_cat($idSec){
        $query = $this->db->select('cours.id,cours.nom as nomCours,cours.section,section.id as idSection,section.nom as nomSection')
        ->from('cours')
        ->join('section','cours.section = section.id')
        ->where('section.id = '.$idSec)
        ->get();
        $affiche = $query->result_array();
        return $affiche;
    }

    public function affiche_cours($idCours){
        $query = $this->db->select('cours.id as idCours,cours.nom as nomCours,cours.section,section.id as idSection,section.nom as nomSection')
        ->from('cours')
        ->join('section','cours.section = section.id')
        ->where('cours.id = '.$idCours)
        ->get();
        $affiche = $query->result_array();
        return $affiche;
    }

    public function affiche_Mod($idSec){
        $query = $this->db->select('module.id,module.nom as moduleCours,module.section,section.id,section.nom as nomSection')
        ->from('module')
        ->join('section','module.section = section.id')
        ->where('section.id = '.$idSec)
        ->get();
        $affiche = $query->result_array();
        return $affiche;
    }

    public function affiche_Ser($idSec){
        $query = $this->db->select('section.id,section.nom as nomSection')
        ->from('section')
        ->where('section.id = '.$idSec)
        ->get();
        $affiche = $query->result_array();
        return $affiche;
    }
}
?>
