<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ModuleModel extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function getModules(){
        return $this->db
            ->select('id, nom')
            ->order_by('nom', 'ASC')
            ->get('module')->result();
    }

    public function ajouteModule($module){
        $this->db->insert('module', $module);
        if($this->db->affected_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    public function deleteModule($module){
        $this->db->where('id', $module->id)->delete('module');
    }

    public function getModule($id){
        return $this->db->where('id', $id)->get('module')->result();
    }

    public function updateModule($id, $module){
        $this->db->where('id', $id)->update('module', $module);
    }

}