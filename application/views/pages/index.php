<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
$afficheIndex = $query->row_array();
?>
	
<section id="header" class="mx-0">

	<div class="row">
	
		<!-- Zone image 1-->
		<div class="col-md-12 p-5 text-white" id="StudentImg">
		<!-- Zone Texte-->
			
			<div class=" col-md-8 col-lg-5 p-5 mask rgba-navy">
			
			
				<h1 class="text-center"><?= $afficheIndex['nom']; ?></h1>
				<p><?= $afficheIndex['contenu']; ?></p>

		
			</div>
	
			<!-- Fin Zone texte-->
		</div>
		<!-- Fin Zone image 1-->
	
	</div>

</section>

</header>
<!--Fin du header-->
<?php
$afficheIndex = $query->next_row('array');
?>

<!--section About Ifosup-->
<section id="AboutIfosup">

	<div class="d-flex align-items-center">
		<!-- Zone de texte -->

		<div class="col-md-8 p-5">
			<div class="container py-5">
				<h3><?= $afficheIndex['nom']; ?></h3>
				<p><?= $afficheIndex['contenu']; ?></p>
			</div>
		</div>
		<!-- Fin Zone de texte -->
	
		<!-- Zone image -->
		<div class=" d-none d-md-block col-md-4 align-self-bottom d-print-none">
			<img src="<?= base_url(); ?>assets/img/ifosup.jpg" alt="Logo IFOSUP" title="IFOSUP Wavre | Institut de formation supérieure" class="img-fluid">
		</div>
		<!-- Fin Zone image -->
	</div>

</section>
<!--/section About Ifosup-->
<?php
$afficheIndex = $query->last_row('array');
?>
<!--section Slogan-->
<section id="slogan" class="d-flex justify-content-center message d-print-none">
	<div class="d-flex align-items-center text-white">
		<div class="col-md-12">
			<h2 class="text-center py-5"><?= $afficheIndex['nom']; ?></h2>
		</div>
	</div>
</section>
<!-- Fin section Slogan-->

<?php
$afficheIndex = $query->next_row('array');
?>

<!--section formation-->
<section id="formations" class="p-5 d-print-none">
	<div class="container">
		<!--  Row Titre-->
		<div class="row py-2">
			<div class="col-md-12">
				<h2 class="text-center"><?= $afficheIndex['nom']; ?></h2>
			</div>
		</div>
		<!-- Fin Row Titre-->

		<!--  Row Cartes des formations-->
		<div class="row py-5">
			<div class="col-md-4">
				<!-- Card -->
				<div class="card card-image" id="CarteFormation1">
					<!-- Content -->
					<div class="mask rgba-black-light">
						<div class="text-center py-5">
							<h3 class="card-title pt-2 white-text"><strong>Sections du secondaire</strong></h3>
							<a class="btn btn-navy" href="<?= base_url();?>pages/section?idSec=1"><i class="fa fa-clone left"></i> Voir les sections</a>
						</div>
					</div>
				</div>
				<!-- Card -->
			</div>
			<div class="col-md-4">
				<!-- Card -->
				<div class="card card-image" id="CarteFormation2">
					<!-- Content -->
					<div class="mask rgba-black-light">
						<div class="text-center py-5">
							<h3 class="card-title pt-2 white-text"><strong>Sections du supérieur</strong></h3>
							<a class="btn btn-navy" href="<?= base_url();?>pages/section?idSec=2"><i class="fa fa-clone left"></i> Voir les sections</a>
						</div>
					</div>
				</div>
				<!-- Card -->
			</div>

			<div class="col-md-4">
				<!-- Card -->
				<div class="card card-image" id="CarteFormation3">
					<!-- Content -->
					<div class="mask rgba-black-light">
						<div class="text-center py-5">
							<h3 class="card-title pt-2 white-text"><strong> Langues</strong></h3>
							<a class="btn btn-navy" href="<?= base_url();?>pages/langues?idSec=3"><i class="fa fa-clone left"></i> Voir les cours</a>
						</div>
					</div>
				</div>
			</div>
			<!-- Card -->
		</div>
		<!-- Fin de la 1e row-->

		<!--  Début de la 2e row  -->
		<div class="row justify-content-center">
			<!-- Card -->
			<div class="col-md-4">
				<div class="card card-image" id="CarteFormation4">
					<!-- Content -->
					<div class="mask rgba-black-light">
						<div class="text-center py-5">
							<h3 class="card-title pt-2 white-text"><strong> Modules informatique</strong></h3>
							<a class="btn btn-navy" href="<?= base_url();?>pages/decouverte?idSec=4"><i class="fa fa-clone left"></i> Voir les modules</a>
						</div>
					</div>
				</div>
			</div>
			<!-- Card -->

			<!-- Card -->
			<div class="col-md-4">
				<div class="card card-image" id="CarteFormation5">
					<!-- Content -->
					<div class="mask rgba-indigo-slight">
						<div class="text-center py-5">
							<h3 class="card-title pt-2 white-text"><strong> Services aux entreprises</strong></h3>
							<a class="btn btn-navy" href="<?= base_url();?>pages/serviceentreprise?idSec=6"><i class="fa fa-clone left"></i> Voir les services</a>
						</div>
					</div>
				</div>
			</div>
			<!-- Card -->
		</div>
		<!-- fin de la 2e row-->
		<!-- Fin Row Cartes des formations-->
	</div>
</section>

<!--/section formation-->

<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/mdb.min.js"></script>

</body>
</html>
