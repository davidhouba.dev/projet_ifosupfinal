<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container mt-5 pt-2">
    <a href="<?= base_url(); ?>index.php/Connection/login" class="btn btn-blue mt-5">
        <i class="fas fa-align-justify pr-2"></i> Panel administrateur</a>
<?php if(isset($cours)):?>
    <ul class="pt-5">
        <?php foreach($cours as $c): ?>
        <li class="nav">
                    <div class="nav-item col-sm-4 py-3">
                        <span><?= $c->nom ?> </span>
                    </div>
                    <div class="nav-item col-sm-2"><button
                            class="delete btn btn-blue"
                            data-id="<?=$c->id?>"
                            data-toggle="modal"
                            data-target="#deleteModal"
                            data-cours="<?= $c->nom?>">Delete</button></div>
                    <div class="nav-item">
                        <a href="<?=base_url()."cours/updateCours/".$c->id?>">
                            <button class="btn btn-blue">Update</button>
                        </a>
                    </div>
            </li>
        <?php endforeach;?>
    </ul>
<?php endif;?>

<h2 class="py-5 text-center">Ajouter un cours</h2>

<form id="form" method="post">
    <p class="form-group">
        <label for="nom">Nom:</label>
        <input id="nom" name="nom" type="text" class="text-input form-control" required>
    </p>

    <p class="form-group">
        <label for="nom">Header du cours:</label>
        <input id="header" name="header" type="file" class="form-control-file" accept="image/*" required>
    </p>

    <p class="form-group">
        <label for="section">Section:</label>
        <select class="form-control" id="section" name="section" class="selectpicker" required>
            <?php foreach ($sections as $section): ?>
                <option value="<?= $section->id ?>"><?= $section->nom ?></option>
            <?php endforeach; ?>
        </select>
    </p>

    <p class="form-group">
        <label for="description">Description:</label>
        <textarea id="description" name="description" class="trumbowyg form-control" required>

        </textarea>
    </p>
    <p class="form-group">
        <label for="pre_requis">Pré-requis:</label>
        <textarea id="pre_requis" name="pre_requis" class="trumbowyg form-control" required>

        </textarea>
    </p>
    <p class="form-group">
        <label for="documents_utiles">Documents utiles:</label>
        <textarea id="documents_utiles" name="documents_utiles" class="trumbowyg form-control" required>

        </textarea>
    </p>
    <p class="form-group">
        <label for="exemption">Exemption:</label>
        <textarea id="exemption" name="exemption" class="trumbowyg form-control" required>

        </textarea>
    </p>
    <p class="form-group">
        <label for="observation">Observation:</label>
        <textarea id="observation" name="observation" class="trumbowyg form-control" required>
        </textarea>
    </p>
    <p>La possibilité de gérer le sommaire d'un cours sera disponible une fois celui-ci créé</p>
    <button type="submit" id="submit" name="submit" class="btn btn-blue btn-block col-sm-4 mb-4 mx-auto">Envoyer</button>
</form>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Suppression du cours</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Etes-vous sûr de vouloir supprimer le cours <span data-fonction="show-nom"></span> ?
            </div>
            <div class="alert alert-danger" role="alert" name="errorDelCours" style="display: none;">
                Le cours n'a pas pu être supprimé de la base de données, rafraichissez la page et réessayer. Si l'erreur persiste, contactez un administrateur.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Garder</button>
                <button type="button" class="btn btn-primary" data-fonction="supprimer">Supprimer</button>
            </div>
        </div>
    </div>
</div>

<div class="alert alert-success" role="alert" style="display: none;">
    Le cours a été ajouté à la base de données, la page sera automatiquement rafraichie dans 3 secondes.
</div>
<div class="alert alert-danger" role="alert" name="errorAjouteCours" style="display: none;">
    Le cours n'a pas pu être ajouté à la base de données, vérifiez que vous avez entré au minimum le nom et la section du cours.
</div>
</div>

<script>

    $(document).ready(function(){
        $('html, body').animate({ scrollTop: 0 }, 'fast');
    });


    $("#deleteModal").on("show.bs.modal",function(e){
        var button = e.relatedTarget;
        $("#deleteModal span[data-fonction=show-nom]").html("<strong>"+$(button).attr("data-cours")+"</strong>");
        var id=  $(button).attr("data-id");
        $("#deleteModal button[data-fonction=supprimer]").click(function(){
            $.ajax({
                method: "POST",
                url: "<?=base_url()?>cours/deleteCours",
                data:{
                    id: id
                },
                error: function(){
                    $("div.alert-danger[name=errorDelCours]").fadeIn();
                },
                success: function(){
                    $("#deleteModal").modal('hide').end();
                    var cours =  $($(button).parents()[1]);
                    cours.fadeOut(400, function(){
                        cours.remove();
                    });
                }
            });
        });
    });

    $("#form").submit(function(e){
        e.preventDefault();
        $("div.alert").fadeOut('fast');
        var formData = new FormData(this);

        $.ajax({
            method: "POST",
            url: "<?=base_url()?>cours/ajouteCours",
            processData: false,
            contentType: false,
            data: formData,
            success: function(){
                $("div.alert-success").fadeIn(400, function(){
                    setTimeout(function(){
                        location.reload(true);
                    },3000)
                });
            },
            error: function(){
                $("div.alert-danger[name=errorAjouteCours]").fadeIn();
            }
        });

    });
    $.trumbowyg.svgPath = '<?= base_url(); ?>assets/css/icons.svg';
    var config = {
        lang: 'fr',
        btnsDef: {
            // Create a new dropdown
            image: {
                dropdown: ['insertImage', 'upload'],
                ico: 'insertImage'
            }
        },
        // Redefine the button pane
        btns: [
            ['historyUndo','historyRedo'],
            ['formatting'],
            ['strong', 'em', 'underline'],
            ['superscript', 'subscript'],
            ['link'],
            ['image'], // Our fresh created dropdown
            /*  */           ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['foreColor', 'backColor'],
            ['fullscreen'],
            ['table']
        ],
        plugins: {
            // Add imagur parameters to upload plugin for demo purposes
            upload: {
                serverPath: "<?= base_url();?>index.php/upload/image",
                urlPropertyName: 'url'
            },
            resizimg : {
                minSize: 20,
                step: 1,
            },
            table: {
                rows:7,
                columns:7,
                styler:'table',
            }
        }
    };
    $('.trumbowyg').trumbowyg(config);
</script>