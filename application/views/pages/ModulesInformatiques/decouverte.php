
 <section id="decouverte"> 
    <div class="row d-none d-sm-block d-print-none">
           <div class="col-md-12 " style="min-height: 40vh;width: 100%;background-repeat: no-repeat;background-size: cover;background-position: center; background-image: url(<?= base_url().$afficheDecouverte[0]['header_path']; ?>);"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                  foreach($afficheDecouverte as $afficheDecouverte) :
                ?>
                <h1 class="py-5 text-center"><?= $afficheDecouverte['nom']; ?></h1>
                <?php
                  endforeach;
                ?>
            </div>
        </div>
     </div>       
</section> 

</header>
<!--Fin du header-->  
       
<!--  Breadcrumbs -->
  <nav aria-label="breadcrumb" class="px-5">
    <ol class="breadcrumb">
      <li class="breadcrumb-item arrow-green">
         <a class="grey-text t-green" href="<?= base_url(); ?>pages">Accueil</a>
      </li>
      <li class="breadcrumb-item arrow-green">
         <a class="grey-text t-green" href="<?= base_url(); ?>pages/formations">Nos formations</a>
      </li>
       <li class="breadcrumb-item active arrow-green">
         <a class="t-green" href="<?= base_url(); ?>pages/decouverte?idSec=4">Découverte - Initiation</a>
      </li>
    </ol>
  </nav>
<!--  Breadcrumbs -->

<!--Content-->
<main class="pb-5">  
    <div class="container">
           <div class="row py-5">
                  
               <div class="col-md-12">
                  <!-- Card secondaire-->
                     <div class="card">
                      <!-- Card header -->
                      <h5 class="card-header text-center ch-green">Nos cours</h5>

                      <!-- Card content -->
                      <div class="card-body">
                        <!-- Text -->
                        <ul class="card-text list-group list-group-flush py-3">
                          <div class="row">
                           <div class="col-md-6">
                           <?php
                             for($i=0; $i<$nbr_col1; $i++) :
                            ?>
                            <li class="list-group-item"><a href="#<?= $afficheModuleDecouverte[$i]['id'];?>" class="grey-text t-green"><?= $afficheModuleDecouverte[$i]['nom']; ?></a></li>
                            <?php
                            endfor;
                            ?>
                           </div>
                            <div class="col-md-6">
                            <?php
                             for($i=0; $i<$nbr_col2; $i++) :
                            ?>
                            <li class="list-group-item"><a href="#<?= $afficheModuleDecouverte[$i+$nbr_col1]['id'];?>" class="grey-text t-green"><?= $afficheModuleDecouverte[$i+$nbr_col1]['nom']; ?></a></li>
                            <?php
                            endfor;
                            ?>
                            </div>
                            </div> 
                        </ul> 
                      </div>

                    </div>
                <!-- Card -->
                   
               </div>
           </div> 
<!--           fin de row        -->
           <div class="col-md-12">
           <?php
                foreach($afficheModuleDecouverteDescription as $afficheModuleDecouverteDescription) :
            ?>
              <h3 id="<?= $afficheModuleDecouverteDescription['id']; ?>"><?= $afficheModuleDecouverteDescription['nom']; ?></h3>
              <p><?= $afficheModuleDecouverteDescription['description']; ?></p>
              <p>
                <span class="h5">Jour :</span> <?= $afficheModuleDecouverteDescription['jour'];?><br>
                <span class="h5">Dates :</span> <?= $afficheModuleDecouverteDescription['date_debut'];?> au <?= $afficheModuleDecouverteDescription['date_fin'];?> <br> 
                <span class="h5">Heures :</span> <?= $afficheModuleDecouverteDescription['heure_debut'];?> à <?= $afficheModuleDecouverteDescription['heure_fin'];?> 
              </p>
            <?php
                endforeach;
            ?>
               
           </div>
    </div>

  
</main>

<!--Content-->

<!--Pour ne pas cacher le titre-->
<script>
var shiftWindow = function() { scrollBy(0, -100) };
if (location.hash) shiftWindow();
window.addEventListener("hashchange", shiftWindow);
</script>

	<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/mdb.min.js"></script>

</body>
</html>
