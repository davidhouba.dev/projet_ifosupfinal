
 <section id="perfection"> 
    <div class="row d-none d-sm-block d-print-none">
           <div class="col-md-12" style="min-height: 40vh;width: 100%;background-repeat: no-repeat;background-size: cover;background-position: center; background-image: url(<?= base_url().$affichePerfection[0]['header_path']; ?>);"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <?php
              foreach($affichePerfection as $affichePerfection):
            ?>
                <h1 class="py-5 text-center"><?=$affichePerfection['nom'];?></h1>
              <?php endforeach;?>
            </div>
        </div>
     </div>       
</section> 

</header>
<!--Fin du header-->  

<!--  Breadcrumbs -->
  <nav aria-label="breadcrumb" class="px-5">
    <ol class="breadcrumb">
      <li class="breadcrumb-item arrow-green">
         <a class="grey-text t-green" href="<?= base_url(); ?>pages">Accueil</a>
      </li>
      <li class="breadcrumb-item arrow-green">
         <a class="grey-text t-green" href="<?= base_url(); ?>pages/formations">Nos formations</a>
      </li>
       <li class="breadcrumb-item active arrow-green">
         <a class="t-green" href="<?= base_url(); ?>pages/perfection?idSec=5">Perfectionnement</a>
      </li>
    </ol>
  </nav>
<!--  Breadcrumbs -->

<!--Content-->
<main class="pb-5">  
    <div class="container">
           <div class="row py-5">
                   <?php
              foreach($afficheModule as $afficheModulePerfection):
            ?>
            
           <div class="col-md-12">
              <h3 id="ExcelPerf"><?=$afficheModulePerfection['nom'];?></h3>
              <p><?=$afficheModulePerfection['description'];?></p>
              <p>
                <span class="h5">Jour :</span> <?=$afficheModulePerfection['jour'];?> <br>
                <span class="h5">Dates :</span> <?=$afficheModulePerfection['date_debut'];?> au <?=$afficheModulePerfection['date_fin'];?> <br> 
                <span class="h5">Heures :</span> <?=$afficheModulePerfection['heure_debut'];?> à <?=$afficheModulePerfection['heure_fin'];?> 
              </p>
           </div>
           <?php
      endforeach;
    ?>
           </div>
            
    </div>
</main>

<!--Content-->

	<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/mdb.min.js"></script>

</body>
</html>
