<!DOCTYPE html>
<html lang="fr" class="nav-blue">

<section id="contact">
	<!-- Row carte Google Maps -->
	<div class="row d-none d-sm-block d-print-none">
		<div class="col-md-12 border">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10104.496746100398!2d4.6078182!3d50.7176352!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x96bf566252976fb4!2sIFOSUP+-+Wavre+Promotion+Sociale!5e0!3m2!1sfr!2sbe!4v1542902179269" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>
	<!-- Row carte Google Maps -->
</section>
</header>
<!--Fin du header-->




<section>


	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="py-5 text-center">Contactez-nous</h1>
			</div>
		</div>
		<div class="d-flex align-items-center pb-5">
			<div class="col-md-7">
				<!-- Default form contact -->
				<form class="" id="formulaire" method="post">
					<!-- Name -->
					<div class="form-group">
						<label for="nom">Nom : </label>
						<input value="" class="validate[required] text-input form-control" type="text" name="req" id="req" placeholder="Jean Dubois" />
					</div>

					<!-- Email -->
					<div class="form-group">
						<label for="email">Email : </label>
						<input type="text" value=""
							   name="email" id="email"
							   class="validate[required, custom[email]] form-control"
							   data-prompt-position="bottomLeft" placeholder="jean.dubois@gmail.com" >
					</div>

					<!-- Message -->
					<div class="form-group">
						<label for="message">Message : </label>
						<textarea class="form-control rounded-0" id="message" rows="3" placeholder="Votre message"></textarea>
					</div>

					<!-- Send button -->
					<button class="btn btn-blue btn-block" type="submit">Envoyer</button>

				</form>
				<!-- Default form contact -->
			</div>


			<!--            CONTACTS SUR LA DROITE      -->
			<div class="col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5">
				<div class="fancy-collapse-panel">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h5 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Direction
									</a>
								</h5>
							</div>
							<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<p>Directrice <br> Mme Valérie VANDERAVERO <br> <i class="far fa-envelope"></i> valerie.vanderavero@ifosupwavre.be</p>
									<p>Sous-Directrice <br> Mme Nicole THOMAS  <br> <i class="far fa-envelope"></i> nicole.thomas@ifosupwavre.be</p>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h5 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Conseiller en formation
									</a>
								</h5>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
									<p>Mme Karin LOVIBOND <br> <i class="far fa-envelope"></i> conseiller.formation@ifosupwavre.be <br> <i class="fas fa-phone"></i> 010/497.442</p>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingThree">
								<h5 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Congés éducation
									</a>
								</h5>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
								<div class="panel-body">
									<p>Mme Annick COISMAN <br> <i class="far fa-envelope"></i> ac.secretariat@ifosupwavre.be <br> <i class="fas fa-phone"></i> 010/497.442</p>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingFour">
								<h5 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Site Intranet
									</a>
								</h5>
							</div>
							<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
								<div class="panel-body">
									<p>Mme Christelle MICHAUX <br> <i class="far fa-envelope"></i> cm.secretariat@ifosupwavre.be <br> <i class="fas fa-phone"></i> 010/497.443</p>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
			<!--            CONTACTS SUR LA DROITE      -->
		</div>
	</div>
</section>

<script src="<?= base_url();?>assets/js/jquery.min.js"></script>
<script src="<?= base_url();?>assets/js/popper.min.js"></script>
<script src="<?= base_url();?>assets/js/mdb.min.js"></script>

</body>
</html>
