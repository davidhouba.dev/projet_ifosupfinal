<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container mt-5 pt-2 text-center">
    <a href="<?= base_url(); ?>index.php/Connection/login" class="btn btn-light mt-5 col-sm-4">
        Retour</a>
    <?php if(isset($user)): ?>
    <form id="form" class="form-group mt-5">
        <input id="id" name="id" value="<?=$user->id?>" hidden>
        <label for="email" class="col-sm-3">Email :</label>
        <input type="email" id="email" name="email" value="<?= $user->email ?>" class="form-control col-sm-4 mx-auto" required><br>
        <label for="oldmdp" class="col-sm-3">Ancien mot de passe:</label>
        <input type="password" name="oldmdp" id="oldmdp" class="form-control col-sm-4 mx-auto" required><br>
        <label for="newmdp" class="col-sm-3">Nouveau mot de passe:</label>
        <input type="password" id="newmdp" class="form-control col-sm-4 mx-auto" name="newmdp">
        <input type="submit" class="btn btn-blue btn-block col-sm-4 mb-4 mx-auto">
    </form>
    <?php endif; ?>
    <div class="alert alert-success" role="alert" name="successUpdateUser" style="display: none;">
        Les identifiants ont été mis à jour ! Vous serez redirigé automatiquement au panel administrateur dans 3 secondes.
    </div>
    <div class="alert alert-danger" role="alert" name="errorUpdateUser" style="display: none;">
        Les identifiants n'ont pas pu être mis à jour. Peut-être que les identifiants donnés sont mauvais ? Si ce n'est
        pas le cas, veuillez contacter un administrateur système.
    </div>
</div>

<script>
    $('#form').submit(function(e){
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            method: 'POST',
            url: '<?=base_url().'user/updateUser'?>',
            data: formData,
            processData: false,
            contentType: false,
            error: function(){
                $('div[name=errorUpdateUser]').fadeIn(400, function(){
                    setTimeout(function(){
                        $('div[name=errorUpdateUser]').fadeOut();
                    }, 3000)
                })
            },
            success: function(){
                $('div[name=successUpdateUser]').fadeIn(400, function(){
                    setTimeout(function(){
                        window.location = '<?= base_url()."connection/login"?>';
                    }, 3000)
                });
            }
        });
    });

</script>
