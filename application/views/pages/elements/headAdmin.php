<!DOCTYPE html>
<html lang="fr" class="nav-blue">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>IFOSUP WAVRE | Institut de formation supérieure</title>
    <link rel="icon" type="image/png" href="<?= base_url(); ?>assets/img/ifosup-favicon.png" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/mdb.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/style.css">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery-resizable.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/trumbowyg.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/trumbowyg.upload.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/fr.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/trumbowyg.resizimg.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/trumbowyg.colors.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/trumbowyg.history.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/trumbowyg.table.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap-select.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/defaults-fr_FR.min.js"></script>


    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/trumbowyg.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/trumbowyg.colors.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/trumbowyg.table.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-select.min.css">


</head>
<body>