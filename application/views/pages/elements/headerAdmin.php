<div class="fixed-top">
    <nav class="navbar navbar-expand-lg navbar-dark nav-transparent">
        <a class="navbar-brand" href="<?= base_url() ?>pages/index"><img src="<?= base_url(); ?>assets/img/logo-nav.png" alt="" class="img-fluid"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-sm-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url()."connection/logout";?>">Déconnexion</a>
                </li>
            </ul>
        </div>
    </nav>
</div>