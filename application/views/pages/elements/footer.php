
<!-- Footer -->
<footer class="page-footer font-small nav-transparent">

    <div>
      <div class="container">

        <!-- Grid row-->
        <div class="row py-4 d-flex align-items-center">

          <!-- Grid column -->
          <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
            <h6 class="mb-0">Suivez-nous sur les réseaux!</h6>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-6 col-lg-7 text-center text-md-right">

            <!-- Facebook -->
            <a href="https://www.facebook.com/IFOSUP-WAVRE-195112403868932/" class=" white-text mr-4" target="blank">
              <i class="fab fa-facebook-f"> </i>
            </a>
            <!--Linkedin -->
            <a href="https://www.linkedin.com/company/ifosup-wavre" class=" white-text mr-4" target="blank">
              <i class="fab fa-linkedin-in"> </i>
            </a>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row-->

      </div>
    </div>

    <!-- Footer Links -->
    <div class="container text-center text-md-left mt-5">

      <!-- Grid row -->
      <div class="row mt-3">

        <!-- 1e colonne -->
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">

          <!-- Content -->
          <h6>News</h6>
          <hr class="grey-text mb-4 mt-0 d-inline-block mx-auto">
          <p class="news px-3 text-justify"><div class="fb-page" data-href="https://www.facebook.com/pg/IFOSUP-WAVRE-195112403868932/posts/?ref=page_internal" data-tabs="timeline" data-height="260" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><blockquote cite="https://www.facebook.com/pg/IFOSUP-WAVRE-195112403868932/posts/?ref=page_internal" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pg/IFOSUP-WAVRE-195112403868932/posts/?ref=page_internal">IFOSUP WAVRE</a></blockquote></div></p>

          </div>

        <!-- Fin 1e colonne  -->

        <!-- 2e colonne -->
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

          <!-- Links -->
          <h6>Généralités</h6>
          <hr class="grey-text mb-4 mt-0 d-inline-block mx-auto">
          <p>
            <a href="<?= base_url(); ?>pages/roi">R.O.I</a>
          </p>
           <p>
            <a href="<?= base_url(); ?>pages/valorisation">Valorisation</a>
          </p>
          <p>
            <a href="<?= base_url(); ?>pages/ensinclusif">Enseignement inclusif</a>
          </p>
          <p>
            <a href="<?= base_url(); ?>pages/qualite">Qualité</a>
          </p>
        </div>
        <!-- Fin 2e colonne -->

        <!-- 3e colonne  -->
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

          <!-- Links -->
          <h6>Liens pratiques</h6>
          <hr class="grey-text mb-4 mt-0 d-inline-block mx-auto">
          <p>
            <a href="<?= base_url(); ?>pages/horaires">Horaires</a>
          </p>
          <p>
            <a href="<?= base_url(); ?>pages/congesScolaires">Congés scolaires</a>
          </p>
          <p>
            <a href="<?= base_url(); ?>pages/acces">Accès</a>
          </p>
          <p>
            <a href="<?= base_url(); ?>pages/liens">Liens</a>
          </p>

        </div>
        <!-- Fin 3e colonne -->

        <!-- 4e colonne -->
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

          <!-- Links -->
          <h6>Contact</h6>
          <hr class="grey-text mb-4 mt-0 d-inline-block mx-auto">
          <p>
            <i class="fa fa-home mr-3"></i> Rue de la limite 6, 1300 Wavre</p>
          <p>
            <i class="fa fa-envelope mr-3"></i><a href="mailto:info@ifosupwavre.be"> info@ifosupwavre.be</a></p>
          <p>
            <i class="fa fa-phone mr-3"></i> <a href="tel:010 22 20 26"> 010/22.20.26</a></p>
        </div>
        <!-- Fin 4e colonne -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3 black-text">
    <p>© 2018 Copyright:<a href="<?= base_url(); ?>pages/index" class="white-text"> Ifosup </a><a href="#" class="white-text">Mentions légales</a></p>
    <p>Réalisé par: <span class="white-text">Alexane Bella Fionda - David Houba - Benjamin Georges - Thierry Van Hiel - <a href="http://www.rbgraphics.be/" target="_blank" class="white-text">Romain Bourgeois</a> - <a href="mailto:reinepoire@gmail.com" class="white-text">Regina Nashi</a></span></p>
    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->
