<!DOCTYPE html>
<?php 
	if(isset($couleur)){
		echo '<html lang="fr" class=" nav-'.$couleur.'">';
	}else{
		echo '<html html lang="fr" class="nav-navy">';
	}
?>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>IFOSUP WAVRE | Institut de formation supérieure</title>
	<link rel="icon" type="image/png" href="<?= base_url(); ?>assets/img/ifosup-favicon.png" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" crossorigin="anonymous">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/mdb.min.css">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/style.css">
	<!--<script src="../assets/ckeditor/ckeditor.js"></script>-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery-resizable.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/trumbowyg.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/trumbowyg.upload.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/fr.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/trumbowyg.resizimg.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/trumbowyg.colors.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/trumbowyg.history.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/trumbowyg.table.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap-select.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/defaults-fr_FR.min.js"></script>



    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/trumbowyg.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/trumbowyg.colors.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/trumbowyg.table.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-select.min.css">


</head>
<body>
<div id="fb-root"></div>
<script async defer src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v3.2"></script>
<!--Debut du header-->
<header>

	<div class="fixed-top">
		<!--Mini-navigation -->
		<ul class="nav nav-tranparent font-small navbar-light nav-transparent" id="miniMenu">
			<!--Social media links-->
			<li class="nav-item">
				<a class="nav-link grey-text t-navy" target="blank" href="https://www.facebook.com/IF0OSUP-WAVRE-195112403868932/"><i class="fab fa-facebook-f"></i></a>
			</li>
			<li class="nav-item">
				<a class="nav-link grey-text t-navy" target="blank" href="https://www.linkedin.com/company/ifosup-wavre"><i class="fab fa-linkedin-in"></i></a>
			</li>
			<!--Social media links-->

			<li class="nav-item ml-auto">
				<a class="nav-link grey-text t-navy" href="#">Enseignant</a>
			</li>
			<li class="nav-item">
				<a class="nav-link grey-text t-navy" href="#">Etudiant</a>
			</li>
			<li class="nav-item">
				<a class="nav-link grey-text t-navy" href="#">E-fosup</a>
			</li>

		</ul>
		<!--Mini-navigation -->

		<!--Main navigation-->
		<nav class="navbar navbar-expand-lg navbar-dark nav-transparent">
			<a class="navbar-brand" href="<?= base_url() ?>pages/index"><img src="<?= base_url(); ?>assets/img/logo-nav.png" alt="" class="img-fluid"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ml-sm-auto">
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url(); ?>pages">Accueil</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url(); ?>pages/formations"><strong>Nos Formations</strong></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url(); ?>pages/inscriptions">S'inscrire</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url(); ?>pages/acces">Contact</a>
					</li>
				</ul>
			</div>
		</nav>
		<!-- Fin Main navigation-->
	</div>

