<!DOCTYPE html>
<html lang="fr" class="nav-green">


<section id="liens">
	<div class="row d-none d-sm-block d-print-none">
		<div class="col-md-12 liens"></div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="py-5 text-center">Liens - IFOSUP</h1>
			</div>
		</div>
	</div>
</section>

</header>
<!--Fin du header-->


<!--Content-->
<main class="pb-5">
	<div class="container">
		<div class="row py-5">
			<figure class="col-md-3 text-center"><a href="http://www.wavre.be/" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/logo_wavre.png" alt="Logo Ville de Wavre" title="Ville de Wavre" class="img-fluid">
					<figcaption class="text-center">Ville de Wavre</figcaption>
				</a></figure>

			<figure class="col-md-3 text-center"><a href="http://www.federation-wallonie-bruxelles.be/" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/logo_fwb.png" alt="Logo de la Fédération Wallonie-Bruxelles" title="Fédération Wallonie-Bruxelles" class="img-fluid">
					<figcaption class="text-center">Fédération Wallonie-Bruxelles</figcaption>
				</a></figure>

			<figure class="col-md-3 text-center"><a href="http://www.cpeons.be/page.asp?id=2&langue=FR" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/logo_cepeons.png" alt="Logo CPEONS" title="CPEONS" class="img-fluid">
					<figcaption class="text-center">CPEONS</figcaption>
				</a></figure>

			<figure class="col-md-3 text-center"><a href="http://www.cefora.be/" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/logo_cefora.png" alt="Logo Cefora" title="Cefora" class="img-fluid">
					<figcaption class="text-center">Cefora</figcaption>
				</a></figure>
		</div>

		<div class="row py-5">
			<figure class="col-md-3 text-center"><a href="http://www.adewavre.com/" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/logo_adewavre.png" alt="Logo ADE Wavre" title="ADE Wavre" class="img-fluid">
					<figcaption class="text-center">ADE Wavre</figcaption>
				</a></figure>

			<figure class="col-md-3 text-center"><a href="http://www.mirebw.be/" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/logo_mire.png" alt="Logo Mire Brabant Wallon" title="Mire Brabant Wallon" class="img-fluid">
					<figcaption class="text-center">Mire Brabant Wallon</figcaption>
				</a></figure>

			<figure class="col-md-3 text-center"><a href="https://www.leforem.be/" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/logo_forem.png" alt="Logo du Forem" title="Le Forem" class="img-fluid">
					<figcaption class="text-center">Le Forem</figcaption>
				</a></figure>

			<figure class="col-md-3 text-center"><a href="http://www.actiris.be/tabid/173/language/fr-BE/A-propos-d-Actiris.aspx" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/logo_actiris.png" alt="Logo Actiris" title="Actiris" class="img-fluid">
					<figcaption class="text-center">Actiris</figcaption>
				</a></figure>
		</div>

		<div class="row py-5">
			<figure class="col-md-3 text-center"><a href="http://www.vdab.be/francais" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/logo_vdab.png" alt="Logo VDAB" title="VDAB" class="img-fluid">
					<figcaption class="text-center">VDAB</figcaption>
				</a></figure>

			<figure class="col-md-3 text-center"><a href="https://www.jetbrains.com/" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/jetbrains.png" alt="Logo Jet Brains" title="Jet Brains" class="img-fluid">
					<figcaption class="text-center">Jet Brains</figcaption>
				</a></figure>

			<figure class="col-md-3 text-center"><a href="https://cursus.polelouvain.be/" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/cursus.jpg" alt="Logo Pôle Louvain - Cursus" title="Pôle Louvain - Cursus" class="img-fluid">
					<figcaption class="text-center">Pôle Louvain - Cursus</figcaption>
				</a></figure>

			<figure class="col-md-3 text-center"><a href="http://polelouvain.be/" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/logo_pole.png" alt="Logo Pôle Louvain" title="Pôle Louvain" class="img-fluid">
					<figcaption class="text-center">Pôle Louvain</figcaption>
				</a></figure>
		</div>

		<div class="row py-5">
			<figure class="col-md-3 text-center"><a href="http://wavre.rotary2170.org/club/" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/rotary.png" alt="Logo Rotary de Wavre" title="Rotary de Wavre" class="img-fluid">
					<figcaption class="text-center">Rotary de Wavre</figcaption>
				</a></figure>

			<figure class="col-md-3 text-center"><a href="https://be.jooble.org/" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/jooble.jpg" alt="Logo Jooble - recherche d'emploi" title="Jooble - recherche d'emploi" class="img-fluid">
					<figcaption class="text-center">Jooble - recherche d'emploi</figcaption>
				</a></figure>

			<figure class="col-md-3 text-center"><a href="http://www.enseignement.be/" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/enseignement.jpg" alt="Logo Enseignement.be" title="Enseignement.be" class="img-fluid">
					<figcaption class="text-center">Enseignement.be</figcaption>
				</a></figure>

			<figure class="col-md-3 text-center"><a href="http://www.fse.be/" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/FSE_ok.jpg" alt="Logo Fonds Social Européen" title="Fonds Social Européen" class="img-fluid">
					<figcaption class="text-center">Fonds Social Européen</figcaption>
				</a></figure>
		</div>

		<div class="row py-5">

			<figure class="col-md-3 text-center"><a href="http://www.lafeweb.be/" class="t-green">
					<img src="<?= base_url();?>assets/img/liens/feweb.jpg" alt="Logo Fédération des métiers du Web" title="Fédération des métiers du Web" class="img-fluid">
					<figcaption class="text-center">Fédération des métiers du Web</figcaption>
				</a></figure>
		</div>

	</div>
</main>

<!--Content-->


<script src="<?= base_url();?>assets/js/jquery.min.js"></script>
<script src="<?= base_url();?>assets/js/popper.min.js"></script>
<script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url();?>assets/js/mdb.min.js"></script>

</body>
</html>
