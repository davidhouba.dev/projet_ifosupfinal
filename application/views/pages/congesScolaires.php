<!DOCTYPE html>
<html lang="fr" class="nav-yellow">


<section id="conges">
	<div class="row d-none d-sm-block d-print-none">
		<div class="col-md-12 conges"></div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="py-5 text-center">Congés scolaires</h1>
			</div>
		</div>
	</div>
</section>

</header>
<!--Fin du header-->


<!--Content-->
<main class="pb-5">
	<div class="container">
		<div class="row py-5">
			<div class="col-md-12 yellow-border">
				<h2>Année académique 2018-2019</h2>
				<ul>
					<li><span class="h5">Rentrée scolaire :</span> lundi 3 septembre 2018</li>
					<li><span class="h5">Fête de la Communauté française :</span> jeudi 27 septembre 2018</li>
					<li><span class="h5">Congé d'automne (Toussaint) :</span> du lundi 29 octobre 2018 au samedi 3 novembre 2018</li>
					<li><span class="h5">Vacances d'hiver (Noël) :</span> du lundi 24 décembre 2018 au samedi 5 janvier 2019</li>
					<li><span class="h5">Congé de détente (Carnaval) :</span> du lundi 4 mars 2019 au samedi 9 mars 2019</li>
					<li><span class="h5">Vacances de printemps (Pâques) :</span> du lundi 8 avril 2019 au lundi 22 avril 2019</li>
					<li><span class="h5">Fête du 1er mai :</span> mercredi 1er mai 2019</li>
					<li><span class="h5">Ascension :</span> jeudi 30 mai 2019</li>
					<li><span class="h5">Lundi de Pentecôte :</span> lundi 10 juin 2019</li>
					<li><span class="h5">Les vacances d'été débutent le </span> lundi 1er juillet 2019</li>
				</ul>
			</div>
		</div>
	</div>
</main>

<!--Content-->



<script src="<?= base_url();?>assets/js/jquery.min.js"></script>
<script src="<?= base_url();?>assets/js/popper.min.js"></script>
<script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url();?>assets/js/mdb.min.js"></script>

</body>
</html>
