
<section>
	<div class="row d-none d-sm-block d-print-none">
		<div class="col-md-12 " style="min-height: 40vh;width: 100%;background-repeat: no-repeat;background-size: cover;background-position: center; background-image: url(<?= base_url().$affiche_cours_front[0]['header_path']; ?>);"></div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h1 class="py-5 text-center"><?= $affiche_cours_front[0]['nom'];?></h1>
			</div>
		</div>
	</div>
</section>

</header>
<!--Fin du header-->


<!--  Breadcrumbs -->
<nav aria-label="breadcrumb" class="px-5">
	<ol class="breadcrumb">
		<li class="breadcrumb-item arrow-yellow">
			<a class="grey-text t-yellow" href="<?= base_url() ?>pages/index">Accueil</a>
		</li>
		<li class="breadcrumb-item arrow-yellow">
			<a class="grey-text t-yelow" href="<?= base_url() ?>pages/formations">Nos formations</a>
		</li>
		<li class="breadcrumb-item arrow-yellow">
			<a class="grey-text t-yellow" href="<?= base_url() ?>pages/section?idSec=<?= $affiche_cours[0]['idSection'];?>"><?= $affiche_cours[0]['nomSection']; ?></a>
		</li>
		<li class="breadcrumb-item active arrow-yellow">
			<a class="t-yellow" href="<?= base_url() ?>pages/cours_front?idCours=<?= $affiche_cours[0]['idCours']; ?>"><?= $affiche_cours[0]['nomCours']; ?></a>
		</li>
	</ol>
</nav>
<!--  Breadcrumbs -->
<!--Content-->
<main class="pb-5">
	<div class="container">
		<div class="d-flex align-items-center py-5">
			<div class="col-md-7 yellow-border">
				<h3>Description</h3>
				<p><?= $affiche_cours_front[0]['description']; ?></p>
			</div>

			<div class="col-md-5">
				<!-- Card secondaire-->
				<div class="card">
					<!-- Card header -->
					<h5 class="card-header text-center ch-yellow">Organisation du cours</h5>

					<!-- Card content -->
					<div class="card-body">

						<!-- Title -->
						<!-- Text -->
						<ul class="card-text list-group list-group-flush py-3">
						<?php foreach($affiche_aside as $affiche_aside) : ?>
							<li class="list-group-item"><a href="<?= base_url().$affiche_aside['lien']; ?>" target="_blank" class="grey-text t-yellow"><?= $affiche_aside['texte']; ?></a></li>
						<?php endforeach; ?>
						</ul>
					</div>

				</div>
				<!-- Card -->

			</div>
		</div>
		<!--    Fin de la row       -->
		<div class="row">
			<div class="col-md-12">
				<h3>Pré-requis</h3>
				<p><?= $affiche_cours_front[0]['pre_requis']; ?></p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<h3>Documents utiles</h3>
				<p><?= $affiche_cours_front[0]['documents_utiles']; ?></p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 ">
				<h3>Exemptions</h3>
				<?= $affiche_cours_front[0]['exemption']; ?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 ">
				<h3>Observation</h3>
				<p><?= $affiche_cours_front[0]['observation']; ?></p>
			</div>
		</div>
	</div>

</main>

<!--Content-->


<script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url() ?>assets/js/popper.min.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/mdb.min.js"></script>

</body>
</html>
