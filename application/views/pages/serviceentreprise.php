

 <section id="ServiceEntreprise"> 
    <div class="row d-none d-sm-block d-print-none">
           <div class="col-md-12 d-print-none" style="min-height: 40vh;width: 100%;background-repeat: no-repeat;background-size: cover;background-position: center; background-image: url(<?= base_url().$afficheServiceEntreprise[0]['header_path']; ?>);"></div>
    </div>
    <div class="container">
        <div class="row">
		<?php
        foreach ($afficheServiceEntreprise as $afficheServiceEntreprise) :
        ?>
            <div class="col-md-12">
                <h1 class="py-5 text-center"><?= $afficheServiceEntreprise['nom']; ?></h1>
            </div>
        </div>
     </div>       
</section> 

</header>
<!--Fin du header-->  

<!--  Breadcrumbs -->
  <nav aria-label="breadcrumb" class="px-5">
    <ol class="breadcrumb">
      <li class="breadcrumb-item arrow-yellow">
         <a class="grey-text t-yellow" href="<?= base_url() ?>pages/index">Accueil</a>
      </li>
      <li class="breadcrumb-item arrow-yellow">
         <a class="grey-text t-yellow" href="<?= base_url() ?>pages/formations">Nos formations</a>
      </li>
       <li class="breadcrumb-item arrow-yellow active">
         <a class="t-yellow" href="<?= base_url() ?>pages/serviceentreprise?idSec=6"><?= $affiche_cat[0]['nomSection'];?></a>
      </li>
    </ol>
  </nav>
<!--  Breadcrumbs -->

<!--Content-->
<main class="pb-5">  
    <div class="container">
           <div class="row pb-5">
               <div class="col-md-12">
                   <?= $afficheServiceEntreprise['description']; ?>
                   <p class="h4"><a href="https://docs.google.com/forms/d/e/1FAIpQLSe5Em7HzWGvoBumd6j-_XsJn9dk7-k7CWXN3jMiiJRqDUrkfg/viewform">Pré-inscription</a></p>
               </div>
           </div>        
    </div>
	<?php
    endforeach;
    ?>
</main>

<!--Content-->


	<script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
	<script src="<?= base_url() ?>assets/js/popper.min.js"></script>
	<script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
	<script src="<?= base_url() ?>assets/js/mdb.min.js"></script>

</body>
</html>
