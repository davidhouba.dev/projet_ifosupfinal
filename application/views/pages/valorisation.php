<!DOCTYPE html>
<html lang="fr" class="nav-blue">

<section id="valorisation">
	<div class="row d-none d-sm-block d-print-none">
		<div class="col-md-12 valorisation"></div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="py-5 text-center">Valorisation <br>IFOSUP</h1>
			</div>
		</div>
	</div>
</section>

</header>
<!--Fin du header-->

<!--CONTENT-->
<main class="valorisation pb-5">
	<div class="container">
		<div class="d-flex">
			<div class="col-md-8">
				<p>L’étudiant peut introduire un dossier de demande de valorisation des acquis (VA) sur base de compétences développées : </p>
				<ul>
					<li>dans un autre établissement d’enseignement ;</li>
					<li>dans le cadre d’une formation hors enseignement ;</li>
					<li>dans le cadre de son expérience professionnelle ;</li>
					<li>dans le cadre de son expérience personnelle.</li>
				</ul>
				<p>Et ce en vue de solliciter :</p>
				<ul>
					<li>Son <a href="docs/ANNEXE6.docx" class="t-blue" target="_blank">admission</a> dans une formation alors qu’il ne possède pas les titres requis tels que mentionnés au dossier pédagogique ;</li>
					<li>La <a href="docs/ANNEXE7.docx" class="t-blue" target="_blank">dispense</a> à une partie d’un cours où il s’inscrit ;</li>
					<li>La <a href="docs/ANNEXE8.docx" class="t-blue" target="_blank">sanction</a> (réussite) d’une unité d’enseignement composant une section ; UE dans laquelle il ne devra dès lors pas s’inscrire.</li>
				</ul>
				<p>En vue de bénéficier de ces mesures ; l’étudiant devra, <strong>à l’exclusion de toute autre procédure</strong> introduire un dossier de « demande de valorisation ».</p>
				<p>Ce dossier sera considéré comme recevable s’il est introduit <bold>avant le 31 octobre</bold> de l’année scolaire considérée. Il comprendra :</p>
				<ul>
					<li>Le formulaire de demande dûment complété ;</li>
					<li>Copie de tous les documents nécessaires à la prise de décision par le conseil des études (bulletins, attestations de réussite, fiches descriptives des cours suivis, tables des matières, CV, diplômes, attestations de suivi relatives à des formations hors enseignement, attestations des employeurs,…)</li>
				</ul>
				<p>Le dossier complet, <strong>incluant une copie recto-verso de la carte d’identité</strong> doit être déposé au secrétariat. Les documents illisibles et envois de courriers électroniques ne seront pas pris en considération.</p>
				<p>Le formulaire d’introduction de la demande peut être imprimé à partir de notre site www.ifosupwavre.be sous la rubrique : « validation des acquis » ou peut être retiré au secrétariat pendant les heures d’ouverture de celui-ci.</p>
				<p><strong>Il est à noter que le conseil des études conserve la prérogative de soumettre l’étudiant à une évaluation (examen écrit, oral,…)</strong>. Dans ce cas, celle-ci se déroule avant le premier dixième de l’unité d’enseignement pour laquelle la valorisation a été demandée.</p>
				<p>L’examen du dossier fourni par l’étudiant portera sur la correspondance entre les éléments qui le composent et les acquis d’apprentissage ou capacités préalables requises, listés au dossier pédagogique de chacune des unités d’enseignement concernées.</p>
				<p>Si les documents produits mentionnent une cote, en cas d’avis favorable du conseil des études, celle-ci sera automatiquement reportée. Si la procédure se base sur des documents qui ne mentionnent aucune cotation, il sera soit procédé à une épreuve, soit appliqué une cote moyenne de 60%.</p>
				<p>La procédure de valorisation ne peut pas être appliquée à l’épreuve intégrée d’une section. </p>
				<p>Lors de la valorisation du stage, l’étudiant ne devra pas prester les heures de stage, mais est tenu de rédiger un rapport. Il doit donc y être inscrit.</p>

				<p>Pour l’enseignement supérieur exprimé en crédits (ECTS) :</p>
				<ul>
					<li>Le nombre d’ECTS valorisés ne peut dépasser ceux validés dans une autre forme d’enseignement ;</li>
					<li>En BES, sauf dérogation, seuls 60 ECTS peuvent être valorisés ;</li>
					<li>En Bachelier, la valorisation maximum est de 120 ECTS.</li>
				</ul>
				<p>Tout étudiant qui s’inscrit à l’épreuve intégrée doit avoir suivi un minimum de 30 ECTS dans l’établissement.</p>

				<h4>Cas particulier</h4>
				<p>Dans l’enseignement supérieur, il faut distinguer le cas spécifique de l’étudiant qui peut se prévaloir de 5 ans d’expérience dans le domaine considéré (dont maximum 2 ans justifiés par des études). Cet étudiant a la possibilité de valoriser entre 60 (minimum) et 120 ECTS (maximum).</p>
				<p>L’étudiant qui pense pouvoir valoriser plus de 60 ECTS et souhaite présenter l’épreuve intégrée d’une section d’enseignement supérieur, peut composer un <a href="docs/ANNEXE9.docx" class="t-blue" target="_blank">dossier de valorisation</a> qu’il défendra oralement devant le conseil des études. </p>
				<p>Ce dossier doit impérativement reprendre : un CV, une lettre de motivation, la preuve des formations suivies (dans l’enseignement et hors enseignement), des attestations d’employeurs relevant explicitement une expérience professionnelle probante dans la matière. Le dossier doit prouver que l’étudiant a les compétences suffisantes pour valoriser les acquis d’apprentissage des différentes UEs constitutives de la section. Sur base du dossier écrit et de sa pertinence, une date sera fixée pour l’épreuve orale.</p>

				<p>A l’issue de la défense orale, le conseil des études prend l’une des décisions suivantes :</p>
				<ul>
					<li>Accorder la valorisation en reliant cette épreuve aux acquis d’apprentissage de diverses UE pour sanctionner la réussite de 60 à 120 ECTS (90 ECTS en BES);</li>
					<li>Déclarer que le dossier n’est pas probant et exiger une ou des épreuves pour valoriser un maximum de 60 ECTS ;</li>
					<li>Refuser toute valorisation.</li>
				</ul>
				<p><strong>Dans l’attente de la décision du Conseil des études, l’étudiant n’est pas dispensé du/des cours et est donc tenu d’y participer avec assiduité.</strong></p>
				<p>Il est à noter que la valorisation ne consiste en aucun cas le moyen d’obtenir une troisième session et que les décisions du Conseil des études en la matière sont définitives. </p>
				<p>Personnes de référence pour la valorisation : Mme Karin LOVIBOND (<a href="mailto:conseiller.formation@ifosupwavre.be" class="t-blue">conseiller.formation@ifosupwavre.be</a>), Conseillère à la formation, Mme Valérie VANDERAVERO, Directrice.</p>

			</div>






			<div class="col-md-4 text-center">
				<a class="btn btn-blue" target="_blank" href="https://drive.google.com/drive/folders/19epAZTL4V4Q03injMsVhDITCZafYeK-Y">Dossiers pédagogiques</a>
			</div>
		</div>
	</div>
</main>

<!--CONTENT-->

<script src="<?= base_url();?>assets/js/jquery.min.js"></script>
<script src="<?= base_url();?>assets/js/popper.min.js"></script>
<script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>

</body>
</html>
