<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container mt-5 pt-2">
<?php if(isset($cours)):?>
<h2>Edition d'un cours</h2>

<form id="form" method="post">
    <?php if($cours->id): ?>
    <input name="id" value="<?=$cours->id?>" hidden>
    <?php endif;?>
    <p>
        <label for="nom">Nom :</label>
        <input id="nom" name="nom" type="text" class="ml-2 col-sm-2">
    </p>


    <p>
        <label for="header">Header du cours (laissez vide pour ne pas le changer):</label>
        <input id="header" name="header" type="file" class="form-control-file" accept="image/*">
    </p>

    <p>
        <label for="section">Section :</label>
        <select id="section" name="section" class="selectpicker" title="Sélectionner une option" data-width="fit">
            <?php foreach ($sections as $section): ?>
                <option value="<?= $section->id ?>"><?= $section->nom ?></option>
            <?php endforeach; ?>
        </select>
    </p>

    <p>
        <label for="description">Description :</label>
        <textarea id="description" name="description" class="trumbowyg">

        </textarea>
    </p>
    <p>
        <label for="pre_requis">Pré-requis :</label>
        <textarea id="pre_requis" name="pre_requis" class="trumbowyg">

    </textarea>
    </p>
    <p>
        <label for="documents_utiles">Documents utiles :</label>
        <textarea id="documents_utiles" name="documents_utiles" class="trumbowyg">

    </textarea>
    </p>
    <p>
        <label for="exemption">Exemption :</label>
        <textarea id="exemption" name="exemption" class="trumbowyg">

    </textarea>
    </p>
    <p>
        <label for="observation">Observation :</label>
        <textarea id="observation" name="observation" class="trumbowyg">

    </textarea>
    </p>
    <div data-name="aside">
        <h3>Aside :</h3>
        <?php if(isset($cours->aside->aside_texte) && is_array($cours->aside->aside_texte) && count($cours->aside->aside_texte) > 0):?>
        <?php for($i = 1; $i <= count($cours->aside->aside_texte); $i++): ?>
        <div data-name="box">
            <textarea name="ligneaside_old_<?=$cours->aside->aside_ordre_texte[$i-1];?>" class="trumbowyg"></textarea>
            <p>
                Fichier associé à cette ligne: <input name="asidefile_old_<?=$cours->aside->aside_ordre_texte[$i-1];?>" type="file">
                <?php if($cours->aside->aside_lien[$i-1] !== "..."): ?>
                <button type="button" class="btn btn-light" data-name="deleteFichier" value="<?=$cours->aside->aside_lien[$i-1]?>" data->Supprimer le fichier associé</button>
                <?php endif;?>
                <button type='button' class="btn btn-light" data-name="deleteLigne">Supprimer cette ligne</button>
            </p>
            <div class="alert alert-danger" role="alert" name="errorDeleteDoc" style="display: none;">
                Le document lié n'a pas pu être supprimé. Si le problème persiste, contactez un administrateur.
            </div>
            <div class="alert alert-success" role="alert" name="successDeleteDoc" style="display: none;">
                Suppression réussie !
            </div>
        </div>
        <?php endfor;?>
        <?php endif;?>
        <button type="button" id="ajouteLigneAside" class="btn btn-light">Ajouter une ligne</button>
    </div>
    <div class="col-sm-8 mx-auto mt-4 row">
        <button type="submit" id="submit" name="submit"class="btn btn-blue btn-block col-sm-4 mr-4 mb-4">Envoyer</button>
        <a href="<?= base_url().'cours/listecours'?>" class="btn btn-light btn-block col-sm-4 mb-4">Retour</a>
    </div>
</form>

<div class="alert alert-success" role="alert" name="successUpdateCours" style="display: none;">
    Le cours a été mis à jour ! Vous serez redirigé automatiquement à la liste des cours dans 3 secondes.
</div>
<div class="alert alert-danger" role="alert" name="errorUpdateCours" style="display: none;">
    Le cours n'a pas pu être mis à jour. Si le problème persiste, contactez un administrateur système.
</div>

<script>
    $(document).ready(function(){

        var nbrligne = <?= isset($cours->aside->aside_texte) ? count($cours->aside->aside_texte) : 1 ?>;

        $("#nom").val(`<?=$cours->nom?>`);
        $("#section").selectpicker('val', `<?=$cours->section?>`);

        $("#ajouteLigneAside").click(function(){
            nbrligne++;
            var $box = $("<div data-name='box'></div>");

            var $textarea = $("<textarea name='ligneaside_new_"+nbrligne+"'class=\"trumbowyg\"></textarea>");
            $box.append($textarea);
            $textarea.trumbowyg(config);
            $textarea.on('tbwchange', changeEditorInput);

            var $paragraphe = $("<p>Fichier associé à cette ligne: <input name=\"asidefile_new_"+nbrligne+"\" type=\"file\" disabled></p>");
            $box.append($paragraphe);

            var $deleteButton = $("<button type='button' class='btn btn-light' data-name='deleteLigne'>Supprimer cette ligne</button>");
            $deleteButton.click(deleteLigne);


            $box.children().last().after($deleteButton);
            $("div[data-name=aside] #ajouteLigneAside").before($box);

        });

        function deleteLigne(event){
            event.preventDefault();
            var $target = $(this).parent();
            $target.fadeOut(400, function(){
                $target.remove();
            });
        }


        $.trumbowyg.svgPath = '<?= base_url(); ?>assets/css/icons.svg';
        var config = {
            lang: 'fr',
            btnsDef: {
                // Create a new dropdown
                image: {
                    dropdown: ['insertImage', 'upload'],
                    ico: 'insertImage'
                }
            },
            // Redefine the button pane
            btns: [
                ['historyUndo','historyRedo'],
                ['formatting'],
                ['strong', 'em', 'underline'],
                ['superscript', 'subscript'],
                ['link'],
                ['image'], // Our fresh created dropdown
                /*  */           ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                ['unorderedList', 'orderedList'],
                ['horizontalRule'],
                ['removeformat'],
                ['foreColor', 'backColor'],
                ['fullscreen'],
                ['table']
            ],
            plugins: {
                // Add imagur parameters to upload plugin for demo purposes
                upload: {
                    serverPath: "<?= base_url();?>index.php/upload/image",
                    urlPropertyName: 'url'
                },
                resizimg : {
                    minSize: 20,
                    step: 1,
                },
                table: {
                    rows:7,
                    columns:7,
                    styler:'table',
                }
            }
        };
        $('.trumbowyg').trumbowyg(config);

        $('#description').trumbowyg('html', `<?=$cours->description?>`);
        $('#pre_requis').trumbowyg('html', `<?=$cours->pre_requis?>`);
        $('#documents_utiles').trumbowyg('html', `<?=$cours->documents_utiles?>`);
        $('#exemption').trumbowyg('html', `<?=$cours->exemption?>`);
        $('#observation').trumbowyg('html', `<?=$cours->observation?>`);

        <?php if(isset($cours->aside->aside_texte) && is_array($cours->aside->aside_texte) && count($cours->aside->aside_texte) > 0):?>
        <?php for($i = 0; $i < count($cours->aside->aside_texte); $i++): ?>
            $("div[data-name=box]:eq(<?=$i?>) textarea.trumbowyg").trumbowyg('html', `<?=$cours->aside->aside_texte[$i]?>`);
        <?php endfor;?>
        <?php endif;?>

        function changeEditorInput(){
            $textarea = $(this);
            var content = $textarea.trumbowyg('html');
            var bool = content === "";
            $textarea.parentsUntil("div[data-name=box]").parent().find("input").prop("disabled", bool);
        }

        $('div[data-name=aside] textarea.trumbowyg').on('tbwchange', changeEditorInput);

        $('div[data-name=aside] button[data-name=deleteFichier]').click(function(e){
           e.preventDefault();
           var $button = $(this);
           var filePath = $button.val();

            $.ajax({
                method: 'POST',
                url: '<?=base_url().'cours/deleteFichierCours'?>',
                data:{
                    file: filePath,
                    id_cours : $("input[name=id]").val(),
                    lien : $button.val()
                },
                error: function(){
                    $message = $button.parent().parent().find("div[name=errorDeleteDoc]");
                    $message.fadeIn(400, function(){
                        setTimeout(function(){
                            $message.fadeOut();
                        },3000)
                    });
                },
                success: function(){
                    $message = $button.parent().parent().find("div[name=successDeleteDoc]");
                    $message.fadeIn(400, function(){
                        setTimeout(function(){
                            $message.fadeOut();
                            $button.fadeOut();
                        },3000)
                    });
                }
            });
        });

        $("div[data-name=box] button[data-name=deleteLigne]").click(deleteLigne);

        $('#form').submit(function(e){
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                method: 'POST',
                url: '<?=base_url().'cours/updateCours'?>',
                data: formData,
                processData: false,
                contentType: false,
                error: function(){
                    $('div[name=errorUpdateCours]').fadeIn(400, function(){
                        setTimeout(function(){
                            $('div[name=errorUpdateCours]').fadeOut();
                        }, 3000)
                    })
                },
                success: function(){
                    $('div[name=successUpdateCours]').fadeIn(400, function(){
                        setTimeout(function(){
                            window.location = '<?= base_url()."cours/listecours"?>';
                        }, 3000)
                    });
                }
            });

        });

    });
</script>
<?php endif;?>
