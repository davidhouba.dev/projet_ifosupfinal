<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container pt-5 mt-2">
    <a href="<?= base_url(); ?>index.php/Connection/login" class="btn btn-blue mt-5">
        <i class="fas fa-align-justify pr-2"></i> Panel administrateur</a>
    <?php if(isset($sections)):?>
        <ul>
            <?php foreach($sections as $s): ?>
            <li class="nav">
                    <div class="nav-item col-sm-4 py-3"><span><?= $s->nom ?> </span></div>
                    <div class="nav-item col-sm-2">
                        <a href="<?=base_url()."section/updateSection/".$s->id?>">
                            <button class="btn btn-blue">Update</button>
                        </a>
                    </div>
                </li>
            <?php endforeach;?>
        </ul>
    <?php endif;?>
</div>
