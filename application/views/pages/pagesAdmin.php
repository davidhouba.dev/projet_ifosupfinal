<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container mt-5 pt-2">
    <a href="<?= base_url(); ?>index.php/Connection/login" class="btn btn-blue mt-5">
        <i class="fas fa-align-justify pr-2"></i> Panel administrateur</a>
    <?php if(isset($pagesListe)):?>
        <ul>
            <?php foreach($pagesListe as $p): ?>
                <li class="nav">
                    <div class="nav-item col-sm-5 py-3"><span><?= $p->nom ?> </span></div>
                    <div class="nav-item col-sm-2">
                        <a href="<?=base_url()."pagesAdmin/updatePage/".rawurlencode($p->nom)?>">
                            <button class="btn btn-blue">Update</button>
                        </a>
                    </div>
                </li>
            <?php endforeach;?>
        </ul>
    <?php endif;?>
</div>
