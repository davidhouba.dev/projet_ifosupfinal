<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>



<div class="container mt-5 pt-2">
    <?php if(isset($page)):?>
        <h2>Edition d'un cours</h2>

        <form id="form" method="post">
            <?php if($page->nom): ?>
                <input name="oldnom" value="<?=$page->nom?>" hidden>
            <?php endif;?>
            <p>
                <label for="nom" value="<?=$page->nom?>">Nom :</label>
                <input id="nom" name="nom" type="text" class="ml-2 col-sm-4">
            </p>

            <p>
                <label for="contenu">Contenu :</label>
                <textarea id="contenu" name="contenu" class="trumbowyg">
                </textarea>
            </p>
            <div class="col-sm-8 mx-auto mt-4 row">
                <button type="submit" id="submit" name="submit"class="btn btn-blue btn-block col-sm-4 mr-4 mb-4">Envoyer</button>
                <a href="<?= base_url().'PagesAdmin/listePages'?>" class="btn btn-light btn-block col-sm-4 mb-4">Retour</a>
            </div>
        </form>

        <div class="alert alert-success" role="alert" name="successUpdatePage" style="display: none;">
            La page a été mise à jour ! Vous serez redirigé automatiquement à la liste des pages dans 3 secondes.
        </div>
        <div class="alert alert-danger" role="alert" name="errorUpdatePage" style="display: none;">
            La pase n'a pas pu être mise à jour. Si le problème persiste, contactez un administrateur système.
        </div>
    <?php endif; ?>
</div>

<script>
    $.trumbowyg.svgPath = '<?= base_url(); ?>assets/css/icons.svg';
    var config = {
        lang: 'fr',
        btnsDef: {
            // Create a new dropdown
            image: {
                dropdown: ['insertImage', 'upload'],
                ico: 'insertImage'
            }
        },
        // Redefine the button pane
        btns: [
            ['historyUndo','historyRedo'],
            ['formatting'],
            ['strong', 'em', 'underline'],
            ['superscript', 'subscript'],
            ['link'],
            ['image'], // Our fresh created dropdown
            /*  */           ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['foreColor', 'backColor'],
            ['fullscreen'],
            ['table']
        ],
        plugins: {
            // Add imagur parameters to upload plugin for demo purposes
            upload: {
                serverPath: "<?= base_url();?>index.php/upload/image",
                urlPropertyName: 'url'
            },
            resizimg : {
                minSize: 20,
                step: 1,
            },
            table: {
                rows:7,
                columns:7,
                styler:'table',
            }
        }
    };
    $('.trumbowyg').trumbowyg(config);

    $("#nom").val(`<?=$page->nom?>`);
    $('#contenu').trumbowyg('html', `<?=$page->contenu?>`);

    $('#form').submit(function(e){
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            method: 'POST',
            url: '<?=base_url().'pagesAdmin/updatePage'?>',
            data: formData,
            processData: false,
            contentType: false,
            error: function(){
                $('div[name=errorUpdatePage]').fadeIn(400, function(){
                    setTimeout(function(){
                        $('div[name=errorUpdatePage]').fadeOut();
                    }, 3000)
                })
            },
            success: function(){
                $('div[name=successUpdatePage]').fadeIn(400, function(){
                    setTimeout(function(){
                        window.location = '<?= base_url()."pagesAdmin/listepages"?>';
                    }, 3000)
                });
            }
        });
    });
</script>
