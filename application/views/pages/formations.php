
 <section id="formations"> 
    <div class="row d-none d-sm-block d-print-none">
           <div class="col-md-12 formations"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
				<?php
				foreach($afficheNosFormations as $afficheNosFormations) :
				?>
				<h1 class="py-5 text-center"><?= $afficheNosFormations['nom'];?></h1>
				<?php
				endforeach;
				?>
            </div>
        </div>
     </div>       
</section> 

</header>
<!--Fin du header-->  

<!--Content-->

<!--  Breadcrumbs -->
  <nav aria-label="breadcrumb" class="px-5">
    <ol class="breadcrumb">
      <li class="breadcrumb-item arrow-blue">
         <a class="grey-text t-blue" href="<?= base_url(); ?>pages">Accueil</a>
      </li>
      <li class="breadcrumb-item active arrow-blue">
         <a class="t-blue" href="<?= base_url(); ?>pages/formations">Nos formations</a>
      </li>
    </ol>
  </nav>
<!--  Breadcrumbs -->
<section class="pb-5">  
       <div class="container">      
        <!-- L'ensemble de cartes-->
        
          <!-- Card secondaire-->
           <div class="row py-3">
            <div class="col-md-12">
             <div class="card">
                      <!-- Card header -->
                      <!-- Title -->
				 <?php
				 foreach($afficheTitreSecondaire as $afficheTitreSecondaire) :
				 ?>
                      <h5 class="card-header text-center ch-blue"><a href="<?= base_url() ?>pages/section?idSec=1" class="black-text"><?= $afficheTitreSecondaire['nom'];?></a></h5>
				 <?php
				 endforeach;
				 ?>
                      <!-- Card content -->
                      <div class="card-body">

                        

                        <!-- Text -->
						  
                        <ul class="card-text list-group list-group-flush py-3">
                        <?php foreach ($afficheLiensSecondaires as $afficheLiensSecondaires) : ?>
                          <li class="list-group-item"><a href="<?= base_url() ?>pages/cours_front?idCours=<?= $afficheLiensSecondaires['idSecondaire']; ?> " class="grey-text t-blue"><?= $afficheLiensSecondaires['nomSecondaire'];?></a></li>
                        <?php endforeach; ?>
                        </ul>
                      </div>

            </div>
            </div>
            </div>
            <!-- Card -->
   
            <!-- Card superieur --> 
            <div class="row py-3">
            <div class="col-md-12">         
             <div class="card">
                      <!-- Card header -->
				 <?php
				 foreach($afficheTitreSuperieur as $afficheTitreSuperieur) :
				 ?>
                      <h5 class="card-header text-center ch-blue"><a href="<?= base_url(); ?>pages/section?idSec=2" class="black-text"><?= $afficheTitreSuperieur['nom'];?></a></h5>
				 <?php
				 endforeach;
				 ?>
                      <!-- Card content -->
                      <div class="card-body">

                        <!-- Title -->
                        <h5 class="card-title">Nos bacheliers</h5>
                        <!-- Text -->
						
                        <ul class="card-text list-group list-group-flush py-3">
                        <?php foreach ($afficheLiensSuperieurBac as $afficheLiensSuperieurBac) : ?>
                           <li class="list-group-item"><a href="<?= base_url(); ?>pages/cours_front?idCours=<?= $afficheLiensSuperieurBac['idSupérieur'];?>" class="grey-text t-blue"><?= $afficheLiensSuperieurBac['nomSuperieur'];?></a></li>
                      <?php endforeach; ?>
                        </ul> 
                        
                        <!-- Title -->
                        <h5 class="card-title">Nos brevets de l'enseignement supérieur</h5>
                        <!-- Text -->
                        <ul class="card-text list-group list-group-flush py-3">
                        <?php foreach($afficheLiensSuperieurBes as $afficheLiensSuperieurBes) : ?>
						              <li class="list-group-item"><a href="<?= base_url(); ?>pages/cours_front?idCours=<?= $afficheLiensSuperieurBes['idSupérieurBes'];?>" class="grey-text t-blue"><?= $afficheLiensSuperieurBes['nomSuperieurBes'];?></a></li>
                        <?php endforeach; ?>
                        </ul> 
                      </div>
              </div> 
            </div>  
        </div>      
            <!-- Fin Card superieur -->
            
            <!-- Card langues -->
            <div class="row py-3"> 
            <div class="col-md-12">         
             <div class="card">
				 <?php
				 foreach($afficheTitreLangue as $afficheTitreLangue) :
					 ?>
                      <!-- Card header -->
                      <h5 class="card-header text-center ch-blue"><a href="<?= base_url(); ?>pages/langues?idSec=3" class="black-text"><?= $afficheTitreLangue['nom'];?></a></h5>
				 <?php
				 endforeach;
				 ?>
                      <!-- Card content -->
                      <div class="card-body">

                        <!-- Title -->
                        <h5 class="card-title">Nos cours</h5>
                        <!-- Text -->
                        <ul class="card-text list-group list-group-flush py-3">
                        <?php foreach($afficherLiensLangues as $afficherLiensLangues) : ?>
                           <li class="list-group-item"><a href="<?= base_url().$afficherLiensLangues['lien']; ?>" target="_blank" class="grey-text t-blue"><?= $afficherLiensLangues['texte']; ?></a></li>
                          <?php endforeach; ?> 
                        </ul>  
                      </div>
              </div> 
            </div>
        </div>        
            <!-- Fin Card langues -->
            
            <!-- Card modules info -->
            <div class="row py-3">
            <div class="col-md-12">         
             <div class="card">
                      <!-- Card header -->

                      <h5 class="card-header text-center ch-blue">Modules informatiques</h5>

                      <!-- Card content -->
                      <div class="card-body">

                        <!-- Title -->
                        <h5 class="card-title">Nos modules</h5>
                        <!-- Text -->
                        <ul class="card-text list-group list-group-flush py-3">
							<?php
							foreach($afficheDecouverte as $afficheDecouverte) :
							?>
                            <li class="list-group-item"><a href="<?= base_url(); ?>pages/decouverte?idSec=4" class="grey-text t-blue"><?= $afficheDecouverte['nom'];?></a></li>
							<?php
							endforeach;
							?>
							<?php
							foreach($affichePerfection as $affichePerfection) :
								?>
                            <li class="list-group-item"><a href="<?= base_url(); ?>pages/perfection?idSec=5" class="grey-text t-blue"><?= $affichePerfection['nom'];?></a></li>
							<?php
							endforeach;
							?>
                        </ul>  
                      </div>
              </div>
            </div> 
        </div>        
            <!-- Fin Card module info -->
  
  
        <!-- L'ensemble de cartes-->
                
    </div>

</section>

<!--Content-->

	<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/mdb.min.js"></script>

</body>
</html>
