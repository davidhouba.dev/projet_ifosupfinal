<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container mt-5 pt-2">
    <?php if(isset($module)):?>
        <h2>Edition d'un cours</h2>

        <form id="form" method="post">
            <?php if($module->id): ?>
                <input name="id" value="<?=$module->id?>" hidden>
            <?php endif;?>
            <p>
                <label for="nom">Nom :</label>
                <input id="nom" name="nom" type="text" class="form-control" value="<?=$module->nom?>" required>
            </p>

            <?php if(isset($sections)): ?>
                <p class="form-group">
                    <label for="section">Section:</label>
                    <select class="form-control selectpicker" id="section" name="section">
                        <?php foreach ($sections as $section): ?>
                            <option value="<?= $section->id ?>"><?= $section->nom ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
            <?php endif; ?>

            <p class="form-group">
                <label for="jour">Jour:</label>
                <select id="jour" name="jour" class="selectpicker form-control">
                    <option value="0">Lundi</option>
                    <option value="1">Mardi</option>
                    <option value="2">Mercredi</option>
                    <option value="3">Jeudi</option>
                    <option value="4">Vendredi</option>
                    <option value="5">Samedi</option>
                    <option value="6">Dimanche</option>
                </select>
            </p>

            <p class="form-group">
                <label for="date_debut">Date de début:</label>
                <input class="form-control" type="date" nom="date_debut" id="date_debut" required value="<?=$module->date_debut?>">
            </p>

            <p class="form-group">
                <label for="date_debut">Date de fin:</label>
                <input class="form-control" type="date" nom="date_fin" id="date_fin" value="<?=$module->date_fin?>" required>
            </p>

            <p class="form-group">
                <label for="heure_debut">Heure de début:</label>
                <input class="form-control" type="time" nom="heure_debut" id="heure_debut" value="<?=$module->heure_debut?>" required>
            </p>

            <p class="form-group">
                <label for="heure_fin">Heure de fin:</label>
                <input class="form-control" type="time" nom="heure_fin" id="heure_fin" value="<?=$module->heure_fin?>" required>
            </p>

            <p>
                <label for="description">Description :</label>
                <textarea id="description" name="description" class="trumbowyg">
                </textarea>
            </p>
            <div class="col-sm-8 mx-auto mt-4 row">
                <button type="submit" id="submit" name="submit" class="btn btn-blue btn-block col-sm-4 mr-4 mb-4">Envoyer</button>
                <a href="<?= base_url().'module/listemodules'?>" class="btn btn-light btn-block col-sm-4 mb-4">Retour</a>
            </div>
        </form>

        <div class="alert alert-success" role="alert" name="successUpdateModule" style="display: none;">
            Le module a été mise à jour ! Vous serez redirigé automatiquement à la liste des module dans 3 secondes.
        </div>
        <div class="alert alert-danger" role="alert" name="errorUpdateModule" style="display: none;">
            Le module n'a pas pu être mise à jour. Une ou plusieurs entrées sont incorrectes.
        </div>
    <?php endif; ?>
</div>

<script>
    $.trumbowyg.svgPath = '<?= base_url(); ?>assets/css/icons.svg';
    var config = {
        lang: 'fr',
        btnsDef: {
            // Create a new dropdown
            image: {
                dropdown: ['insertImage', 'upload'],
                ico: 'insertImage'
            }
        },
        // Redefine the button pane
        btns: [
            ['historyUndo','historyRedo'],
            ['formatting'],
            ['strong', 'em', 'underline'],
            ['superscript', 'subscript'],
            ['link'],
            ['image'], // Our fresh created dropdown
            /*  */           ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['foreColor', 'backColor'],
            ['fullscreen'],
            ['table']
        ],
        plugins: {
            // Add imagur parameters to upload plugin for demo purposes
            upload: {
                serverPath: "<?= base_url();?>index.php/upload/image",
                urlPropertyName: 'url'
            },
            resizimg : {
                minSize: 20,
                step: 1,
            },
            table: {
                rows:7,
                columns:7,
                styler:'table',
            }
        }
    };
    $('.trumbowyg').trumbowyg(config);
    <?php if(isset($module->section)):?>
    $("#section").selectpicker('val', <?=$module->section?>);
    <?php endif;?>
    <?php if(isset($module->description)): ?>
    $('#description').trumbowyg('html', `<?=$module->description?>`);
    <?php endif; ?>
    $('#jour').selectpicker('val', <?=$module->jour?>);

    $('#form').submit(function(e){
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('date_debut', $('#date_debut').val());
        formData.append('date_fin', $('#date_fin').val());
        formData.append('heure_debut', $('#heure_debut').val());
        formData.append('heure_fin', $('#heure_fin').val());
        $.ajax({
            method: 'POST',
            url: '<?=base_url().'module/updateModule'?>',
            data: formData,
            processData: false,
            contentType: false,
            error: function(){
                $('div[name=errorUpdateModule]').fadeIn(400, function(){
                    setTimeout(function(){
                        $('div[name=errorUpdateModule]').fadeOut();
                    }, 3000)
                })
            },
            success: function(){
                $('div[name=successUpdateModule]').fadeIn(400, function(){
                    setTimeout(function(){
                        window.location = '<?= base_url()."module/listeModules"?>';
                    }, 3000)
                });
            }
        });
    });
</script>