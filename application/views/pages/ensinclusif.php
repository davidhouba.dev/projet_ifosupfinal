<!DOCTYPE html>
<html lang="fr" class="nav-blue">

<section id="EnsInclusif">
	<div class="row d-none d-sm-block d-print-none">
		<div class="col-md-12 EnsInclusif"></div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="py-5 text-center">Enseignement inclusif</h1>
			</div>
		</div>
	</div>
</section>

</header>
<!--Fin du header-->

<!--CONTENT-->
<main class="EnsInclusif pb-5">
	<div class="container">
		<div class="d-flex">
			<div class="col-md-12">
				<h5>Enseignement inclusif (décret du 30 juin 2016)</h5>
				<p>On entend par « enseignement inclusif » : enseignement qui met en œuvre des dispositifs visant à supprimer ou à réduire les barrières matérielles, pédagogiques, culturelles, sociales et psychologiques rencontrées lors de l’accès aux études, au cours des études, aux évaluations des acquis d’apprentissage et à l’insertion socioprofessionnelle par les étudiants en situation de handicap.</p>
				<p>On entend par « étudiant en situation de handicap » : étudiant qui présente des incapacités physiques, mentales, intellectuelles ou sensorielles durables dont l’interaction avec diverses barrières peut faire obstacle à sa pleine et effective participation à l’Enseignement de promotion sociale sur base de l’égalité avec les autres.</p>
				<p>Le décret garantit le droit pour des étudiants en situation de handicap, de solliciter la prise en compte de leurs besoins spécifiques dans leur parcours d’apprentissage.</p>
				<p>La personne de référence pour l’introduction des demandes est Madame Karin Lovibond (conseiller.formation@ifosupwavre.be), conseillère à la formation. Son rôle est :</p>
				<ul>
					<li>D’accueillir l’étudiant en situation de handicap et demandeur d’aménagements ;</li>
					<li>De prendre connaissance des difficultés qui peuvent entraver son parcours au sein de l’établissement ;</li>
					<li>De recueillir le document de demande ;</li>
					<li>D’introduire la demande d’aménagements raisonnables et de faire rapport au Conseil des Etudes ;</li>
					<li>De demeurer la personne de contact de l’étudiant en situation de handicap tout au long de sa formation.</li>
				</ul>
				<p>Le décret prévoit les points suivants :</p>
				<ul>
					<li>Si l’admission en formation est soumise à un ou plusieurs tests, ceux-ci tiennent compte du handicap ;</li>
					<li>Le Conseil des Etudes rend une décision motivée sur la demande d’aménagements raisonnables ;</li>
					<li>La direction de l’établissement adresse la décision au demandeur par lettre recommandée, ainsi qu’à la personne de référence ;</li>
					<li>Si les aménagements raisonnables demandés nécessitent un délai de mise en œuvre ou des conditions particulières, la direction de l’établissement le mentionne dans sa décision.</li>
				</ul>
				<p>Un aménagement raisonnable peut être matériel ou immatériel, pédagogique ou organisationnel. Il ne remet pas en cause les acquis d’apprentissage définis dans les dossiers pédagogiques, mais porte sur la manière d’y accéder et de les évaluer.</p>
				<p>L’étudiant en situation de handicap, lorsqu’il sollicite un ou plusieurs aménagements raisonnables, fournit un des documents suivants à l’appui de sa demande :</p>
				<ul>
					<li>Un document probant, c’est-à-dire toute preuve ou attestation délivrée par une administration publique compétente. Celui-ci doit mentionner une incapacité permanente ;</li>
					<li>Un rapport d’un spécialiste du domaine médical ou paramédical concerné. Ce rapport date de moins d’un an au moment de la demande.</li>
				</ul>
				<p>En cas de décision défavorable, partielle ou totale, du Conseil des Etudes quant aux aménagements raisonnables demandés, la Direction de l’établissement mentionne, dans sa communication écrite, la possibilité pour l’étudiant de saisir la Commission de l’Enseignement de Promotion sociale inclusif.</p>
				<p>Sous peine d’irrecevabilité, cette saisine doit s’opérer par envoi recommandé dans les dix jours ouvrables qui suivent la réception de la décision. Ce délai commence à courir le premier jour ouvrable qui suit la réception du courrier recommandé, la date de la poste ou d’envoi du courriel faisant foi.</p>
				<p>L’étudiant joint à son courrier une copie de la décision de l’établissement.</p>
				<p>Sont considérés comme jours ouvrables tous les jours de la semaine, à l’exception du dimanche et des jours fériés légaux.</p>
				<p>En cas de décision favorable à l’étudiant, cette décision revêt un caractère contraignant pour l’établissement.</p>
				<p>Le ROI de l’IFOSUP prévoit que seront également traitées selon les modalités reprises ci-dessus les demandes d’aménagements raisonnables sollicitées par tous les étudiants diagnostiqués « dys » ou souffrant de maladie chronique.</p>
			</div>
		</div>
	</div>
</main>

<!--CONTENT-->




<script src="<?= base_url();?>assets/js/jquery.min.js"></script>
<script src="<?= base_url();?>assets/js/popper.min.js"></script>
<script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>

</body>
</html>
