

<!DOCTYPE html>
<html lang="fr" class="nav-blue">


<section id="roi">
	<div class="row d-none d-sm-block d-print-none">
		<div class="col-md-12 roi"></div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="py-5 text-center">Règlement d'ordre intérieur</h1>
			</div>
		</div>
	</div>
</section>

</header>
<!--Fin du header-->

<!--CONTENT-->
<main class="roi">
	<div class="container pb-5">
		<div class="d-flex flex-center">
			<div class="col-md-8">
				<p class="pb-2">Bonjour et bienvenue,</p>
				<p class="pb-2">Vous avez décidé d’intégrer l’IFOSUP afin de suivre une formation. Merci pour votre confiance!</p>
				<p class="pb-2">La vie citoyenne active en démocratie implique une participation de tous les instants et l’observation de règles. Le règlement affirme les limites dont l’observance assure la qualité des apprentissages, le respect et la sécurité de tous. Il permet de recevoir une formation et une éducation de qualité. Le règlement favorise la construction de relations sereines et protège chacun de l’arbitraire et de l’injustice.</p>
				<p class="pb-2">Tous les acteurs de l’établissement en sont les garants et les bénéficiaires.</p>
				<p class="pb-2">L’école s’engage à mettre tout en œuvre pour répondre aux besoins de chacun, dans la mesure de ses possibilités, dans un climat de transparence et de dialogue.</p>
				<p class="pb-2">Voici quelques informations essentielles pour bien appréhender le fonctionnement de la promotion sociale en générale et de l'IFOSUP en particulier.</p>
				<p class="pb-2">Nous mettrons tout en oeuvre pour que vous vous épanouissiez pleinement, humainement et professionnellement pendant votre parcours chez nous qu'il soit de quelques semaines ou de plusieurs années.</p>
				<p class="pb-2">L'inscription à l'IFOSUP implique la prise de connaissance intégrale du règlement d'ordre intérieur ainsi que son acceptation et son respect.</p>
				<p class="pb-2">Je reste à votre disposition pour tout renseignement ou conseil.</p>
				<p class="pb-2">Valérie Vanderavero, Directrice</p>
			</div>

			<div class="col-md-4 text-center">
				<a class="btn btn-blue" target="_blank" href="<?= base_url();?>docs/ROI-IFOSUP.pdf">Obtenir le R.O.I en version PDF</a>
			</div>
		</div>
	</div>
</main>

<!--CONTENT-->


<script src="<?= base_url();?>assets/js/jquery.min.js"></script>
<script src="<?= base_url();?>assets/js/popper.min.js"></script>
<script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>

</body>
</html>
