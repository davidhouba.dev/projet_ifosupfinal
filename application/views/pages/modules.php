<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container mt-5 pt-2">
    <?php if(isset($modules)): ?>
    <a href="<?= base_url(); ?>index.php/Connection/login" class="btn btn-blue mt-5">
        <i class="fas fa-align-justify pr-2"></i> Panel administrateur</a>
    <ul class="mt-2">
        <?php foreach ($modules as $m): ?>
        <li class="nav">
            <div class="nav-item col-sm-4">
                <span><?= $m->nom ?> </span>
            </div>
            <div class="nav-item col-sm-2">
                <button
                    class="delete btn btn-blue"
                    data-id="<?=$m->id?>"
                    data-toggle="modal"
                    data-target="#deleteModal"
                    data-module="<?= $m->nom?>">Delete</button>
            </div>
            <div class="nav-item">
                <a href="<?=base_url()."module/updateModule/".$m->id?>">
                    <button class="btn btn-blue">Update</button>
                </a>
            </div>
        </li>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>


    <h2 class="py-5 text-center">Ajouter un module</h2>

    <form id="form" method="post">
        <p class="form-group">
        <label for="nom">Nom:</label>
        <input id="nom" name="nom" type="text" class="text-input form-control" required>
        </p>

        <?php if(isset($sections)): ?>
        <p class="form-group">
            <label for="section">Section:</label>
            <select class="form-control selectpicker" id="section" name="section">
                <?php foreach ($sections as $section): ?>
                    <option value="<?= $section->id ?>"><?= $section->nom ?></option>
                <?php endforeach; ?>
            </select>
        </p>
        <?php endif; ?>

        <p class="form-group">
            <label for="jour">Jour:</label>
            <select id="jour" name="jour" class="selectpicker form-control">
                <option value="0">Lundi</option>
                <option value="1">Mardi</option>
                <option value="2">Mercredi</option>
                <option value="3">Jeudi</option>
                <option value="4">Vendredi</option>
                <option value="5">Samedi</option>
                <option value="6">Dimanche</option>
            </select>
        </p>

        <p class="form-group">
            <label for="date_debut">Date de début:</label>
            <input class="form-control" type="date" nom="date_debut" id="date_debut" required>
        </p>

        <p class="form-group">
            <label for="date_debut">Date de fin:</label>
            <input class="form-control" type="date" nom="date_fin" id="date_fin" required>
        </p>

        <p class="form-group">
            <label for="heure_debut">Heure de début:</label>
            <input class="form-control" type="time" nom="heure_debut" id="heure_debut" required>
        </p>

        <p class="form-group">
            <label for="heure_fin">Heure de fin:</label>
            <input class="form-control" type="time" nom="heure_fin" id="heure_fin" required>
        </p>

        <p class="form-group">
            <label for="description">Description:</label>
            <textarea id="description" name="description" class="trumbowyg form-control"></textarea>
        </p>

        <button type="submit" id="submit" name="submit" class="btn btn-blue btn-block col-sm-4 mb-4 mx-auto">Envoyer</button>
    </form>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Suppression du module</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Etes-vous sûr de vouloir supprimer le module <span data-fonction="show-nom"></span> ?
                </div>
                <div class="alert alert-danger" role="alert" name="errorDelModule" style="display: none;">
                    Le module n'a pas pu être supprimé de la base de données, rafraichissez la page et réessayer. Si l'erreur persiste, contactez un administrateur.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Garder</button>
                    <button type="button" class="btn btn-primary" data-fonction="supprimer">Supprimer</button>
                </div>
            </div>
        </div>
    </div>

    <div class="alert alert-success" role="alert" style="display: none;">
        Le module a été ajouté à la base de données, la page sera automatiquement rafraichie dans 3 secondes.
    </div>
    <div class="alert alert-danger" role="alert" name="errorAjouteModule" style="display: none;">
        Le module n'a pas pu être ajouté à la base de données, vérifiez que vous avez bien complété le formulaire. Si le problème persiste, contactez l'adminisatrateur du site.
    </div>
</div>

<script>
    $(document).ready(function(){
        $('html, body').animate({ scrollTop: 0 }, 'fast');
    });

    $.trumbowyg.svgPath = '<?= base_url(); ?>assets/css/icons.svg';
    var config = {
        lang: 'fr',
        btnsDef: {
            // Create a new dropdown
            image: {
                dropdown: ['insertImage', 'upload'],
                ico: 'insertImage'
            }
        },
        // Redefine the button pane
        btns: [
            ['historyUndo','historyRedo'],
            ['formatting'],
            ['strong', 'em', 'underline'],
            ['superscript', 'subscript'],
            ['link'],
            ['image'], // Our fresh created dropdown
            /*  */           ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['foreColor', 'backColor'],
            ['fullscreen'],
            ['table']
        ],
        plugins: {
            // Add imagur parameters to upload plugin for demo purposes
            upload: {
                serverPath: "<?= base_url();?>index.php/upload/image",
                urlPropertyName: 'url'
            },
            resizimg : {
                minSize: 20,
                step: 1,
            },
            table: {
                rows:7,
                columns:7,
                styler:'table',
            }
        }
    };
    $('.trumbowyg').trumbowyg(config);

    $("#deleteModal").on("show.bs.modal",function(e){
        var button = e.relatedTarget;
        $("#deleteModal span[data-fonction=show-nom]").html("<strong>"+$(button).attr("data-module")+"</strong>");
        var id=  $(button).attr("data-id");
        $("#deleteModal button[data-fonction=supprimer]").click(function(){
            $.ajax({
                method: "POST",
                url: "<?=base_url()?>module/deleteModule",
                data:{
                    id: id
                },
                error: function(){
                    $("div.alert-danger[name=errorDelModule]").fadeIn();
                },
                success: function(){
                    $("#deleteModal").modal('hide').end();
                    var module =  $($(button).parents()[1]);
                    module.fadeOut(400, function(){
                        module.remove();
                    });
                }
            });
        });
    });

    $("#form").submit(function(e){
        e.preventDefault();
        $("div.alert").fadeOut('fast');
        var formData = new FormData(this);
        formData.append('date_debut', $('#date_debut').val());
        formData.append('date_fin', $('#date_fin').val());
        formData.append('heure_debut', $('#heure_debut').val());
        formData.append('heure_fin', $('#heure_fin').val());
        $.ajax({
            method: "POST",
            url: "<?=base_url()?>module/ajouteModule",
            processData: false,
            contentType: false,
            data: formData,
            success: function(){
                $("div.alert-success").fadeIn(400, function(){
                    setTimeout(function(){
                        location.reload(true);
                    },3000)
                });
            },
            error: function(){
                $("div.alert-danger[name=errorAjouteModule]").fadeIn();
            }
        });
    });
</script>