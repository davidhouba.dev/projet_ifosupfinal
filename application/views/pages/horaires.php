<!DOCTYPE html>
<html lang="fr" class="nav-yellow">
<!--DEBUT DU HEADER AVEC NAV-->

<!--FIN DEBUT DU HEADER AVEC NAV-->
<section id="horaires">
	<div class="row d-none d-sm-block d-print-none">
		<div class="col-md-12 horaires"></div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="py-5 text-center">Horaires des cours</h1>
			</div>
		</div>
	</div>
</section>

</header>
<!--Fin du header-->

<!--Content-->

<section class="pb-5">
	<div class="container">
		<!-- L'ensemble de cartes-->

		<!-- Card secondaire-->
		<div class="row py-3">
			<div class="col-md-12">
				<div class="card">
					<!-- Card header -->
					<h5 class="card-header text-center ch-yellow"><a href="<?= base_url();?>pages/section?idSec=1" class="black-text">Sections du secondaire</a></h5>

					<!-- Card content -->
					<div class="card-body">

						<!-- Title -->
						<!-- Text -->
						<ul class="card-text list-group list-group-flush py-3">
							<li class="list-group-item"><a href="<?= base_url();?>docs/horaires/CTSS_Compta.pdf" target="_blank" class="grey-text t-yellow">Technicien en comptabilité</a></li>
							<li class="list-group-item"><a href="<?= base_url();?>docs/horaires/CTSS_Elec.pdf" target="_blank"  class="grey-text t-yellow">Technicien électricien/automaticien </a></li>
							<li class="list-group-item"><a href="<?= base_url();?>docs/secondaire/Gestion.pdf" target="_blank"  class="grey-text t-yellow">Connaissance de gestion</a></li>
							<li class="list-group-item"><a href="<?= base_url();?>docs/secondaire/CTSS_Info.pdf" target="_blank"  class="grey-text t-yellow">Technicien en informatique</a></li>
						</ul>
					</div>

				</div>
			</div>
		</div>
		<!-- Card -->

		<!-- Card superieur -->
		<div class="row py-3">
			<div class="col-md-12">
				<div class="card">
					<!-- Card header -->
					<h5 class="card-header text-center ch-yellow"><a href="<?= base_url();?>pages/section?idSec=2" class="black-text">Sections du supérieur</a></h5>

					<!-- Card content -->
					<div class="card-body">

						<!-- Title -->
						<h5 class="card-title">Nos bacheliers</h5>
						<!-- Text -->
						<ul class="card-text list-group list-group-flush py-3">
							<li class="list-group-item"><a href="<?= base_url();?>docs/horaires/BAC_Compta.pdf" class="grey-text t-yellow" target="_blank" >Bachelier en comptabilité</a></li>
							<li class="list-group-item"><a href="<?= base_url();?>docs/horaires/BAC_Info.pdf" class="grey-text t-yellow" target="_blank" >Bachelier en informatique de gestion  </a></li>
							<li class="list-group-item"><a href="<?= base_url();?>docs/horaires/BAC_Market.pdf" class="grey-text t-yellow" target="_blank" >Bachelier en marketing</a></li>
						</ul>

						<!-- Title -->
						<h5 class="card-title">Nos brevets de l'enseignement supérieur</h5>
						<!-- Text -->
						<ul class="card-text list-group list-group-flush py-3">
							<li class="list-group-item"><a href="<?= base_url();?>docs/horaires/BES_Web_Des.pdf" class="grey-text t-yellow" target="_blank" >BES Webdesign</a></li>
							<li class="list-group-item"><a href="<?= base_url();?>docs/horaires/BES_Web_Dev.pdf" class="grey-text t-yellow" target="_blank" >BES Webdeveloper</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- Fin Card superieur -->

		<!-- Card langues -->
		<div class="row py-3">
			<div class="col-md-12">
				<div class="card">
					<!-- Card header -->
					<h5 class="card-header text-center ch-yellow"><a href="<?= base_url();?>pages/langues?idSec=3" class="black-text">Langues</a></h5>

					<!-- Card content -->
					<div class="card-body">

						<!-- Title -->
						<h5 class="card-title">Nos cours</h5>
						<!-- Text -->
						<ul class="card-text list-group list-group-flush py-3">
							<li class="list-group-item"><a href="<?= base_url();?>docs/horaires/FR_soir.pdf" target="_blank" class="grey-text t-yellow">Français langue étrangère</a></li>
							<li class="list-group-item"><a href="<?= base_url();?>docs/langues/Anglais.pdf" target="_blank" class="grey-text t-yellow">Anglais</a></li>
							<li class="list-group-item"><a href="<?= base_url();?>docs/langues/Ndls_Allemand.pdf" target="_blank" class="grey-text t-yellow">Néerlandais</a></li>
							<li class="list-group-item"><a href="<?= base_url();?>docs/langues/Ndls_Allemand.pdf" target="_blank" class="grey-text t-yellow">Allemand</a></li>
							<li class="list-group-item"><a href="<?= base_url();?>docs/langues/Esp_Ital.pdf" target="_blank" class="grey-text t-yellow">Espagnol</a></li>
							<li class="list-group-item"><a href="<?= base_url();?>docs/langues/Esp_Ital.pdf" target="_blank" class="grey-text t-yellow">Italien</a></li>
							<li class="list-group-item"><a href="<?= base_url();?>docs/horaires/FR_soir.pdf" target="_blank" class="grey-text t-yellow">Langue des signes</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- Fin Card langues -->

		<!-- Card En journée -->
		<div class="row py-3">
			<div class="col-md-12">
				<div class="card">
					<!-- Card header -->
					<h5 class="card-header text-center ch-yellow"> En journée</h5>

					<!-- Card content -->
					<div class="card-body">

						<!-- Title -->
						<h5 class="card-title">Nos modules</h5>
						<!-- Text -->
						<ul class="card-text list-group list-group-flush py-3">
							<li class="list-group-item"><a href="<?= base_url();?>docs/horaires/Langues_jour.pdf" class="grey-text t-yellow">Langues</a></li>
							<li class="list-group-item"><a href="<?= base_url();?>docs/horaires/FR_jour.pdf" class="grey-text t-yellow">Français langue étrangère</a></li>
							<li class="list-group-item"><a href="<?= base_url();?>docs/horaires/MODULES_INFO_18-19.pdf" class="grey-text t-yellow">Modules informatiques</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- Fin Card En journée -->


		<!-- L'ensemble de cartes-->

	</div>
</section>

<!--Content-->


<script src="<?= base_url();?>assets/js/jquery.min.js"></script>
<script src="<?= base_url();?>assets/js/popper.min.js"></script>
<script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url();?>assets/js/mdb.min.js"></script>

</body>
</html>
