<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container mt-5 pt-2">
    <?php if(isset($section)):?>
    <h2>Edition d'un cours</h2>

    <form id="form" method="post">
        <?php if($section->id): ?>
            <input name="id" value="<?=$section->id?>" hidden>
        <?php endif;?>
        <p>
            <label for="nom">Nom :</label>
            <input id="nom" name="nom" type="text" class="ml-2 col-sm-4" value="<?=$section->nom?>"
            <?php if($section->nom == "Langues") : ?>
            disabled
            <?php endif;?>
            >
        </p>

        <p>
            <label for="header">Header de la section (laissez vide pour ne pas le changer):</label>
            <input id="header" name="header" type="file" class="form-control-file" accept="image/*">
        </p>

        <?php if(isset($sections)): ?>
            <p class="form-group">
                <label for="section">Section:</label>
                <select class="form-control selectpicker" id="section" name="section">
                    <?php foreach ($sections as $section): ?>
                        <option value="<?= $section->id ?>"><?= $section->nom ?></option>
                    <?php endforeach; ?>
                </select>
            </p>
        <?php endif; ?>

        <p class="form-group">
            <label for="jour">Jour:</label>
            <select id="jour" name="jour" class="selectpicker form-control">
                <option value="0">Lundi</option>
                <option value="1">Mardi</option>
                <option value="2">Mercredi</option>
                <option value="3">Jeudi</option>
                <option value="4">Vendredi</option>
                <option value="5">Samedi</option>
                <option value="6">Dimanche</option>
            </select>
        </p>


        <p>
            <label for="description">Description :</label>
            <textarea id="description" name="description" class="trumbowyg">

        </textarea>
        </p>
        <div class="col-sm-8 mx-auto mt-4 row">
            <button type="submit" id="submit" name="submit" class="btn btn-blue btn-block col-sm-4 mr-4 mb-4">Envoyer</button>
            <a href="<?= base_url().'section/listesections'?>" class="btn btn-light btn-block col-sm-4 mb-4">Retour</a>
        </div>
    </form>

    <div class="alert alert-success" role="alert" name="successUpdateSection" style="display: none;">
        La section a été mise à jour ! Vous serez redirigé automatiquement à la liste des sections dans 3 secondes.
    </div>
    <div class="alert alert-danger" role="alert" name="errorUpdateSection" style="display: none;">
        La section n'a pas pu être mise à jour. Une ou plusieurs entrées sont incorrectes.
    </div>
    <?php endif; ?>
</div>

<script>
    $.trumbowyg.svgPath = '<?= base_url(); ?>assets/css/icons.svg';
    var config = {
        lang: 'fr',
        btnsDef: {
            // Create a new dropdown
            image: {
                dropdown: ['insertImage', 'upload'],
                ico: 'insertImage'
            }
        },
        // Redefine the button pane
        btns: [
            ['historyUndo','historyRedo'],
            ['formatting'],
            ['strong', 'em', 'underline'],
            ['superscript', 'subscript'],
            ['link'],
            ['image'], // Our fresh created dropdown
            /*  */           ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['foreColor', 'backColor'],
            ['fullscreen'],
            ['table']
        ],
        plugins: {
            // Add imagur parameters to upload plugin for demo purposes
            upload: {
                serverPath: "<?= base_url();?>index.php/upload/image",
                urlPropertyName: 'url'
            },
            resizimg : {
                minSize: 20,
                step: 1,
            },
            table: {
                rows:7,
                columns:7,
                styler:'table',
            }
        }
    };
    $('.trumbowyg').trumbowyg(config);

    $('#description').trumbowyg('html', `<?=$section->description?>`);

    $('#form').submit(function(e){
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            method: 'POST',
            url: '<?=base_url().'section/updateSection'?>',
            data: formData,
            processData: false,
            contentType: false,
            error: function(){
                $('div[name=errorUpdateSection]').fadeIn(400, function(){
                    setTimeout(function(){
                        $('div[name=errorUpdateSection]').fadeOut();
                    }, 3000)
                })
            },
            success: function(){
                $('div[name=successUpdateSection]').fadeIn(400, function(){
                    setTimeout(function(){
                        window.location = '<?= base_url()."section/listesections"?>';
                    }, 3000)
                });
            }
        });
    });

</script>
