<!DOCTYPE html>
<html lang="fr">
<!--DEBUT DU HEADER AVEC NAV-->
 <section id="langues"> 
    <div class="row d-none d-sm-block d-print-none">
           <div class="col-md-12" style="min-height: 40vh;width: 100%;background-repeat: no-repeat;background-size: cover;background-position: center; background-image: url(<?= base_url().$afficheLangue[0]['header_path']; ?>);"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
              foreach($afficheLangue as $afficheLangue) :
            ?>
                <h1 class="py-5 text-center"><?= $afficheLangue['nom']; ?></h1>
            </div>
        </div>
     </div>       
</section> 

</header>
<!--Fin du header-->  

        
<!--  Breadcrumbs -->
  <nav aria-label="breadcrumb" class="px-5">
    <ol class="breadcrumb">
      <li class="breadcrumb-item arrow-blue">
         <a class="grey-text t-blue" href="<?= base_url(); ?>pages">Accueil</a>
      </li>
      <li class="breadcrumb-item arrow-blue">
         <a class="grey-text t-blue" href="<?= base_url(); ?>pages/formations">Nos formations</a>
      </li>
       <li class="breadcrumb-item active arrow-blue">
         <a class="t-blue" href="<?= base_url(); ?>pages/langues?idSec=<?= $affiche_cat[0]['idSection'] ?>">Langues</a>
      </li>
    </ol>
  </nav>
<!--  Breadcrumbs -->

<!--Content-->
<main class="pb-5 select-blue">  
    <div class="container">
           <div class="d-flex align-items-start py-5">
               <div class="col-md-7 blue-border">
                   <h3>Description</h3>
                         <p> <?= $afficheLangue['description']; ?></p>
               </div>
               
               <div class="col-md-5">
                  <!-- Card secondaire-->
                     <div class="card">
                      <!-- Card header -->
                      <h5 class="card-header text-center ch-blue">Nos cours de langues</h5>

                      <!-- Card content -->
                      <div class="card-body">
                        <!-- Text -->
                        <ul class="card-text list-group list-group-flush py-3">
                        <?php foreach($afficherLiensLangues as $afficherLiensLangues) : ?>
                            <li class="list-group-item"><a href="<?= base_url().$afficherLiensLangues['lien'];?>" target="_blank" class="grey-text t-blue"><?=$afficherLiensLangues['texte'];?></a></li>
                        <?php endforeach; ?>
                        </ul> 
                      </div>

                    </div>
                <!-- Card -->
                   
               </div>
           </div>        
    </div>
	<?php
      endforeach;
    ?>
</main>

<!--Content-->

		<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
		<script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
		<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
		<script src="<?= base_url(); ?>assets/js/mdb.min.js"></script>

</body>
</html>
