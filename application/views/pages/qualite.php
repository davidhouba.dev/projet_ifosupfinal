<!DOCTYPE html>
<html lang="fr" class="nav-blue">


<section id="qualite">
	<div class="row d-none d-sm-block d-print-none">
		<div class="col-md-12 qualite"></div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="py-5 text-center">Politique de qualité</h1>
			</div>
		</div>
	</div>
</section>

</header>
<!--Fin du header-->

<!--CONTENT-->
<main class="qualite pb-5">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p>L’IFOSUP a à cœur de maintenir une dynamique d’amélioration continue de la qualité de ses programmes. Son but principal est de pérenniser ou de développer ses forces tout en travaillant au développement d’axes jugés essentiels au maintien ou à l’amélioration de la qualité de chacun de ses cursus, sans faire de distinction entre les sections ou les niveaux d’étude.</p>
				<p>L’IFOSUP a choisi de privilégier 3 types d’actions centrées autour d’une stratégie globale d’aide à la réussite:</p>
				<ul>
					<li>les actions visant à maintenir un processus d’amélioration continue via <strong>l’évaluation</strong> constante de nos pratiques;</li>
					<li>les actions visant à <strong>lutter contre l’abandon</strong>;</li>
					<li>les actions de type <strong>“aide à la réussite”</strong>.</li>
				</ul>


				<figure class="text-center"><img src="<?= base_url();?>assets/img/qualite.png" alt="Axes stratégiques" class="img-fluid"></figure>

				<p><strong>Ces axes stratégiques spécifiques auront comme finalité absolue d’assurer, à chaque étudiant, tout au long de son cursus, un encadrement de qualité, lui permettant de s'épanouir au maximum de ses possibilités et de mener à bien son objectif.</strong></p>

				<h3>Audits externes</h3>
				<p>Dans le cadre de l’évaluation externe de la qualité dans l’enseignement supérieur, sous la responsabilité de l’AEQES (Agence pour l’évaluation de la qualité de l’enseignement supérieur en Fédération Wallonie-Bruxelles), les cursus ci-dessous ont été audités.</p>
				<p>Les résultats de ces analyses, produits sous forme de rapports, sont téléchargeables à partir de cette page de notre site.</p>

				<h5>Bachelier en Informatique de gestion</h5>
				<ul>
					<li><a href="<?= base_url();?>docs/IFOSUPWavreRFS.pdf" class="t-blue" target="_blank">Rapport final de synthèse Informatique (2011-2012)</a></li>
					<li><a href="<?= base_url();?>docs/CPDSINFOIFOSUP.pdf" class="t-blue" target="_blank">Plan de suivi Informatique (2011-2012)</a></li>
					<li><a href="<?= base_url();?>docs/rapportsuiviIFOSUPinformatique.pdf" class="t-blue" target="_blank">Rapport d'évaluation de suivi Informatique (2016-2017)</a></li>
				</ul>

				<h5>Bachelier en Comptabilité</h5>
				<ul>
					<li><a href="<?= base_url();?>docs/20170626ComptaIfosupRE.pdf" class="t-blue" target="_blank">Rapport d'évaluation (2016-2017)</a></li>
					<li>Les actions sélectionnées comme prioritaires par l’IFOSUP sont détaillées dans notre plan d’action : <a href="<?= base_url();?>docs/PlanAction1722.pdf" class="t-blue" target="_blank">Plan d’action 2017-2022</a></li>
				</ul>

				<h3>Contacter le Service Qualité</h3>
				<ul>
					<li>Bruno Martin : Bruno.martin@ifosupwavre.be</li>
					<li>Nathalie Vanassche : Nathalie.vanassche@ifosupwavre.be</li>
				</ul>

			</div>
		</div>
	</div>
</main>

<!--CONTENT-->




<script src="<?= base_url();?>assets/js/jquery.min.js"></script>
<script src="<?= base_url();?>assets/js/popper.min.js"></script>
<script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>

</body>
</html>d
