<!DOCTYPE html>
<html lang="fr" class="nav-green">

<?php foreach($afficheInscription as $afficheInscription) : ?>
 <section id="inscriptions"> 
    <div class="row d-none d-sm-block d-print-none">
           <div class="col-md-12 inscriptions"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="py-5 text-center"><?= $afficheInscription['nom']; ?></h1>
            </div>
        </div>
     </div>       
</section> 

</header>
<!--Fin du header-->  


<!--Content-->
<main class="pb-5">  
    <div class="container">
           <div class="row pb-5">
               <div class="col-md-12">
                <?= $afficheInscription['contenu']; ?>

               </div>
                    
           </div>        
    </div>
</main>
<?php
endforeach;
?>

<!--Content-->



	<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/mdb.min.js"></script>

</body>
</html>
