
<section > 
<div class="row d-none d-sm-block d-print-none">
       <div class="col-md-12 " style="min-height: 40vh;width: 100%;background-repeat: no-repeat;background-size: cover;background-position: center; background-image: url(<?= base_url().$affiche_section[0]['header_path']; ?>);"></div>

</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="py-5 text-center"><?= $affiche_section[0]['nomSection'];?></h1>
        </div>
    </div>
 </div>       
</section> 

</header>
<!--Fin du header-->  

    
<!--  Breadcrumbs -->
<nav aria-label="breadcrumb" class="px-5">
<ol class="breadcrumb">
  <li class="breadcrumb-item arrow-yellow">
     <a class="grey-text t-yellow" href="<?= base_url(); ?>pages">Accueil</a>
  </li>
  <li class="breadcrumb-item arrow-yellow">
     <a class="grey-text t-yellow" href="<?= base_url(); ?>pages/formations">Nos formations</a>
  </li>
   <li class="breadcrumb-item active arrow-yellow">
     <a class="t-yellow" href="<?= base_url(); ?>pages/section?idSec=<?= $affiche_cat[0]['idSection'] ?>"><?= $affiche_cat[0]['nomSection']; ?></a>
  </li>
</ol>
</nav>
<!--  Breadcrumbs -->
<!--Content-->
<main class="pb-5">  
<div class="container">
       <div class="d-flex py-5 align-items-center">
           <div class="col-md-7 yellow-border">
               <h3>Description</h3>
                <p><?= $affiche_section[0]['descSection'];?></p>
            </div>
           
           <div class="col-md-5">
              <!-- Card secondaire-->
                 <div class="card">
                  <!-- Card header -->
                  <h5 class="card-header text-center ch-yellow">Nos sections</h5>

                  <!-- Card content -->
                  <div class="card-body">

                    <!-- Title -->
                    <!-- Text -->
                    <ul class="card-text list-group list-group-flush py-3">
                    <?php
                    foreach($affiche_section as $affiche_section) :
                    ?>
                        <li class="list-group-item"><a href="<?= base_url(); ?>pages/cours_front?idCours=<?= $affiche_section['idCours'] ?>" class="grey-text t-yellow"><?= $affiche_section['nomCours'] ?></a></li>
                       
                    <?php
                    endforeach;
                    ?>
                    </ul> 
                  </div>

                </div>
            <!-- Card -->
               
           </div>
       </div>        
</div>
</main>

<!--Content-->


<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/mdb.min.js"></script>

</body>
</html>
