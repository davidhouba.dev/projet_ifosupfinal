<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" crossorigin="anonymous">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/login.css">
</head>
<body>

<div class="header">
	<h2>Panel administrateur</h2>
</div>
<div class="content">
        <h3 class="text-center">Bienvenue</h3>
        <p class="col-11 mx-auto">
            <a href="<?= base_url().'cours/listecours'?>" class="col-12 btn text-light my-1">Management des cours</a>
            <a href="<?= base_url().'section/listesections'?>" class="col-12 btn text-light my-1">Management des sections</a>
            <a href="<?= base_url().'PagesAdmin/listePages'?>" class="col-12 btn text-light my-1">Management des pages</a>
            <a href="<?= base_url().'module/listemodules'?>" class="col-12 btn text-light my-1">Management des modules</a>
            <a href="<?= base_url().'user/updateuser'?>" class="col-12 btn text-light my-1">Modifier les identifiants de connexion</a>
            <hr>
            <center>
            <a href="<?= base_url(); ?>index.php/connection/logout" class="col-12 text-danger">Déconnexion</a>
            </center>
        </p>
</div>

</body>
</html>
