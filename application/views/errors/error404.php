<div class="container my-5 pt-5 mx-auto col-sm-8">

	<h1 class="display-1 mt-5  mx-auto col-sm-2" >
		<i class="far fa-frown fa-2x"></i>
	</h1>
	<h2 class="display-1 text-danger font-title font-weight-bold text-center">404</h2>
	<h3 class="display-4 font-title text-center">Page non trouvée</h3>

	<div class="text-center">
		La page que vous tentez d'afficher n'existe pas ou une autre erreur s'est produite.
	</div>
	<div class="text-center">
		Vous pouvez revenir à <a href="javascript:history.back()">la page précédente</a> ou aller à
		<a href="<?= base_url();?>pages">la page d'accueil</a>.
	</div>

</div>

<script src="<?= base_url();?>assets/js/jquery.min.js"></script>
<script src="<?= base_url();?>assets/js/popper.min.js"></script>
<script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url();?>assets/js/mdb.min.js"></script>
<script src="<?= base_url();?>assets/js/main.js"></script> <!-- il doit tjs etre à la -->
