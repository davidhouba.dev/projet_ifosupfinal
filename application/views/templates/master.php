<?php

    if(isset($couleur)){
        $this->load->view('pages/elements/header',$couleur);
    }else{
        $this->load->view('pages/elements/header');
    }

    if(isset($data)){
        $this->load->view($pages, $data);

    }else{
        $this->load->view($pages);
    }

    $this->load->view('pages/elements/footer');

?>
