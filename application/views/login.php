<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/jquery-resizable.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/trumbowyg.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/trumbowyg.upload.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/fr.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/trumbowyg.resizimg.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/trumbowyg.colors.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/trumbowyg.history.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/trumbowyg.table.min.js"></script>

	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/trumbowyg.min.css">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/trumbowyg.colors.min.css">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/trumbowyg.table.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/login.css">
</head>
<body>
<div class="header">
	<h2>Login</h2>
</div>

<form method="post" action="<?= base_url(); ?>index.php/Connection/login">
	<?php
	$this->load->view('elements/errors.php',$errors);
	?>
	<div class="input-group">
		<label>Email</label>
		<input type="email" name="email" required>
	</div>
	<div class="input-group">
		<label>Password</label>
		<input type="password" name="password" required>
	</div>
	<div class="input-group">
		<button type="submit" class="btn" name="login_user">Login</button>
	</div>

</form>
</body>
</html>
